
function initialize() {
 // geocoder = new google.maps.Geocoder();
 
  var latlng = new google.maps.LatLng(majLat, majLon);
  
  var mapOptions = {
    zoom: 13,
       mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
        position: google.maps.ControlPosition.BOTTOM_CENTER
    },
    center: latlng
  };
  
  map = new google.maps.Map(document.getElementById('map'), mapOptions);
 
}

var infowindow;

function mapaInicio() {

 // geocoder = new google.maps.Geocoder();
 
  var latlng = new google.maps.LatLng(majLat, majLon);
  
  var mapOptions = {
    zoom: 13,
       mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
        position: google.maps.ControlPosition.BOTTOM_CENTER
    },
    center: latlng
  };
  
  map = new google.maps.Map(document.getElementById('map'), mapOptions);
  
}

function busPos(){
    
     if(document.getElementById('logoMap').src.indexOf("_off.png")<0){
        if(markerUser!==null){
            markerUser.setMap(null);
            markerUser = null;
        }
            
          getGps();
         document.getElementById("logoMap").src ="img/map_off.png";

         map.panTo( new google.maps.LatLng( userLat, userLon));
         var latlng = new google.maps.LatLng(userLat, userLon);
                  markerUser = new google.maps.Marker({
            map:map,
            draggable:false,
            animation: google.maps.Animation.DROP,    
            details: "user",
            icon: "img/userPos.png",
            position: latlng
        }); 
      }
      else {
         
          document.getElementById("logoMap").src ="img/map.png";
          map.panTo( new google.maps.LatLng( majLat, majLon));
                 markerUser.setMap(null);
            markerUser = null;
          
//          map.addMarker({       	
//            lat: userLat,
//            lng: userLon,         
//            icon:"img/userPos.png",
//            size: 'small',
//            details: 'Usuario',
//            title:"Usuario",
//            infoWindow: {                  
//                content: "<h3>" + nomUser + "tu posición gps es, Latitud: " + userLat +" Longitud: " + userLon + "</h3>"
//            }
//          });
     }
 }
 
 
function mapaEdicion() { 
    
  var latlng = new google.maps.LatLng(majLat, majLon);
  
  var mapOptions = {
    zoom: 13,
       mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
        position: google.maps.ControlPosition.BOTTOM_CENTER
    },
    center: latlng
  };
  
  map = new google.maps.Map(document.getElementById('map'), mapOptions);
  markers = [];
  markers2 = [];

  
}


function addMarkerMap(){
      
 edicionMapa = true;
 var latlng = new google.maps.LatLng(majLat, majLon);
     
  var marker = new google.maps.Marker({
    map:map,
    draggable:true,
    animation: google.maps.Animation.DROP,    
    details: numMarkers,
    icon: "img/marker_a.png",
    position: map.getCenter()
  }); 
  
  var mark = new Object(); 
  
  mark['lat'] = majLat;  
  mark['lon'] = majLon;
  mark['details'] = numMarkers;
  mark['titulo'] = "";
  mark['resumen'] = "";  
  mark['activo'] = true;  
  mark['email'] = idUser; 
  markers.push(mark);  
  markers2.push(marker);
  
  numMarkers++;
  

  google.maps.event.addListener(marker, 'click', function(event) {
      
        placeMarker(event.latLng, marker);

      });
      
   google.maps.event.addListener(marker, 'mouseup', function(event) {      
        cambiarLocalizacionMarker(event.latLng, marker);
   }); 
}

function cambiarLocalizacionMarker(location,marker) {   
    for(var i=0; i<markers.length; i++) {       
        if(markers[i]['details'] == marker.details) {
            markers[i]['lat'] = location.lat();
            markers[i]['lon'] = location.lng();            
        }
    }
}



function dibujarRuta(ficheroGpx) {

         $.ajax({
              type: "POST",
              url: "http://150.241.239.51//public/InRoute/fileExists.php",
              data: {FileName:ficheroGpx},
                error:function(jqXHR,text_status,strError){ 
                var message = JSON.stringify(jqXHR);
                console.error(message);               
            },
            success:function(data){

              if(data==="true" || data===true){  // Si exite el fichero de ruta, cargamos la ruta
                  
                  setTimeout(cargarRuta(ficheroGpx), 1000);

              }

             }   
            });
            

}

function cargarRuta(ficheroGpx){
 

                      $.ajax({
                        type: "GET",
                        url: "http://150.241.239.51//public/InRoute/Rutas/" + encodeURIComponent(ficheroGpx) + ".gpx",
                        dataType: "xml",
                        error:function(jqXHR,text_status,strError){         
                            var message = JSON.stringify(jqXHR);        
                            console.error(message);
                           callback();
                           return true;
                        },
                        success: function(xml) {                           
                            console.log("SUCCESS!!!!" + ficheroGpx);
                              var points = [];
                              var bounds = new google.maps.LatLngBounds ();
                              $(xml).find("trkpt").each(function() {
                                var lat = $(this).attr("lat");
                                var lon = $(this).attr("lon");
                                var p = new google.maps.LatLng(lat, lon);                                
                                points.push(p);
                                bounds.extend(p);
                              });
   
                              var poly = new google.maps.Polyline({
                                // use your own style here
                                path: points,
                                strokeColor: "#FF00AA",
                                strokeOpacity: .7,
                                strokeWeight: 4
                              });
   
                              poly.setMap(map);
   
                              // fit bounds to track
                              map.fitBounds(bounds);
                                 
                        }
                      });

}


function cargarMapaPublico(ficheroGpx) {
    
  var latlng = new google.maps.LatLng(majLat, majLon);
  
  var mapOptions = {
    zoom: 13,
       mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
        position: google.maps.ControlPosition.BOTTOM_CENTER
    },
    center: latlng
  };
  map = new google.maps.Map(document.getElementById('map'), mapOptions);
  markers = [];
  markers2 = [];  
  
  dibujarRuta(ficheroGpx);

  if((markersMapa!== null) && (markersMapa!=='null')) {
      for(var i=0; i<markersMapa.length; i++) {
        var latlng = new google.maps.LatLng(markersMapa[i]["lat"], markersMapa[i]["lon"]);      
      
        var marker = new google.maps.Marker({
            position: latlng,
            map:map,            
            details: 'publico',
            icon: "img/marker_r.png",
            draggable:false
        //    animation: google.maps.Animation.DROP            
        });  

            var txt = "<div  class='modListMap'><div  class='modListMap2'> <h2 style='color: white'>" + markersMapa[i]["titulo"] + "</h2> <h3 style='color: #D8D8D8'>" +markersMapa[i]["descripcion"] +"</h3></div></div>";

            infowindow = new google.maps.InfoWindow();

            google.maps.event.addListener(marker,'click', (function(marker,txt){ 
                    return function() {
                       infowindow.setContent(txt);
                       infowindow.open(map,marker);
                    };
                })(marker,txt)); 
        }

  }
}


function cargarMapaEditar(ficheroGpx) {
    
  var latlng = new google.maps.LatLng(majLat, majLon);
  
  var mapOptions = {
    zoom: 13,
       mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
        position: google.maps.ControlPosition.BOTTOM_CENTER
    },
    center: latlng
  };
  markers = [];
  markers2 = [];
  numMarkers = 0;
  map = new google.maps.Map(document.getElementById('map'), mapOptions);
  
  dibujarRuta(ficheroGpx);
    
  if((markersMapa!== null) && (markersMapa!=='null')) {
      for(var i=0; i<markersMapa.length; i++) {
        var latlng = new google.maps.LatLng(markersMapa[i]["lat"], markersMapa[i]["lon"]);
        var modf = false;
        var ico = "img/marker_r.png";        
        if(markersMapa[i]["email"]===idUser){            
            modf = true;
            ico = "img/marker_a.png";            
        }
        var marker = new google.maps.Marker({
            map:map,
            draggable:modf,
       //     animation: google.maps.Animation.DROP,    
            details: numMarkers,
            icon: ico,
            position: latlng
        });  
        
        if(markersMapa[i]["email"]===idUser) {
              var mark = new Object(); 
  
              mark['lat'] = markersMapa[i]["lat"];  
              mark['lon'] = markersMapa[i]["lon"];
              mark['details'] = numMarkers;
              mark['titulo'] = markersMapa[i]["titulo"];
              mark['resumen'] = markersMapa[i]["descripcion"];  
              mark['activo'] = true;  
              mark['email'] = idUser; 
              markers.push(mark);  
              markers2.push(marker);

              google.maps.event.addListener(marker, 'click', (function(marker) {
                  return function(event) {
                       placeMarker(event.latLng, marker);
                     };
              })(marker));

           google.maps.event.addListener(marker, 'mouseup', (function(marker) {   
                  return function(event) {
                       cambiarLocalizacionMarker(event.latLng, marker);
                     };
              })(marker));
 
        } else {
        
            var txt = "<div  class='modListMap'> <div  class='modListMap2'> <h2 style='color: white'>" + markersMapa[i]["titulo"] + "</h2> <h3 style='color: #D8D8D8'>" +markersMapa[i]["descripcion"] +"</h3></div></div>";

            infowindow = new google.maps.InfoWindow();

            google.maps.event.addListener(marker,'click', (function(marker,txt){ 
                    return function() {
                       infowindow.setContent(txt);
                       infowindow.open(map,marker);
                    };
                })(marker,txt)); 
        }
        numMarkers++;
     }
  }
}

function placeMarker(location,marker) {

  var vMar = "<div class='textAlert'>Titulo:</div>";
  vMar += "<input class='iUser' type='text' id='markerTitulo'/>";
  vMar += "<div class='textAlert'>Descripción del maker:</div>";
  vMar += "<textarea  class='iUser' style='width: 70%; height: 100px;' name='descripcion' id='descripcionMarker' maxlength='249'></textarea><br>";

   vMar += "<input id='close'  type='button' value=' guardar  ' onclick='javascript:addInfoMarker(" + location.lat() + "," + location.lng()+ "," + marker.details + ");'/>";
   vMar += "<input id='close'  type='button' value='Cancelar' onclick='salirEdiccionMarker();'/>";
   vMar += "<input id='close'  type='button' value='Borrar Marcador' onclick='borrarMarker(\""+marker.details+"\");'/>";

   document.getElementById("divAlerts").innerHTML=vMar;
   $("#divAlerts").css("display", "block");
   edicionPunto = true;

   for(var i=0; i<markers.length; i++) {       
        if(markers[i].details === marker.details) {
            document.getElementById("markerTitulo").value = markers[i]['titulo'];
            document.getElementById("descripcionMarker").value = markers[i]['resumen'];
        }
   }
}

function borrarMarker(details) {    
        for(var i=0; i<markers2.length; i++) {     
            if(markers2[i].details == details) {                
                markers2[i].setMap(null);                      
            }
        }
        for(var i=0; i<markers.length; i++) {      
            if(markers[i]['details'] == details) {                                 
                markers[i]['activo'] = false;
                // markers[i].setMap(null);                
            }
        }
        edicionMapa=true;
         $("#divAlerts").css("display", "none");

}

function salirEdiccionMarker() {
   $("#divAlerts").css("display", "none");   
}

function addInfoMarker(lat,lng, details) {

    for(var i=0; i<markers.length; i++) {
        if(markers[i].details == details) {
            markers[i]['titulo'] = document.getElementById("markerTitulo").value;
            markers[i]['resumen'] = document.getElementById("descripcionMarker").value;            
        }
    }
    $("#divAlerts").css("display", "none");
//    if(marker!== null){
//      var infowindow = new google.maps.InfoWindow({
//        content: 'Latitude: ' + location.lat() + '<br>Longitude: ' + location.lng()
//      });
//      infowindow.open(map,marker);
//    }
    
}