// ACCESO A LA BBDD

function getId(callback) {	
      $.getJSON('https://iescities.com/IESCities/api/entities/datasets/search?keywords=inRoute&offset=0&limit=1000', function(data) {		       
       var empObject = eval('(' + JSON.stringify(data) + ')');
       numId = empObject[0].datasetId;    
       callback();
    }); 
}



function addMapaBD(nombreMapa, descripcion, tipo) {
   var d = new Date();

   var mes = d.getMonth() + 1;

   var query = "INSERT INTO MAPAS (ID_USUARIOS,  TITULO, DESCRIPCION, TIPO, DATE) VALUES ('"  + idUser + "', '" + nombreMapa  + "', '" + descripcion + "', "  + tipo + ", '"  + d.getFullYear() + "-" + mes + "-" + d.getDate() + "');";     
   
    $.ajax({        
        url:'https://iescities.com/IESCities/api/data/update/' + numId + '/sql',           
        type:'POST', 
        data: query, 
        beforeSend: function (xhr){
            xhr.setRequestHeader('Authorization', 'CLAVE_PERMISOS');  // AÑADIR CLAVE CON PERMISOS DE MODFICIACIÓN
        },
        contentType: 'text/plain',


        error:function(jqXHR,text_status,strError){ 

            var message = JSON.stringify(jqXHR);

            var responseText = jQuery.parseJSON(jqXHR.responseText);

            navigator.notification.alert(
              'No ha sido posible crear el mapa. Intenteló más tarde, sentimos las molestias.', // message
               null,            // callback to invoke with index of button pressed
              'Error de conexión en el servidor',           // title
              'Cerrar' 
            );
        },
        success:function(data){
          setLog("AppProsume", "New map"); 
          navigator.notification.alert(
             'El mapa ha sido creado', // message
             null,            // callback to invoke with index of button pressed
             'Alta mapa',           // title
             'Cerrar'
           );
           Restlogging.appLog("AppProsume", "New map created");
           ultimoMapa(function(){
               editMapa();
                });
        }   
    });
}

function guadarMarker() {    
    if(document.getElementById('icoSave').src.indexOf("_off.png")<0){
        edicionMapa = false;
      document.getElementById("icoSave").src = "img/save_off.png";  
        borrarMarkersOlds(function(){
           addNewsMarkers(function(){               
                document.getElementById("icoSave").src = "img/save.png";     
            },mapa["id"]);
        },mapa["id"]);
    }    
}

function addNewsMarkers(callback,idMap) {
     var okSave = true;

     for(var i=0; i<markers.length; i++) {       

        if((markers[i]['activo'] === true) || (markers[i]['activo'] === 'true')) { 
            
            var query = "INSERT INTO MARKERS (COOR_LAT, COOR_LON, TITULO, DESCRIPCION, ID_MAPA, ID_USUARIO) VALUES ('"  + markers[i]['lat'] + "', '"  + markers[i]['lon'] + "', '"  + markers[i]['titulo'] + "', '" + markers[i]['resumen'] + "', "   +idMap + ",'" + idUser + "');";              
            
              $.ajax({        
                    url:'https://iescities.com/IESCities/api/data/update/' + numId + '/sql',           
                    type:'POST', 
                    data: query, 
                    beforeSend: function (xhr){
                        xhr.setRequestHeader('Authorization', 'CLAVE_PERMISOS');  // AÑADIR CLAVE CON PERMISOS DE MODFICIACIÓN
                    },
                    contentType: 'text/plain',


                    error:function(jqXHR,text_status,strError){ 

                        var message = JSON.stringify(jqXHR);

                         var responseText = jQuery.parseJSON(jqXHR.responseText);
                        document.getElementById("icoSave").src = "img/save.png";     
                     navigator.notification.alert(
                          'No ha sido posible modificar los marcador del mapa', // message
                           null,            // callback to invoke with index of button pressed
                          'Error de conexión en el servidor',           // title
                          'Cerrar' 
                       );
                        okSave= false;                        
                         callback(); 
                    },
                    success:function(data){
                          callback(); 
                    }
               });
        }
    }
    if(okSave){
        navigator.notification.alert(
                            'Se han guardado todos los marcadores en el mapa correctamente', // message
                            null,            // callback to invoke with index of button pressed
                            'Marcador guardados',           // title
                            'Cerrar' 
                        );
                 callback();
    }
        
   
}

function borrarMarkersOlds(callback,idMap) {
  var query = "DELETE FROM MARKERS WHERE ((ID_MAPA = " + idMap + ") AND (ID_USUARIO = '"+ idUser +"'));";  
  $.ajax({        
        url:'https://iescities.com/IESCities/api/data/update/' + numId + '/sql',           
        type:'POST', 
        data: query, 
        beforeSend: function (xhr){
            xhr.setRequestHeader('Authorization', 'CLAVE_PERMISOS');  // AÑADIR CLAVE CON PERMISOS DE MODFICIACIÓN
        },
        contentType: 'text/plain',


        error:function(jqXHR,text_status,strError){ 

            var message = JSON.stringify(jqXHR);

             var responseText = jQuery.parseJSON(jqXHR.responseText);

         navigator.notification.alert(
              'No ha sido posible modificar los marcadores del mapa', // message
               null,            // callback to invoke with index of button pressed
              'Error de conexión en el servidor',           // title
              'Cerrar' 
           );
            callback(); 
        },
        success:function(data){
           console.log("ok borrados marker de mapa correctamente");
           callback(); 
        }
   });
    
     
}

function borrarMapaBD(idMap) {
     loading();
    borrarMapa(function(){
           borrarMarkers(idMap);
        },idMap);
}

function borrarMapa(callback,idMap) {
  var query = "DELETE FROM MAPAS WHERE (ID = " + idMap + ");";  
  $.ajax({        
        url:'https://iescities.com/IESCities/api/data/update/' + numId + '/sql',           
        type:'POST', 
        data: query, 
        beforeSend: function (xhr){
            xhr.setRequestHeader('Authorization', 'CLAVE_PERMISOS');  // AÑADIR CLAVE CON PERMISOS DE MODFICIACIÓN
        },
        contentType: 'text/plain',


        error:function(jqXHR,text_status,strError){ 

            var message = JSON.stringify(jqXHR);

             var responseText = jQuery.parseJSON(jqXHR.responseText);

         navigator.notification.alert(
              'No ha borrar el mapa, intentalo más parte, por favor', // message
               null,            // callback to invoke with index of button pressed
              'Error de conexión en el servidor',           // title
              'Cerrar' 
           );
        },
        success:function(data){
           console.log("ok borrad mapa");
           callback(); 
        }
   });
    
     
}

function borrarMarkers(idMap) {
  var query = "DELETE FROM MARKERS WHERE (ID_MAPA = " + idMap + ");";  
  $.ajax({        
        url:'https://iescities.com/IESCities/api/data/update/' + numId + '/sql',           
        type:'POST', 
        data: query, 
        beforeSend: function (xhr){
            xhr.setRequestHeader('Authorization', 'CLAVE_PERMISOS');  // AÑADIR CLAVE CON PERMISOS DE MODFICIACIÓN
        },
        contentType: 'text/plain',


        error:function(jqXHR,text_status,strError){ 

            var message = JSON.stringify(jqXHR);

             var responseText = jQuery.parseJSON(jqXHR.responseText);

             console.log("Error borrado markers al eleminar el mapa");
        },
        success:function(data){
            navigator.notification.alert(
              'El mapa ha sido eliminado', // message
               null,            // callback to invoke with index of button pressed
              'Mapa borrado',           // title
              'Cerrar' 
            );
            misMapas();
        }
   });
    
     
}

function ultimoMapa(callback) {
     var query = "select * from MAPAS order by ID DESC limit 1";
      $.ajax({        
            url:'https://iescities.com/IESCities/api/data/query/' + numId + '/sql',
            type:'POST', 
            data: query, 
            contentType:'text/plain',    
            error:function(jqXHR,text_status,strError){ 
                var message = JSON.stringify(jqXHR);
                console.error(message);
             navigator.notification.alert(
                  'Intenteló más tarde, sentimos las molestias', // message
                   null,            // callback to invoke with index of button pressed
                  'Error de conexión en el servidor',           // title
                  'Cerrar' 
               );
               pagInicio();
               return true;
            },
            success:function(data){
                var empObjectLen = data.rows.length; 
                var empObject = eval('(' + JSON.stringify(data) + ')');                
                if(empObjectLen>0) {
                    mapa = new Object();
                    mapa["id"] = empObject.rows[0].ID;
                    mapa["email"] = empObject.rows[0].ID_USUARIOS;
                    mapa["titulo"] = empObject.rows[0].TITULO;
                    
                    mapa["descripcion"] = empObject.rows[0].DESCRIPCION;
                    mapa["tipo"] = empObject.rows[0].TIPO;
                    mapa["date"] = empObject.rows[0].DATE;                            
                }
            }
       });
       callback();   
}

// Obtenermos todos los mapas publicos (tipo 1 y 2) existente.
function obtenerMapasPublicos(callback) {    
     var query = "SELECT * FROM MAPAS WHERE (tipo =1 OR tipo =2)ORDER BY DATE DESC";
      $.ajax({        
            url:'https://iescities.com/IESCities/api/data/query/' + numId + '/sql',
            type:'POST', 
            data: query, 
            contentType:'text/plain',    
            error:function(jqXHR,text_status,strError){ 
                var message = JSON.stringify(jqXHR);
                console.error(message);
             navigator.notification.alert(
                  'Intenteló más tarde, sentimos las molestias', // message
                   null,            // callback to invoke with index of button pressed
                  'Error de conexión en el servidor',           // title
                  'Cerrar' 
               );
               pagInicio();
               callback();   
               return true;
            },
            success:function(data){                
                var empObjectLen = data.rows.length; 
                var empObject = eval('(' + JSON.stringify(data) + ')');                
                listMaps = new Array(empObjectLen);
                for (var i=0; i<empObjectLen; i++) {  
                    listMaps[i] = new Object();
                    listMaps[i]["id"] = empObject.rows[i].ID;
                    listMaps[i]["email"] = empObject.rows[i].ID_USUARIOS;                    
                    listMaps[i]["titulo"] = empObject.rows[i].TITULO;                           
                    listMaps[i]["descripcion"] = empObject.rows[i].DESCRIPCION;
                    listMaps[i]["tipo"] = empObject.rows[i].TIPO;
                    listMaps[i]["date"] = empObject.rows[i].DATE;                            
                }
                callback();   
            }
       });      
       
}


// Obtiene los mapas del usaurio
function obtenerMapasUsuario(callback) {    
     var query = "SELECT * FROM MAPAS WHERE (ID_USUARIOS = '"+ idUser +"')ORDER BY DATE DESC";
      $.ajax({        
            url:'https://iescities.com/IESCities/api/data/query/' + numId + '/sql',
            type:'POST', 
            data: query, 
            contentType:'text/plain',    
            error:function(jqXHR,text_status,strError){ 
                var message = JSON.stringify(jqXHR);
                console.error(message);
             navigator.notification.alert(
                  'Intenteló más tarde, sentimos las molestias', // message
                   null,            // callback to invoke with index of button pressed
                  'Error de conexión en el servidor',           // title
                  'Cerrar' 
               );
               pagInicio();
               callback();   
               return true;
            },
            success:function(data){                
                var empObjectLen = data.rows.length; 
                var empObject = eval('(' + JSON.stringify(data) + ')');                
                listMaps = new Array(empObjectLen);
                for (var i=0; i<empObjectLen; i++) {  
                    listMaps[i] = new Object();
                    listMaps[i]["id"] = empObject.rows[i].ID;
                    listMaps[i]["email"] = empObject.rows[i].ID_USUARIOS;                    
                    listMaps[i]["titulo"] = empObject.rows[i].TITULO;                           
                    listMaps[i]["descripcion"] = empObject.rows[i].DESCRIPCION;
                    listMaps[i]["tipo"] = empObject.rows[i].TIPO;
                    listMaps[i]["date"] = empObject.rows[i].DATE;                            
                }
                callback();   
            }
       });      
       
}

// UTILIZAR PARA OBTENER LOS MARKERS DE LOS MAPAS YA CREADOS!!!
function obtenerMarkers(callback, idMapa) {
     var query = "select * from MARKERS where ID_MAPA = " + idMapa;     
      $.ajax({        
            url:'https://iescities.com/IESCities/api/data/query/' + numId + '/sql',
            type:'POST', 
            data: query, 
            contentType:'text/plain',    
            error:function(jqXHR,text_status,strError){ 
                var message = JSON.stringify(jqXHR);
                console.error(message);
             navigator.notification.alert(
                  'Intenteló más tarde, sentimos las molestias', // message
                   null,            // callback to invoke with index of button pressed
                  'Error de conexión en el servidor',           // title
                  'Cerrar' 
               );
               pagInicio();
               callback();  
               return true;
            },
            success:function(data){
                var empObjectLen = data.rows.length; 
                var empObject = eval('(' + JSON.stringify(data) + ')');
                markersMapa = new Array(empObjectLen);
                for (var i=0; i<empObjectLen; i++) {  
                    markersMapa[i] = new Object();
                    markersMapa[i]["id"] = empObject.rows[i].ID;
                    markersMapa[i]["id_mapa"] = empObject.rows[i].ID_MAPA;
                    markersMapa[i]["titulo"] = empObject.rows[i].TITULO;
                    markersMapa[i]["descripcion"] = empObject.rows[i].DESCRIPCION;                            
                    markersMapa[i]["lat"] = empObject.rows[i].COOR_LAT;
                    markersMapa[i]["lon"] = empObject.rows[i].COOR_LON;
                    markersMapa[i]["email"] = empObject.rows[i].ID_USUARIO;
                }
                callback();  
            }
       });
        
}

function comprobarUser(callback) {

    var email = document.getElementById("emailInicio").value;
    var contrasena = document.getElementById("contrasenaInicio").value;
    contrasena = md5(contrasena);
    var query = "Select * from USUARIOS where EMAIL='" + email + "' and PASS='" + contrasena + "'";
    
    
  $.ajax({            
    type:"POST", 
    url:"http://150.241.239.51/public/InRoute/comprobarExiteBB.php",
    data:{sqlQuery:query},     
    error:function(jqXHR,text_status,strError){         
        var message = JSON.stringify(jqXHR);        
     navigator.notification.alert(
          'Intenteló más tarde, sentimos las molestias', // message
           null,            // callback to invoke with index of button pressed
          'Error de conexión en el servidor',           // title
          'Cerrar' 
       );
       callback();
       return true;
    },
    success:function(data){
        if(data!=="false") {            
            idUser = data;            
            window.localStorage.setItem("idUser", idUser);
            window.localStorage.setItem("nombreUser", nombreUser); 
            emailUser = email;
            window.localStorage.setItem("emailUser", emailUser); 
            pass = contrasena;
            window.localStorage.setItem("pass", pass);    
            document.getElementById("icoAccUser").src="img/accesoUsuario_off.png";
            document.getElementById("icoPerfil").src="img/cerrarSesion.png";  
            document.getElementById("icoNuevoMapa").src="img/nuevoMapa.png";
            document.getElementById("icoMisMapas").src="img/MisMapas.png";              
            pagInicio();
        }
        else {
             navigator.notification.alert(
                 'El usuario o la contraseña son incorrectas', // message
                 null,            // callback to invoke with index of button pressed
                 'Fallo de acceso',           // title
                 'Cerrar' 
               );
        }
        callback();
    }
  });
  
}

function comprobarExisteUser(email,contrasena) {

    var query = "Select * from USUARIOS where EMAIL='" + email + "' and PASS='" + contrasena + "'";
    
   
  $.ajax({            
    type:"POST", 
    url:"http://150.241.239.51/public/InRoute/comprobarExiteBB.php",
    data:{sqlQuery:query},     
    error:function(jqXHR,text_status,strError){         
        var message = JSON.stringify(jqXHR);        
        console.log("error comprobaExiteUser : " + message);

    },
    success:function(data){
        if(data!=="false") {
            idUser = data;
            window.localStorage.setItem("idUser", idUser);            
        }      
    }
  });
  
}


function crearUser(callback) {
    var email = document.getElementById("email").value;
    var query = "Select * from USUARIOS where EMAIL='" + email + "'";
  thereUser=true;  
    $.ajax({            
    type:"POST", 
    url:"http://150.241.239.51/public/InRoute/comprobarExiteBB.php",
    data:{sqlQuery:query},     
    error:function(jqXHR,text_status,strError){         
        var message = JSON.stringify(jqXHR);        
     navigator.notification.alert(
          'Intenteló más tarde, sentimos las molestias', // message
           null,            // callback to invoke with index of button pressed
          'Error de conexión en el servidor',           // title
          'Cerrar' 
       );
       callback();
       return true;
    },
    success:function(data){
        if(data!=="false") {
            idUser = data;
            navigator.notification.alert(
                       'Ese email ya existe está dado de alta en el sistema', 
                        null,         
                        'Ya esta registrado ese email',           
                        'Aceptar'
                    );
        }
        else {                        
           var contrasena = document.getElementById("contrasena").value;     
           contrasena = md5(contrasena);
           var nombre = document.getElementById("nombre").value;
            
            query = "INSERT INTO USUARIOS (NOMBRE,  EMAIL, PASS) VALUES ('"  + nombre + "', '" + email + "', '"   +contrasena + "');";      
            setNewUser(query, nombre, email, contrasena);
        }
        callback();
    }
  });  
  
} 


function setNewUser(query, nombre,  email, contrasena) {

 $.ajax({            
    type:"POST", 
    url:"http://150.241.239.51/public/InRoute/comprobarExiteBB.php",
    data:{sqlQuery:query},         
    error:function(jqXHR,text_status,strError){ 

        var message = JSON.stringify(jqXHR);
       
         var responseText = jQuery.parseJSON(jqXHR.responseText);
     navigator.notification.alert(
          'No ha sido posible añadir tu usuario. Intenteló más tarde, sentimos las molestias.', // message
           null,            // callback to invoke with index of button pressed
          'Error de conexión en el servidor',           // title
          'Cerrar' 
       );
    },
    success:function(data){
      navigator.notification.alert(
         'Usuario dado de alta', // message
         null,            // callback to invoke with index of button pressed
         'Alta usuarios',           // title
         'Cerrar'
       );
        nombreUser = nombre;
        window.localStorage.setItem("nombreUser", nombreUser); 
        emailUser = email;
        window.localStorage.setItem("emailUser", emailUser); 
        pass = contrasena;
        window.localStorage.setItem("pass", pass);         
        
        setTimeout(function(){ comprobarExisteUser(email,contrasena); }, 500);       
       
        
        pagInicio();
    }
   
});
}



