var xhReq = new XMLHttpRequest();
var userLat = 40.474218;
var userLon = -3.869158;
var majLat = 40.474218;
var majLon = -3.869158;
var userLat2 = 40.464218;
var userLon2 = -3.879158;
var numId= 251;
var markers = [];
var markers2 = [];
var markerUser=null;
var emailUser = (window.localStorage.getItem("emailUser")!==null)?window.localStorage.getItem("emailUser"):null;
var idUser = (window.localStorage.getItem("idUser")!==null)?window.localStorage.getItem("idUser"):null;
var nombreUser = (window.localStorage.getItem("nombreUser")!==null)?window.localStorage.getItem("nombreUser"):null;
var pass = (window.localStorage.getItem("pass")!==null)?window.localStorage.getItem("pass"):null;
var mapa = null;
var listMaps = null;
var markersMapa = null;
var numMarkers = 0;
var goMenu = false;
var ayuda = false;
var edicionPunto = false;
var edicionMapa = false;
var numSessionLog=1;
var iMapa = null;
var tituloMapa = null;



var app = {
    // Application Constructor
    initialize: function() {
startRatingSurvey();
       var htMap = "<div id='ranting'  style='text-align:left'><a  href='javascript:startRatingSurvey(\"true\");'><img class='logo' src='img/estrellas.png' /></a></div>";
       htMap += "<div id='divHelp'></div><div id='map'></div>";
       document.getElementById("contenidoCuerpo").innerHTML= htMap;
       goMenu = true;
       
       initialize();

    
$(document).ready(function() {
    
    $("#nav").ferroMenu({
        position    : "right-center",
        
       drag : false,
       delay       : 50,      
       radius      : 100,
       rotation    : 0, 
       margin      : 5,
       opened      : false
    });    
    if((emailUser!==null) && (emailUser!=='null')){
        document.getElementById("icoAccUser").src="img/accesoUsuario_off.png";
        document.getElementById("icoPerfil").src="img/cerrarSesion.png";        
    }
    else
    {
        document.getElementById("icoNuevoMapa").src="img/nuevoMapa_off.png";
        document.getElementById("icoMisMapas").src="img/MisMapas_off.png";                
    }
    getGps();

});
       this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};

function getGps() {    
	navigator.geolocation.getCurrentPosition(getGpsonSuccess,getGpsonError);         
} 
function getGpsonSuccess(position) {
  userLat = position.coords.latitude;
  userLon = position.coords.longitude;	        
}

function getGpsonError(error) { console.log('code: ' +error.code+ '\n' + 'message: ' +error.message+ '\n'); }


function changeImage(element, img, img2) {
       if(document.getElementById(element).src.indexOf("_off.png")<0){        
            document.getElementById(element).src = "img/" + img;     
            setTimeout(function(){document.getElementById(element).src = "img/" + img2;},400);
        }
 }
 
 function inicioSesion(){
     if(document.getElementById('icoAccUser').src.indexOf("_off.png")<0){

        $.fn.ferroMenu.toggleMenu("#nav");
        
        
        //document.getElementById('ferromenu-controller-0').style.visibility = 'hidden'; 
        document.getElementById('idFerro').style.visibility = 'hidden';  

        document.getElementById('nav').style.visibility = 'hidden';    

        xhReq.open("GET", "pags/inicioSesion.html", false);      
        xhReq.send(null);   
        document.getElementById("contenidoCuerpo").innerHTML=xhReq.responseText;
        goMenu = false;
        if((emailUser!==null) && (emailUser!=='null')){
          document.getElementById("emailInicio").value = emailUser;
        //  document.getElementById("contrasenaInicio").value = pass;
        }
//        document.getElementById("contenidoCuerpo").className = 'page transition center';
//        document.getElementById("menuprincipal").className = 'page transition center';

    }
    else {        
        infoHelp2();        
    }
    //}
 }
 
 function editMapa() {     

            $.fn.ferroMenu.toggleMenu("#nav");
            document.getElementById('idFerro').style.visibility = 'hidden';  
           //document.getElementById('ferromenu-controller-0').style.visibility = 'hidden'; 

            document.getElementById('nav').style.visibility = 'hidden';   
              
            var editMap = "<div id='menugen'></div>";
            editMap += "<div id='divAlerts'></div>";
            editMap += "<div id='divHelp'></div>";
            editMap += "<div id='menu3'><a href='javascript:changeImage(\"icoAddMarker\",\"addMarker_off.png\",\"addMarker.png\");addMarkerMap();'><img align='right' id='icoAddMarker' class='logo' src='img/addMarker.png' /></a></div>";
            editMap += "<div id='menu4'> <a href='javascript:guadarMarker();'><img align='right' id='icoSave' class='logo' src='img/save.png' /></a></div>"; // if(document.getElementById('icoSave').src.indexOf('_off.png')<0)
            editMap += "<div id='menu5'> <a href='javascript:changeImage(\"icoHelp\",\"help_off.png\",\"help.png\");infoHelp();'><img align='right' id='icoHelp' class='logo' src='img/help.png' /></a></div>";
            editMap += "<div id='menu6'> <a href='javascript:cerrarEdicion();'><img align='right' id='icoCerrar' class='logo' src='img/close.png' /></a></div>";
            editMap += "<div id='map'></div>";
            editMap += "<div id=\"posMap\"> <a href=\"javascript:busPos();\"><img align=\"right\" id=\"logoMap\" class=\"logoMap\" src=\"img/map.png\" /></a></div>";
            $("#divAlerts").css("display", "none");
            $("#divHelp").css("display", "none");
            
             document.getElementById("contenidoCuerpo").innerHTML=editMap;
           //  goMenu = false;
             mapaEdicion();     
 }
 
 function infoHelp() {
    xhReq.open("GET", "pags/help.html", false);      
    xhReq.send(null); 
    ayuda=true;
  
    document.getElementById("divHelp").innerHTML=xhReq.responseText;;
    
    $("#divHelp").css("display", "block");
 }
 
 
 function cerrarHelp() {
     $("#divHelp").css("display", "none");    
 }
 
 
 function infoHelp2() {
    $.fn.ferroMenu.toggleMenu("#nav");
    document.getElementById('idFerro').style.visibility = 'hidden';  
    document.getElementById('nav').style.visibility = 'hidden';  
    xhReq.open("GET", "pags/help2.html", false);      
    xhReq.send(null);   
  
    document.getElementById("divHelp").innerHTML=xhReq.responseText;;
    
    $("#divHelp").css("display", "block");
 }
 
  function cerrarHelp2() {      
     $("#divHelp").css("display", "none");    
     document.getElementById('idFerro').style.visibility = 'visible'; 
     document.getElementById('nav').style.visibility = 'visible'; 
 }
 
 function opcionSalir(button)
 {
     
    if(button !== 1) {              
          edicionMapa=false;
          $("#contenidoCuerpo").css("opacity", "1"); 
          cerrarEdicion1(iMap);
    }
    else {
         $("#contenidoCuerpo").css("opacity", "1");          
    }
 }
 
 
 function cerrarEdicion1(indMapa){
     
       if(edicionMapa)
       {   
            iMap = indMapa;
            $("#contenidoCuerpo").css("opacity", "0.50");
                       navigator.notification.confirm (
                      '¿Quieres salir de la edición, sin guardar los cambios? ', // message
                      opcionSalir,            // callback to invoke with index of button pressed
                      'Mapa no guardado',           // title
                      'No, Si'
                ); 
       }
       else 
       {
         document.getElementById('idFerro').style.visibility = 'visible'; 
         document.getElementById('nav').style.visibility = 'visible'; 
         var editMap = "<div id='divHelp'></div>";
         editMap += "<div id='menugenTitulo'>" + listMaps[indMapa]["titulo"] + " </div>";
         editMap += "<div id='menugenEdit'></div>";
         editMap += "<div id='menuEdit'><a href='javascript:changeImage(\"icoEdit\",\"edit_off.png\",\"edit.png\");editMapaSel("+indMapa+");'><img align='right' id='icoEdit' class='logo' src='img/edit.png' /></a></div>";
         editMap += "<div id='map'></div>";
         editMap += "<div id=\"posMap\"> <a href=\"javascript:busPos();\"><img align=\"right\" id=\"logoMap\" class=\"logoMap\" src=\"img/map.png\" /></a></div>";
         document.getElementById("contenidoCuerpo").innerHTML= editMap;
         goMenu = true;

         obtenerMarkers(function(){
                cargarMapaPublico(listMaps[indMapa]["titulo"]);
          },mapa["id"]);
       }
 }
 
  
 function cerrarEdicion(){
     
    //  $.fn.ferroMenu.toggleMenu("#nav");
     // document.getElementById('ferromenu-controller-0').style.visibility = 'visible'; 
     document.getElementById('idFerro').style.visibility = 'visible'; 
     document.getElementById('nav').style.visibility = 'visible'; 
     var editMap = "<div id='divHelp'></div>";     
      editMap += "<div id='map'></div>";
      editMap += "<div id=\"posMap\"> <a href=\"javascript:busPos();\"><img align=\"right\" id=\"logoMap\" class=\"logoMap\" src=\"img/map.png\" /></a></div>";
      document.getElementById("contenidoCuerpo").innerHTML= editMap;
      goMenu = true;
   //   dibujarRuta(listMaps[indMapa]["titulo"]);

     obtenerMarkers(function(){
            cargarMapaPublico(tituloMapa);  // OJO MIRAR
     },mapa["id"]);
 }
 
  function nuevoMapa() {     
       if(document.getElementById('icoNuevoMapa').src.indexOf("_off.png")<0){

            $.fn.ferroMenu.toggleMenu("#nav");
            //document.getElementById('ferromenu-controller-0').style.visibility = 'hidden'; 
            document.getElementById('idFerro').style.visibility = 'hidden';  
            document.getElementById('nav').style.visibility = 'hidden';    


            xhReq.open("GET", "pags/nuevoMapa.html", false);      
            xhReq.send(null);   
            document.getElementById("contenidoCuerpo").innerHTML=xhReq.responseText;
            goMenu = false;

//            document.getElementById("contenidoCuerpo").className = 'page transition center';
  //          document.getElementById("menuprincipal").className = 'page transition center';

            //document.getElementById("map").style = 'page transition right';      
       }     
 }
 
  function mapasPublicos() {     
       if(document.getElementById('icoMapasPublicos').src.indexOf("_off.png")<0){
            $.fn.ferroMenu.toggleMenu("#nav");
            //document.getElementById('ferromenu-controller-0').style.visibility = 'hidden'; 
            document.getElementById('idFerro').style.visibility = 'hidden';  

            document.getElementById('nav').style.visibility = 'hidden';    

            loading();
            var lMaps = "";
            obtenerMapasPublicos(function(){                
                xhReq.open("GET", "pags/listaMapaPublicos.html", false);      
                xhReq.send(null);   
                document.getElementById("contenidoCuerpo").innerHTML=xhReq.responseText;
                goMenu = false;
                if((listMaps!==null) && (listMaps!=='null')) {
                    for(var i=0; i< listMaps.length; i++) {                        
                        lMaps+="<div  class='modListMap' id='divSel"+i+"' onclick='javascript:cargaMapaSel("+i+",\""+listMaps[i]["tipo"]+"\");'>";
                        lMaps+="<h2 style='color: white'>" + listMaps[i]["titulo"]  +"</h2>";                                       
                        lMaps+="<h3 style='color: #D8D8D8'> " + listMaps[i]["descripcion"] + "</h3>";        
                        if(listMaps[i]["tipo"]==='0')
                            lMaps+="<h3><span style='color: #e0e0e0'> Tipo: </span><span style='color: #FF0000'> Privado </span></h3>";                        
                        if(listMaps[i]["tipo"]==='1')
                            lMaps+="<h3><span style='color: #e0e0e0'> Tipo: </span><span style='color: #FF8000'> Público </span></h3>";
                        if(listMaps[i]["tipo"]==='2')
                            lMaps+="<h3 margin-left: 20px;><span style='color: #e0e0e0'> Tipo: </span><span style='color: #00FF00'> Colaborativo </span></h3>";   
                        lMaps+="</div>";
                    }
                }
                document.getElementById("listMapIni").innerHTML=lMaps;
            });
       }     
 }
 
 function loading() {
    
    document.getElementById("contenidoCuerpo").innerHTML="<div align=\"center\" style=\" position: absolute;	top: 50%; left:50%;	height: 30%;	width: 50%;	margin: -15% 0 0 -25%;\" ><img  src=\"img/loading2.gif\" /></div>";
}
 
 
   function misMapas() {     
       if(document.getElementById('icoMisMapas').src.indexOf("_off.png")<0){

            $.fn.ferroMenu.toggleMenu("#nav");
            //document.getElementById('ferromenu-controller-0').style.visibility = 'hidden'; 
            document.getElementById('idFerro').style.visibility = 'hidden';  

            document.getElementById('nav').style.visibility = 'hidden';    
            
            loading();
            obtenerMapasUsuario(function(){                
                xhReq.open("GET", "pags/listaMapaPublicos.html", false);      
                xhReq.send(null);   
                document.getElementById("contenidoCuerpo").innerHTML=xhReq.responseText;
                goMenu = false;
                var lMaps = "";
                if((listMaps!==null) && (listMaps!=='null')) {
                    for(var i=0; i< listMaps.length; i++) {                        
                        
                        lMaps+="<div  class='modListMap' id='divSel"+i+"'>";                        
                        lMaps+="<a href='javascript:changeImage(\"icoDelMap"+i+"\",\"Delete_Icon_off.png\",\"Delete_Icon.png\");deleteMap("+i+");'><img align='right' id='icoDelMap"+i+"' class='logo' src='img/Delete_Icon.png' /></a>";
                        lMaps+="<div onclick='javascript:cargaMapaSel("+i+",\"2\");'>";
                        lMaps+="<h2 style='color: white'>" + listMaps[i]["titulo"]  +"</h2>";                                       
                        lMaps+="<h3 style='color: #D8D8D8'> " + listMaps[i]["descripcion"] + "</h3>";        
                        if(listMaps[i]["tipo"]==='0')
                            lMaps+="<h3><span style='color: #e0e0e0'> Tipo: </span><span style='color: #FF0000'> Privado </span></h3>";                        
                        if(listMaps[i]["tipo"]==='1')
                            lMaps+="<h3><span style='color: #e0e0e0'> Tipo: </span><span style='color: #FF8000'> Público </span></h3>";
                        if(listMaps[i]["tipo"]==='2')
                            lMaps+="<h3 margin-left: 20px;><span style='color: #e0e0e0'> Tipo: </span><span style='color: #00FF00'> Colaborativo </span></h3>";   
                        lMaps+="</div></div>";
                    }
                    if(listMaps.length === 0) {
                        lMaps += "<div id='mensajeInfo'><div id='magin1'>Actualmente no tienes ningún mapa creado, pulsa sobre el icono de \"nuevo mapa\" si quieres crear uno nuevo :<br><br><a href='javascript:nuevoMapa();'><img alt='' src='img/nuevoMapa.png' style='width: 100px; height: 100px; float: center;' /></a></div></div>";   
                    }
                }
                else {
                    lMaps += "<div id='mensajeInfo'><div id='magin1'>Actualmente no tienes ningún mapa creado, pulsa sobre el icono de \"nuevo mapa\" si quieres crear uno nuevo :<br><br><a href='javascript:nuevoMapa();'><img alt='' src='img/nuevoMapa.png' style='width: 100px; height: 100px; float: center;' /></a></div></div>";   
                }
                document.getElementById("listMapIni").innerHTML=lMaps;
                document.getElementById("menu").innerHTML="Mis mapas";
            });
       }     
 }
 
 function deleteMap(idMap){
     
     var txt = "¿Seguro que quieres borrar el mapa \""+ listMaps[idMap]["titulo"] +"\"?";
     
      navigator.notification.confirm (
                      txt, // message
                      function(button) {
            if ( button !== 1 ) {
                borrarMapaBD(listMaps[idMap]["id"]);
                
            }},
                      'Borrar mapa',           // title
                      'No, Si'
             );       
 }
 
 function cagarValores(indMapa) {
     mapa = new Object();
     mapa["id"] = listMaps[indMapa]["id"];
     mapa["email"] = listMaps[indMapa]["email"];
     mapa["titulo"] = listMaps[indMapa]["titulo"];                    
     mapa["descripcion"] = listMaps[indMapa]["descripcion"];
     mapa["tipo"] = listMaps[indMapa]["tipo"];
     mapa["date"] = listMaps[indMapa]["date"];                     
 }
 
 function cargaMapaSel(indMapa, tipo) {
     document.getElementById("divSel"+indMapa).style.background = "#848484";
     

    

  //  Restlogging.appCustom("Map access");
  
//      Restlogging.appLog("AppConsume", "Map access");

     cagarValores(indMapa);


     var editMap ="";
     editMap += "<div id='divHelp'></div>";
     editMap += "<div id='menugenTitulo'>" + listMaps[indMapa]["titulo"] + " </div>";     
     if(((tipo==='1') || (tipo==='2')) && (emailUser !== null) && (emailUser !== 'null')){
         editMap += "<div id='menugenEdit'></div>";
         editMap += "<div id='menuEdit'><a href='javascript:changeImage(\"icoEdit\",\"edit_off.png\",\"edit.png\");editMapaSel("+indMapa+");'><img align='right' id='icoEdit' class='logo' src='img/edit.png' /></a></div>";
     }
     
     editMap += "<div id='map'></div>";
     editMap += "<div id=\"posMap\"> <a href=\"javascript:busPos();\"><img align=\"right\" id=\"logoMap\" class=\"logoMap\" src=\"img/map.png\" /></a></div>";

     document.getElementById("contenidoCuerpo").innerHTML=editMap;
     setLog("AppConsume", "Map access");
     goMenu = true;

    //document.getElementById('ferromenu-controller-0').style.visibility = 'visible'; 
    document.getElementById('idFerro').style.visibility = 'visible'; 
    document.getElementById('nav').style.visibility = 'visible';
    
    tituloMapa= listMaps[indMapa]["titulo"];
    
   //  dibujarRuta(listMaps[indMapa]["titulo"]);
      obtenerMarkers(function(){          
                   cargarMapaPublico(listMaps[indMapa]["titulo"]);
               },listMaps[indMapa]["id"]);
               
               // ---------------------------------------------------------------------------------------------------------
               // /// OJO AL AÑADIR LOS MARKER TENER EN CUENTA QUE SI ES COLABORAIVO SE PODRA AÑADIR MÁS MARKER, SOLO AÑADIR
               // LA MANERA QUE TENDREMOS DE SABER CUALES DON LOS ANTIGUOS Y LOS NUEVOS SERÁ POR LA PROMPIEDAD DETAILS !!!
               // ----------------------------------------------------------------------------------------------------------
//     alert("mapa");
//     alert(indMapa);
 }
 
  function editMapaSel(indMapa) {     
            // $.fn.ferroMenu.toggleMenu("#nav");     
            document.getElementById('idFerro').style.visibility = 'hidden';  
            document.getElementById('nav').style.visibility = 'hidden';   

              
            var editMap = "<div id='menugen'></div>";
            editMap += "<div id='menugenTitulo'>" + listMaps[indMapa]["titulo"] + " </div>";
            editMap += "<div id='divHelp'></div>";
            editMap += "<div id='divAlerts'></div>";
            editMap += "<div id='menu3'><a href='javascript:changeImage(\"icoAddMarker\",\"addMarker_off.png\",\"addMarker.png\");addMarkerMap();'><img align='right' id='icoAddMarker' class='logo' src='img/addMarker.png' /></a></div>";
            editMap += "<div id='menu4'> <a href='javascript:guadarMarker();'><img align='right' id='icoSave' class='logo' src='img/save.png' /></a></div>"; // changeImage(\"icoSave\",\"save_off.png\",\"save_off.png\");
            editMap += "<div id='menu5'> <a href='javascript:changeImage(\"icoHelp\",\"help_off.png\",\"help.png\");infoHelp();'><img align='right' id='icoHelp' class='logo' src='img/help.png' /></a></div>";
            editMap += "<div id='menu6'> <a href='javascript:cerrarEdicion1(\"" + indMapa + "\");'><img align='right' id='icoCerrar' class='logo' src='img/close.png' /></a></div>";
            editMap += "<div id='map'></div>";
            $("#divAlerts").css("display", "none");
            $("#divHelp").css("display", "none");

             document.getElementById("contenidoCuerpo").innerHTML=editMap;
             dibujarRuta(listMaps[indMapa]["titulo"]);
             //goMenu = false;
      obtenerMarkers(function(){          
                   cargarMapaEditar(listMaps[indMapa]["titulo"] );
           },listMaps[indMapa]["id"]);     
       
 }
 
 
 
 function altaUser(){
     
     if($.fn.ferroMenu.isOpen()){     
        if((emailUser!==null) && (emailUser!=='null')){

               nombreUser = null;
               window.localStorage.setItem("nombreUser", nombreUser); 
               emailUser = null;
               window.localStorage.setItem("emailUser", emailUser); 
               pass = null;
               window.localStorage.setItem("idUser", idUser); 
               idUser = null;                              
               window.localStorage.setItem("pass", pass); 
               document.getElementById("icoAccUser").src="img/accesoUsuario.png";
               document.getElementById("icoPerfil").src="img/Perfil.png";
               document.getElementById("icoNuevoMapa").src="img/nuevoMapa_off.png";
               document.getElementById("icoMisMapas").src="img/MisMapas_off.png"; 
               $.fn.ferroMenu.toggleMenu("#nav");

        }
        else {
           $.fn.ferroMenu.toggleMenu("#nav");
           //document.getElementById('ferromenu-controller-0').style.visibility = 'hidden'; 
           document.getElementById('idFerro').style.visibility = 'hidden';    

           document.getElementById('nav').style.visibility = 'hidden';    

           xhReq.open("GET", "pags/datosUser.html", false);      
           xhReq.send(null);              
           document.getElementById("contenidoCuerpo").innerHTML=xhReq.responseText;           
           goMenu = false;
           if((nombreUser!== null) && (nombreUser!=='null') && (emailUser!==null) && (emailUser!=='null')){
             document.getElementById("nombre").value = nombreUser;
             document.getElementById("email").value = emailUser;
           }

   //        document.getElementById("contenidoCuerpo").className = 'page transition center';
   //        document.getElementById("menuprincipal").className = 'page transition center';
        //   document.getElementById("map").className = 'page transition right';            
    }
    }
 }
 
 
 function pagInicio() {
     document.getElementById("contenidoCuerpo").innerHTML="<div id='divHelp'></div><div id='map'></div>";
     goMenu = true;
//     document.getElementById("map").innerHTML="";
     initialize();
        //document.getElementById("contenidoCuerpo").className = 'page transition right';
        //document.getElementById("menuprincipal").className = 'page transition right';
    //    document.getElementById("map").className = 'page transition center';
        //document.getElementById('ferromenu-controller-0').style.visibility = 'visible'; 
        document.getElementById('idFerro').style.visibility = 'visible';         
         document.getElementById('nav').style.visibility = 'visible';    
      
     if((emailUser!==null) && (emailUser!=='null')){      
        document.getElementById("icoAccUser").src="img/accesoUsuario_off.png";
        document.getElementById("icoPerfil").src="img/cerrarSesion.png"; 
        document.getElementById("icoNuevoMapa").src="img/nuevoMapa.png";
        document.getElementById("icoMisMapas").src="img/MisMapas.png";  
     }
     else
     {         
        document.getElementById("icoNuevoMapa").src="img/nuevoMapa_off.png";
        document.getElementById("icoMisMapas").src="img/MisMapas_off.png";                
     }
 }

function toggleBounce(details) {

    for (var i =0; i < markers.length; i++) {

      if(markers[i].details==details){
        if (markers[i].getAnimation() != null) {
          markers[i].setAnimation(null);
         
//         map.markers[i].setMap(null);
        } else {
        //    alert("Desplaza el icono en posición que desees y pulsa una vez sobre el!!!");
            markers[i].setAnimation(google.maps.Animation.BOUNCE);
        }
      }
    }

 
}

function mensajePrivacidad() {
    navigator.notification.alert(
        'Las leyes sobre la privacidad es el área del derecho que se ocupa de la protección y preservación de los derechos a la privacidad de las personas. Cada vez con mayor asiduidad los gobiernos y otras organizaciones tanto públicas como privadas recolectan grandes cantidades de información personal sobre las personas para una gran variedad de propósitos. Las leyes sobre la privacidad regulan que tipo de información puede ser recolectada y como es que dicha información debe ser utilizada y almacenada. El ámbito de aplicabilidad de las leyes sobre la privacidad a veces es denominado expectativa de privacidad.', 
        null,         
        'Política de privacidad',           
        'Aceptar'
    );
    
}

function changePublico(){

    if(document.getElementById("myonoffswitch1").checked){
        document.getElementById("myonoffswitch2").disabled = false;        
    }
    else {
        document.getElementById("myonoffswitch2").disabled = true;
        document.getElementById("myonoffswitch2").checked  = false;
    }
        
}

function selectLicencia() {
    if(aceptLicencia.checked) {
        document.getElementById('btRegistro').disabled = false;        
        document.getElementById("btRegistro").style.background = "#2B72CC";
        document.getElementById("btRegistro").style.background = "-webkit-linear-gradient(top, #4c9bbc, #275c98)";
    }
    else {
        document.getElementById('btRegistro').disabled = true;
        document.getElementById("btRegistro").style.background =  "#cedce7";
        document.getElementById("btRegistro").style.background =  "-webkit-linear-gradient(top, #cedce7, #596a72)";    
    }
}

function iniSession() {
    var contrasena = document.getElementById("contrasenaInicio").value;
    if(contrasena.length<4) {
       navigator.notification.alert(
          'Por favor, introduzca una contraseña de más de 4 caracteres', 
          null,         
          'contraseña demasiado corta',           
          'Aceptar'
        );
    }
    else
    {
         // getId(function() {
              comprobarUser(function(){
          });
      //});        
            
    }
}


 function editarMapa() {
     document.getElementById("contenidoCuerpo").innerHTML="<div id='divHelp'></div><div id='map'></div>";
     goMenu = true;
      $.fn.ferroMenu.toggleMenu("#nav");
//      document.getElementById('ferromenu-controller-0').style.visibility = 'hidden'; 
        document.getElementById('idFerro').style.visibility = 'hidden';          
        document.getElementById('nav').style.visibility = 'hidden';    
//     document.getElementById("map").innerHTML="";
     initialize();
         
 }

function addMapa() {    
    
    var nombreMapa = document.getElementById("nuevoMapa").value;
    var descripcion = document.getElementById("descripcionMapa").value;
    var bMapaPublico = document.getElementById("myonoffswitch1").checked;
    var bMapaColaborativo = document.getElementById("myonoffswitch2").checked;
    if(nombreMapa.length<1) {
        navigator.notification.alert(
          'Es necesario que introduzca el nombre del mapa', 
          null,         
          'Nombre de mapa requerido',           
          'Aceptar'
        );
    }
    else {
        var tipo = 0;
        if(bMapaPublico) {
            tipo=1;
            if(bMapaColaborativo)
                tipo=2;
        }        
        loading();
        addMapaBD(nombreMapa, descripcion, tipo);        
        
    }
}

function setLog(typeMens, message) {
  var d = new Date();
  var n = d.getTime();	
  n = n / 1000;  

  
  var urlAj = "https://iescities.com/IESCities/api/log/app/event/" + n + "/Majadahonda In-Route/" + numSessionLog + "/" + typeMens + "/" + message;
  $.ajax({        
 	url: urlAj,       
    type:'POST', 
    data: '', 
    dataType:'json', 
    error:function(jqXHR,text_status,strError){         
        
      console.log('No connection IESCities api log');

    },
    success:function(data){      

    }
  });  
}


function registrarUser() {
    // VER COMO OBTNER ESTE VALOR !!! var sexo = document.getElementById("buttonHolder").value;
    var contrasena = document.getElementById("contrasena").value;
    var contrasenaConf = document.getElementById("contrasenaConf").value;
    if(contrasena.length<4) {
       navigator.notification.alert(
          'Por favor, introduzca una contraseña de más de 4 caracteres', 
          null,         
          'contraseña demasiado corta',           
          'Aceptar'
        );
    }
    else
    {
        if(contrasena !== contrasenaConf)
        {          
            navigator.notification.alert(
              'Las contraseñas deben de coincidir. Vuelve a introducirlas, por favor', 
              null,         
              'La contraseña no coinciden',           
              'Aceptar'
            );
        }else
        {
            getId(function() {
                crearUser(function(){          
            });});
        
            // GESTIONAR ALTA!!! pagInicio();
        }
    }
}
