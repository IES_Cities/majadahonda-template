// Apache Software License 2.0 (http://www.apache.org/licenses/)

// Declaración de variables globales
var cuerpo, menuprincipal, wrapper, estado, contenidoCuerpo, contenidoRotation, numId, numSessionLog;
// Guardamos en variables elementos para poder rescatarlos después sin tener que volver a buscarlos
cuerpo = document.getElementById("cuerpo"),
menuprincipal = document.getElementById("menuprincipal"),
contenidoCuerpo = document.getElementById("contenidoCuerpo"),
contenidoRotation = document.getElementById("contenidoRotation"),
wrapper = document.getElementById("wrapper");
var xhReq = new XMLHttpRequest();

var nickUser = window.localStorage.getItem("nickUser");
var vRanting = (window.localStorage.getItem("vRanting")!==null)?window.localStorage.getItem("vRanting"):"true";

var bday = window.localStorage.getItem("bday");
var dateEvent = window.localStorage.getItem("dateEvent");
var dateEventFormat = window.localStorage.getItem("dateEventFormat");
var goEvent1 = new Array();
if((window.localStorage.getItem("goEvent")!==null) || (window.localStorage.getItem("goEvent")!==''))
    goEvent1 = window.localStorage.getItem("goEvent");
var goEvent = new Array();
if(goEvent1!==null)
   goEvent = JSON.parse(goEvent1);
var botStatus = {
  "botStatus1" : (window.localStorage.getItem("preference1")!==null)?window.localStorage.getItem("preference1"):"1",
  "botStatus2" : (window.localStorage.getItem("preference2")!==null)?window.localStorage.getItem("preference2"):"1",
  "botStatus3" : (window.localStorage.getItem("preference3")!==null)?window.localStorage.getItem("preference3"):"1",
  "botStatus4" : (window.localStorage.getItem("preference4")!==null)?window.localStorage.getItem("preference4"):"1",
  "botStatus5" : (window.localStorage.getItem("preference5")!==null)?window.localStorage.getItem("preference5"):"1",
  "botStatus6" : (window.localStorage.getItem("preference6")!==null)?window.localStorage.getItem("preference6"):"1",
//  "botStatus7" : (window.localStorage.getItem("preference7")!==null)?window.localStorage.getItem("preference7"):"1"
};
var preference = {
  "preference1" : (window.localStorage.getItem("preference1")!==null)?window.localStorage.getItem("preference1"):"1",
  "preference2" : (window.localStorage.getItem("preference2")!==null)?window.localStorage.getItem("preference2"):"1",
  "preference3" : (window.localStorage.getItem("preference3")!==null)?window.localStorage.getItem("preference3"):"1",
  "preference4" : (window.localStorage.getItem("preference4")!==null)?window.localStorage.getItem("preference4"):"1",
  "preference5" : (window.localStorage.getItem("preference5")!==null)?window.localStorage.getItem("preference5"):"1",
  "preference6" : (window.localStorage.getItem("preference6")!==null)?window.localStorage.getItem("preference6"):"1",
//  "preference7" : (window.localStorage.getItem("preference7")!==null)?window.localStorage.getItem("preference7"):"1"
};
var nChildren = {
  "nChildren1" : window.localStorage.getItem("nChildren1"),
  "nChildren2" : window.localStorage.getItem("nChildren2"),
  "nChildren3" : window.localStorage.getItem("nChildren3")
};
var where = {
  "where1" : (window.localStorage.getItem("where1")!==null)?window.localStorage.getItem("where1"):true,
  "where2" : (window.localStorage.getItem("where2")!==null)?window.localStorage.getItem("where2"):true,
  "where3" : (window.localStorage.getItem("where3")!==null)?window.localStorage.getItem("where3"):true,
  "where4" : (window.localStorage.getItem("where4")!==null)?window.localStorage.getItem("where4"):true,
  "where5" : (window.localStorage.getItem("where5")!==null)?window.localStorage.getItem("where5"):true,
  "where6" : (window.localStorage.getItem("where6")!==null)?window.localStorage.getItem("where6"):true
};


var events = null;
var comments = null;
var lang;
var realizarBusqueda = true;  
var buscarOk = true;  
var idEvent = null;
var map = null;
var exitApp = true;
var numEvnt = 0;
var nextView = "menu('menu')";
var gE =false;
var userLat = null;
var userLon = null;
var userLocate = false;
var numEvents = -1;
var menuActivo = "menu('menu')";

var app = {
       
    // Constructor de la app    
    initialize: function() {      	      
    // Estado inicial mostrando capa cuerpo
    estado="cuerpo";
    numId = 222;
   // numSessionLog = Math.floor((Math.random() *  2147483647) + 1);
    // Creamos el elemento style, lo añadimos al html y creamos la clase cssClass para aplicarsela al contenedor wrapper
    var heightCuerpo=window.innerHeight-46;
    var style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = '.cssClass { position:absolute; z-index:2; left:0; top:46px; width:100%; height: '+heightCuerpo+'px; overflow:auto;}';
    document.getElementsByTagName('head')[0].appendChild(style);
        
    // Añadimos las clases necesarias
    cuerpo.className = 'page center';
    menuprincipal.className = 'page center';
    wrapper.className = 'cssClass';
    
    // Leemos por ajax el archivos menu.html de la carpeta opciones
    xhReq.open("GET", "opciones/preferencias.html", false);
    xhReq.send(null);
    document.getElementById("contenidoMenu").innerHTML=xhReq.responseText;
    
    var start = new Date().getTime();
    
    //setLog("AppStart", start);
    

    document.getElementById("contenidoCuerpo").innerHTML="<div id=\"map\"></div>";
    contenidoRotation.className = 'page transition center';
    contenidoCuerpo.className = 'page transition left';
    
    var query = "";
    query = "Select ID, TITULO, DESCRIPCION_C, DESCRIPCION_L, FECHA_I, FECHA_F, CATEGORIA, PRECIO, DIRECCION, COORDENADAS, IMAGEN, TWITTER, FACEBOOK, WEB from EVENTOS where (Fecha_F >=CURRENT_DATE) order by Fecha_F limit 5";            
    document.getElementById("contenidoRotation").innerHTML+="<div align=\"center\" style=\" position: absolute;	top: 50%; left:50%;	height: 30%;	width: 50%;	margin: -15% 0 0 -25%;\" ><img  src=\"img/loading.gif\" /></div>";

                                                            

    getId(function() {
    getEvents(function() {                
      var contetRotation = getContentRotation();
      document.getElementById("contenidoRotation").innerHTML=contetRotation;     
       iniRotation();
       var queryComent = "Select ID, ID_EVENTO, USUARIO, FECHA, COMENTARIO from COMENTARIOS order by FECHA";
    
       getComments(queryComent);
    }, query);
    });
    
    
     $.datepicker.regional['es'] = {
 closeText: 'Cerrar',
 prevText: '<Ant',
 nextText: 'Sig>',
 currentText: 'Hoy',
 monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
 monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
 dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
 weekHeader: 'Sm',
 dateFormat: 'dd/mm/yy',
 firstDay: 1,
 isRTL: false,
 showMonthAfterYear: false,
 yearSuffix: ''
 };
 $.datepicker.setDefaults($.datepicker.regional['es']);
    
    
     $("#contenidoRotation").css("display", "block");
    updateWhere();
    
    updatepreference();
    
    Calendar.install();

    SocialSharing.install();

$( "#datepicker" ).datepicker();


    
    nickUser =  window.localStorage.getItem("nickUser"); 
    bday = window.localStorage.getItem("bday");  
    dateEvent = window.localStorage.getItem("dateEvent"); 
    dateEventFormat = window.localStorage.getItem("dateEventFormat"); 
    where["where1"] = window.localStorage.getItem("where1");
    where["where1"] = window.localStorage.getItem("where2");
    where["where1"] = window.localStorage.getItem("where3");
    where["where1"] = window.localStorage.getItem("where4");
    where["where1"] = window.localStorage.getItem("where5");
    where["where1"] = window.localStorage.getItem("where6");
    

        
    nChildren[0] = window.localStorage.getItem("nChildren1");
    nChildren[1] = window.localStorage.getItem("nChildren2");
    nChildren[2] = window.localStorage.getItem("nChildren3");
    
    preference[0] = window.localStorage.getItem("preference1");
    preference[1] = window.localStorage.getItem("preference2");
    preference[2] = window.localStorage.getItem("preference3");
    preference[3] = window.localStorage.getItem("preference4");
    preference[4] = window.localStorage.getItem("preference5");
    preference[5] = window.localStorage.getItem("preference6");

    initPreferences();   
    getGps();  

    pressButton("btHome"); 
    
   navigator.splashscreen.show();  
    
    
    (function(d){
         var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
         js = d.createElement('script'); js.id = id; js.async = true;
         js.src = "//connect.facebook.net/es_LA/all.js";
         d.getElementsByTagName('head')[0].appendChild(js);
       }(document));
			
     (function(d, s, id) {
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) return;
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/es_LA/all.js#xfbml=1";
         fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        
    !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
    if(window.localStorage.getItem("sex")==='null')
    {
         window.localStorage.setItem("sex", "mam");   
    }
    if(window.localStorage.getItem("hijos")==='null')
    {
       window.localStorage.setItem("hijos", "withChildren");   
       $("#childrenAge").css("display", "block");
    }
    else
    {
      if(window.localStorage.getItem("hijos")==="withChildren")
        $("#childrenAge").css("display", "block");
      else
       $("#childrenAge").css("display", "block");
    }
    this.bindEvents();
    },
    bindEvents: function() {
      document.addEventListener('deviceready', this.onDeviceReady, false);
    }

};
   

function menu(opcion) {   
  $("#menu").css("opacity", "1");
  $("#header").css("opacity", "1");
  document.getElementById('lus').src = "img/user.png"; 
  exitApp = false;

  switch(opcion){
      case "menu":    
          numEvents = -1;
          $("#wrapper").scrollTop(0);
          if((estado==="cuerpo") || (estado==="perfil")){
        	exitApp = true;

          $("contenidoCuerpo").css("style", "width=100%; height=100%;");      
          contenidoCuerpo.className = 'page transition left';
          contenidoRotation.className = 'page transition center';          
          pressButton("btHome");
          releaseButton("btMap");
          releaseButton("btEvent");          
          estado = "cuerpo";
          menuActivo = "menu('menu')";
        }else if(estado==="menuprincipal"){
          cuerpo.className = 'page transition center';
          estado="cuerpo";
        }        
        break;   
    case "mapa":
     
      var vMap = "<div id=\"map\"></div>";
      vMap += "<div id=\"menu2\"> <a href=\"javascript:menu('userLocate');\"><img align=\"right\" id=\"logoMap\" class=\"logoMap\" src=\"img/map.png\" /></a></div>";
      document.getElementById("contenidoCuerpo").innerHTML=vMap;
      iniacilaMap();
      contenidoCuerpo.className = 'page transition center';
      contenidoRotation.className = 'page transition left';
       pressButton("btMap");
       releaseButton("btHome");
       releaseButton("btEvent");
      cuerpo.className = 'page transition center';
      estado="cuerpo";      
      menuActivo = "menu('mapa')";
      break;
   case "userLocate":
       numEvents = -1;
      if(userLocate) {
          userLocate = false;
         document.getElementById("logoMap").src ="img/map.png";
         for (var i = 0; i < map.markers.length; i++) {
           if(map.markers[i].details === 'Usuario')
           {
             map.markers[i].setMap(null);
           }
         }          
         map.panTo( new google.maps.LatLng( 40.470327, -3.873882));
      }
      else {
          nomUser = "";
           if(nickUser!==null)
               nomUser = nickUser;
          userLocate = true;
          document.getElementById("logoMap").src ="img/map_ON.png";
          map.panTo( new google.maps.LatLng( userLat, userLon));
          map.addMarker({       	
            lat: userLat,
            lng: userLon,         
            icon:"img/userPos.png",
            size: 'small',
            details: 'Usuario',
            title:"Usuario",
            infoWindow: {                  
                content: "<h3>" + nomUser + "tu posición gps es, Latitud: " + userLat +" Longitud: " + userLon + "</h3>"
            }
          });
      }
          
        break;
    case "preferencias":
        numEvents = -1;
      if(estado!=="preferencias"){
        document.getElementById('logoIzda').src = "img/preferenciasActivo.png"; 
        cuerpo.className = 'page transition right';
        estado="preferencias";
      }
      else{
        document.getElementById('logoIzda').src = "img/prefencias.png"; 
        cuerpo.className = 'page transition center';
        estado="cuerpo";
      }
      break;
    case "elegirFecha":
    case "elegirCategoria":
    case "buscar":
    case "where":

      contenidoCuerpo.className = 'page transition center';
      contenidoRotation.className = 'page transition left';
     
         
      xhReq.open("GET", "opciones/"+ opcion + ".html", false);      
      xhReq.send(null);
      document.getElementById("contenidoCuerpo").innerHTML=xhReq.responseText; 
      estado="cuerpo";
      $( "#datepicker" ).datepicker();
      if(opcion==="where") {      
        updateWhere();
        menuActivo = "menu('where')";
        
      }
      if(opcion==="elegirFecha") { 
         $( "#dateEvent" ).val(dateEvent); 
         if((dateEventFormat===null) || (dateEventFormat ==='null'))
         {
            document.getElementById("idTitulo").innerHTML=jQuery.i18n.prop('msg_ChooseDate');
            document.getElementById("idTitulo").innerHTML="<img  src=\"img/date_32.png\" height=\"22\" width=\"22\"/>&nbsp;&nbsp;Elegir Fecha&nbsp;&nbsp;&nbsp;&nbsp;";
        }
         else
             document.getElementById("idTitulo").innerHTML="<img  src=\"img/date_32.png\" height=\"22\" width=\"22\"/>&nbsp;&nbsp;Cambiar fecha(" + dateEventFormat + ")&nbsp;&nbsp;&nbsp;&nbsp;";
         menuActivo = "menu('elegirFecha')";
      }
      if(opcion=== "elegirCategoria") {      
        document.getElementById("botStatus1").src ="img/feriasMercadillos_"+lang+".png";
        document.getElementById("botStatus2").src ="img/zonaComercial_"+lang+".png";
        document.getElementById("botStatus3").src ="img/culturaEspectaculos_"+lang+".png";
        document.getElementById("botStatus4").src ="img/cursosTalleresConferencias_"+lang+".png";
        document.getElementById("botStatus5").src ="img/restauracion_"+lang+".png";
        document.getElementById("botStatus6").src ="img/especiales_"+lang+".png";

        updateButtons();
        menuActivo = "menu('elegirCategoria')";
      }
       $("#contenidoCuerpo").css("background", "#ffffff");

    break;
      case "perfil":     
          numEvents = -1;
          document.getElementById('lus').src = "img/userActivo.png"; 
         
         if(estado!=="perfil"){             

           contenidoCuerpo.className = 'page transition center';
           contenidoRotation.className = 'page transition left';
           xhReq.open("GET", "opciones/perfil.html", false);
           xhReq.send(null);
           document.getElementById("contenidoCuerpo").innerHTML=xhReq.responseText;
           $("#nickUser").val(nickUser);
           $("#bday").val(bday);
           updateProfileUser();
           estado="perfil";
 
           if((vRanting!=="true") || (vRantging==="true"))
           {
              $('#ranting').hide();
           }
           if(versionV2)
              myScroll = new IScroll('#contenidoCuerpo', { mouseWheel: true }); 
           document.getElementById("lbPersonalData").innerHTML=jQuery.i18n.prop('msg_PersonalData');
           document.getElementById("lbName").innerHTML=jQuery.i18n.prop('msg_Name');
           document.getElementById("lbSex").innerHTML=jQuery.i18n.prop('msg_Sex') + ":";
           document.getElementById("lbMale").innerHTML=jQuery.i18n.prop('msg_Male');
           document.getElementById("lbFemale").innerHTML=jQuery.i18n.prop('msg_Female');
           document.getElementById("lbDateBirth").innerHTML=jQuery.i18n.prop('msg_DateOfBirth') + ":";
           document.getElementById("lgFamily").innerHTML=jQuery.i18n.prop('msg_Family');
           document.getElementById("lbChildren").innerHTML=jQuery.i18n.prop('msg_Children') + ":";
           document.getElementById("lbWithCildren").innerHTML=jQuery.i18n.prop('msg_Yes');
           document.getElementById("lbChidless").innerHTML=jQuery.i18n.prop('msg_No');
           document.getElementById("lbChildrenAge").innerHTML=jQuery.i18n.prop('msg_ChildrenAge') + ":";
         }
         else{
           document.getElementById('lus').src = "img/user.png"; 
           contenidoRotation.className = 'page transition center';
           contenidoCuerpo.className = 'page transition left';
           pressButton("btHome");
           releaseButton("btMap");
           releaseButton("btEvent");
           estado="cuerpo";
         }
         nextView = menuActivo;
         menuActivo = "menu('perfil')";
         break;
     case "addComent":
         document.getElementById('lus').src = "img/user.png"; 
        if((!versionV2)  && (!versionV444))
           sleep(1500);

        contenidoCuerpo.className = 'page transition center';
        contenidoRotation.className = 'page transition left';  
        xhReq.open("GET", "opciones/comentarios.html", false);
        xhReq.send(null);
        $("#menu").css("opacity", "0.37");
        $("#header").css("opacity", "0.37");
        document.getElementById("contenidoCuerpo").innerHTML=xhReq.responseText;
        nextView = "getAllEvent(" + numEvent + ", 'comments')";
        menuActivo = "getAllEvent(" + numEvent + ", 'comments')";
        var elem = document.getElementById("nickUserComment");
        elem.value = nickUser;        
      break;
      default:
        // Añadimos la clase al li presionado
        addClass('li-menu-activo' , document.getElementById("ulMenu").getElementsByTagName("li")[opcion]);
        myScroll.refresh();
        myScroll.scrollTo(0,0);
        // Añadimos las clases necesarias para que la capa cuerpo se mueva al centro de nuestra app y muestre el contenido
        cuerpo.className = 'page transition center';
        estado="cuerpo";
        // Quitamos la clase añadida al li que hemos presionado
        setTimeout(function() {
          removeClass('li-menu-activo' , document.getElementById("ulMenu").getElementsByTagName("li")[opcion]);
        }, 300);
  }
} 

function getGps() {    
	navigator.geolocation.getCurrentPosition(getGpsonSuccess,getGpsonError);      
} 
function getGpsonSuccess(position) {
  userLat = position.coords.latitude;
  userLon = position.coords.longitude;	        
}

function getGpsonError(error) { console.log('code: ' +error.code+ '\n' + 'message: ' +error.message+ '\n'); }

function getContentRotation() {


  var txtRotation= "";
  var mostrar= false;
 //   txtRotation+="<div id=\"scroller\">";
  txtRotation += "<ul border=\"1\" id=\"rotation\" class=\"rotation-list\" style=\"background: #cccccc\"> ";
  for (var i=0; i < events.length; i++) {
    if(i < 5) {
      txtRotation += "<li >";
      txtRotation += "<a href=\"javascript:getAllEvent('" + i + "','rotation');\">";
      txtRotation += "<table class=\"table_1\">"; 
      txtRotation += "<tbody>";
      txtRotation += "<tr>";
      txtRotation += "<td class=\"td_izq\" rowSpan=\"2\" >";
      txtRotation += "<img  src=\"img/categoria" + events[i]["id_categoria"] + ".png\" height=\"32\" width=\"32\"/>";
      txtRotation += "</td>";
      txtRotation += "<td colspan=\"2\" rowspan=\"1\">";
      txtRotation += "<h2 class=\"subtitulo\">" + events[i]["nombre"] + "</h2>";      
      txtRotation += "</td>";
      txtRotation += "</tr>";
      txtRotation += "<tr>"; 
      txtRotation += "<td class=\"td_foto\">";
      txtRotation += "<img class= \"foto\" src=\"" + events[i]["usi_imagen"] + "\"/>";
      txtRotation += "</td>";
      txtRotation += "<td class=\"td_evento\">";
      txtRotation += events[i]["summary"] + "<br />";
      txtRotation += events[i]["coste"] + "<br />";
      var starDate = events[i]["start_date"].split('-');
      var endDate = events[i]["end_date"].split('-');
      txtRotation += "<b>Fecha: " + starDate[2] + "/" +  starDate[1] + "/" + starDate[0] +  " - " + endDate[2] + "/"  + endDate[1]  +  "/"+ endDate[0];
      txtRotation += "</b></td>";         
      txtRotation += "</tr></tbody></table>";      
      txtRotation += "</a></li>";
    }
  };
  txtRotation += "</ul>";
  
  txtRotation += "<div id=\"tabla\" >";  
  
  var txtFecha = "";  
  if((dateEventFormat===null) || (dateEventFormat ==='null'))
     txtFecha=jQuery.i18n.prop('msg_ChooseDate');
  else
     txtFecha="Cambiar fecha (" + dateEventFormat + ")";
  txtRotation += "<div  class=\"fila\"/><a   href=\"javascript:menu('elegirFecha');\"><div  class=\"column_1\"><img src=\"img/date_32.png\" height=\"32\" width=\"32\" /></div><div class=\"column_2\"><div  id=\"lbl\" >" +  txtFecha + "</div></div><div class=\"column_3\"><img   src=\"img/flecha_inicio.png\" height=\"32\" width=\"32\"/></div></a></div>";
  txtRotation += "<hr>";
  txtRotation += "<div  class=\"fila\"/> <a href=\"javascript:menu('elegirCategoria');\"><div  class=\"column_1\"><img src=\"img/menuColapsado_32.png\" height=\"32\" width=\"32\"/></div><div class=\"column_2\"><div id=\"lbl\">" + jQuery.i18n.prop('msg_ChooseCategory') + "</div></div><div class=\"column_3\"><img  src=\"img/flecha_inicio.png\" height=\"32\" width=\"32\"/></div></a></div>";
  txtRotation += "<hr>";
  txtRotation += "<div  class=\"fila\"/><a href=\"javascript:menu('where');\"><div  class=\"column_1\"><img src=\"img/where_32.png\" height=\"32\" width=\"32\"/></div><div class=\"column_2\"> <div id=\"lbl\">" + jQuery.i18n.prop('msg_ChooseWhere') + "</div></div><div class=\"column_3\"> <img src=\"img/flecha_inicio.png\" height=\"32\" width=\"32\"/></div></a></div>";
  txtRotation += "<hr id=\"lastHR\">";
  txtRotation += "<div  class=\"search\"/> <input id=\"buscar\" type=\"button\" value=" + jQuery.i18n.prop('msg_Search') + " onclick=\"javascript:htmlBuscar();\"  /></div>";
  
  txtRotation += "</div>";
  
  return txtRotation; 
    
}



function ftBuscar() {
  $("#menu").css("opacity", "1");
  $("#header").css("opacity", "1");
  numEvents = -1;
  pressButton("btEvent");
  releaseButton("btMap");
  releaseButton("btHome");
  exitApp =false;
  nextView = "menu('menu')";
  menuActivo = "ftBuscar()";  
  txtBuscar+="<div id=\"scroller\">";
  var txtBuscar = "<ul style=\" left:0; background: #ffffff;  padding-left: 0;\" >";
  var thereEvent = false;
  for (var i=0; i < events.length; i++) {      		
  
    if((botStatus['botStatus1']==="1" && events[i]["id_categoria"]==="1") || (botStatus['botStatus2']==="1" && events[i]["id_categoria"]==="2") ||
      (botStatus['botStatus3']==="1" && events[i]["id_categoria"]==="3") || (botStatus['botStatus4']==="1" && events[i]["id_categoria"]==="4") ||
      (botStatus['botStatus5']==="1" && events[i]["id_categoria"]==="5") || (botStatus['botStatus6']==="1" && events[i]["id_categoria"]==="6")) {

      if(comprobarFecha(events[i]["start_date"],events[i]["end_date"],dateEvent) || dateEvent==='null' || dateEvent===null || dateEvent === "undefined") {
        txtBuscar += "<li id=\"idLi" +i + "\" class=\"namid\">";
        txtBuscar += "<a href=\"javascript:getAllEvent('" + i + "','search');\">";      
        txtBuscar += "<table class=\"table_1\"cellpadding=\"0\" cellspacing=\"0\">";
        txtBuscar += "<tbody>";
        txtBuscar += "<tr>";
        txtBuscar += "<td class=\"td_izq\" rowSpan=\"3\" >";
        txtBuscar += "<img   src=\"img/categoria" + events[i]["id_categoria"] + ".png\"/>";
        txtBuscar += "</td>";
        txtBuscar += "<td class=\"td_buscar\" colspan=\"2\" rowspan=\"1\"  >";
        txtBuscar += "<h2 class=\"subtituloBuscar\">" +  events[i]["nombre"] + "</h2>";
        txtBuscar += "</td>";
        txtBuscar += "</tr>";
        txtBuscar += "<tr>";
        txtBuscar += "<td class=\"td_fotoBuscar\">";
        txtBuscar += "<img class= \"fotoBuscar\" src=\"" + events[i]["usi_imagen"] + "\"/>";
        txtBuscar += "</td>";
        txtBuscar += "<td class=\"td_eventoBuscar\">";       
        txtBuscar += "<p>" + events[i]["summary"]  + "</p>";
        var starDate = events[i]["start_date"].split('-');
        var endDate = events[i]["end_date"].split('-');
        txtBuscar += "Fecha: " + starDate[2] + "/" +  starDate[1] + "/" + starDate[0] +  " - " + endDate[2] + "/"  + endDate[1]  +  "/"+ endDate[0];
        txtBuscar += "</td>";
        txtBuscar += "</tr>";
        txtBuscar += "<tr>";
        txtBuscar += "<td class=\"td_abajoBuscar\">";
        txtBuscar += "</td>";
        txtBuscar += "<td class=\"td_meGustaBuscar\">";
        txtBuscar +=  events[i]["coste"];
        txtBuscar += "</td>";
        txtBuscar += "</tr>";
        txtBuscar += "</tbody></table>";
        txtBuscar += "</a></li>";
        thereEvent = true;
        mostrar= true;
       }
  }

  };
  txtBuscar += "</ul>";
  txtBuscar+="</div></div>";
  if(!thereEvent)
  txtBuscar += "<img src=\"img/no-eventos.png\"\">";
   
  txtBuscar += "</br></br></br></br>";      
  contenidoRotation.className = 'page transition left';
  contenidoCuerpo.className = 'page transition center';


  document.getElementById("contenidoCuerpo").innerHTML=txtBuscar;
  estado="cuerpo"; 


  if(versionV2)
    myScroll = new IScroll('#contenidoCuerpo', { mouseWheel: true });  

  return txtBuscar;
}


function getAllEvent(num, where){
      if(where === "search")  {       
    $("#idLi" + num).css("-webkit-animation-name", "flipOutY");
$("#idLi" + num).css("-webkit-animation-duration", "1s");
$("#idLi" + num).css("-webkit-animation-timing-function", "ease-in-out");
$("#idLi" + num).css("-webkit-animation-direction", "alternate");
}
    numEvents = num;
    $("#menu").css("opacity", "1");
    $("#header").css("opacity", "1");
    pressButton("btEvent");
    releaseButton("btMap");
    releaseButton("btHome");
    exitApp=false;
    numEvent = num;    
    var txtEvent= "";


   if(where === "search")       
     if((!versionV2) && (!versionV444))
 {
       sleep(1000);
       
 }
     $("#wrapper").scrollTop(0);  
    nextView = "ftBuscar(" + num + ")";
    menuActivo = "ftBuscar(" + num + ")";
    txtEvent+="<div id=\"scroller\">";
    txtEvent += "<li class=\"buscarEvento\" ><div><p> </p></div>";
    txtEvent += "<img class= \"foto_eventos\" src=\"" + events[num]["usi_imagen"] + "\"/>";
    txtEvent += "<img class=\"icono\" src=\"img/categoria" + events[num]["id_categoria"] + ".png\"/>";
    txtEvent += "<h2 class=\"subtitulo_eventos\">" + events[num]["nombre"] + "</h2>";
    txtEvent += "<blockquote>";    
    txtEvent += "<cite class=\"precio\">" + events[num]["coste"] + "</cite>"; 


    txtEvent += "<p class=\"calendario_eventos\"/><a href=\"javascript:exportToCalendar('" + num + "');\"><img id=\"idCalendar\" src=\"img/date_32.png\" height=\"32\" width=\"32\"></p></a>";
    txtEvent += "<img class=\"compartir_eventos\" onClick=\"window.plugins.socialsharing.share('He encontrado este evento desde Vive Majadahonda y estoy pensado asistir a: " + events[num]["nombre"] + " " + events[num]["summary"]  + "', null, null, null)\"  src=\"img/compartir_32.png\">";
    txtEvent += "<a href=\"javascript:menu('mapa');\"><img class=\"ir_mapa\" src=\"img/icoMap" +  events[num]["id_categoria"]  +".png\" height=\"46\" width=\"46\"></a>";
    txtEvent += "</blockquote></li><div class='pDescripcion'><p><h3>" + events[num]["summary"] + "</h3>" + events[num]["descripcion"] + "</p></div>";
        gE=false;
    if(goEvent!==null){
      for (i=0;i<goEvent.length;i++){
          if(goEvent[i]===events[num]["id_evento"]) {
            gE = true;
            txtEvent += "<a href=\"javascript:setGo('" + num + "');\"><p class=\"asistir_eventos\"/><img id=\"imgGo\" src=\"img/go_icon_ON.png\"  height=\"42\" width=\"175\"></p></a>";
            break;
         }
     }
    }
    if(!gE) {      
        txtEvent += "<a href=\"javascript:setGo('" + num + "');\"><p class=\"asistir_eventos\"/><img id=\"imgGo\" src=\"img/go_icon_OFF.png\" height=\"42\" width=\"175\"></p></a>";
    }
    txtEvent += "</p>";
   
    txtEvent += "<div class=\"social-likes\"  data-url=\""+ events[num]["facebook"] +"\" data-title=\"" + events[num]["nombre"] + "\"><div  class=\"facebook\"  onclick=\"removeEventListe();\" title=\"Share link on Facebook\">Facebook</div>	<div onclick=\"removeEventListe();\" class=\"twitter\" data-via=\"" + events[num]["twitter"] + "\" data-related=\"" + events[num]["nombre"] + "\" title=\"Share link on Twitter\">Twitter</div><div onclick=\"removeEventListe();\" class=\"plusone\" title=\"Share link on Google+\">Google+</div></div>";
    txtEvent += "<br><br><br><div  style=\"text-align:center; left:15px;\"> <a href=\"javascript:menu('addComent');\"><img  class=\"imCommnet\" src=\"img/comment.png\"height=\"48\" width=\"120\" ></p></a></div>";
    for (var i=0; i < comments.length; i++) {
    	if(comments[i]["id_evento"] === events[num]["id_evento"]) {
    		txtEvent += "<div>Usuario: <b>" + comments[i]["usuario"] + "</b> fecha: " + comments[i]["fecha"] + "</div>";
    		txtEvent += "<p class=\"triangle-border top\">" + comments[i]["comentario"] + "</p>";
    	}      
    };
    txtEvent += "<br><br><br><br>";
    txtEvent+="</div></div>";
    txtEvent += "<br><br><br><br>";
    idEvent = events[num]["id_evento"];         
         
    contenidoRotation.className = 'page transition left';
    contenidoCuerpo.className = 'page transition center';

  document.getElementById("contenidoCuerpo").innerHTML=txtEvent;  

  estado="cuerpo"; 
  $(".social-likes").socialLikes();

  if(versionV2)
    myScroll = new IScroll('#contenidoCuerpo', { mouseWheel: true });    
  setLog("AppODConsume", "Event Viewed" + events[num]["id_evento"]);
        
    return false;
}

// Función para añadir clases css a elementos
function addClass( classname, element ) {
    var cn = element.className;
    if( cn.indexOf( classname ) !== -1 ) {
      return;
    }
    if(cn !== '') {
      classname = ' '+classname;
    }
    element.className = cn+classname;
}

// Función para eliminar clases css a elementos
function removeClass( classname, element ) {
    var cn = element.className;
    var rxp = new RegExp( "\\s?\\b"+classname+"\\b", "g" );
    cn = cn.replace( rxp, '' );
    element.className = cn;
}

function viewCuerpo() {	
      contenidoRotation.className = 'page transition center';
           contenidoCuerpo.className = 'page transition left';
}

function nuevaFecha()
{

    if($("#datepicker").datepicker('getDate').getDate()!==null)
    {
       var dd = $("#datepicker").datepicker('getDate').getDate(); 
       
       
       var mm =  $("#datepicker").datepicker('getDate').getMonth() + 1;   
       
       var yy = $("#datepicker").datepicker('getDate').getFullYear();
       
      

       dateEvent = yy + "/" + mm + "/" + dd;
       dateEventFormat = dd + "/" + mm + "/" + yy;
       
       
       var contetRotation = getContentRotation();
      document.getElementById("contenidoRotation").innerHTML=contetRotation;  
         iniRotation();
       
  }
  else 
  {
     dateEvent = null;
     dateEventFormat = null;
 }

   window.localStorage.setItem("dateEvent", dateEvent);
   window.localStorage.setItem("dateEventFormat", dateEventFormat);

    contenidoRotation.className = 'page transition center';
    contenidoCuerpo.className = 'page transition left';           
}

function pressButton(name) {
  $("#" + name).css("border-top-color", "#415c7f");
  $("#" + name).css("background", "rgb(255,255,255)");
  $("#" + name).css("background", "-moz-linear-gradient(top, rgba(255,255,255,1) 0%, rgba(30,105,222,1) 78%, rgba(30,105,222,1) 100%)");
  $("#" + name).css("background", " -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,255,255,1)), color-stop(77%,rgba(255,255,255,1)), color-stop(78%,rgba(0,86,159,1)), color-stop(100%,rgba(0,86,159,1)))");
  $("#" + name).css("background", "-webkit-linear-gradient(top, rgba(255,255,255,1) 0%,rgba(255,255,255,1) 77%, rgba(30,105,222,1) 78%,rgba(0,86,159,1) 100%)");
  $("#" + name).css("background", "-o-linear-gradient(top, rgba(255,255,255,1) 0%,rgba(255,255,255,1) 77%, rgba(30,105,222,1) 78%,rgba(0,86,159,1) 100%)");
  $("#" + name).css("background", "-o-linear-gradient(top, rgba(255,255,255,1) 0%,rgba(255,255,255,1) 77%, rgba(30,105,222,1) 78%,rgba(0,86,159,1) 100%)");
  $("#" + name).css("background", "linear-gradient(to bottom, rgba(255,255,255,1) 0%,rgba(255,255,255,1) 77%,rgba(30,105,222,1) 78%,rgba(0,86,159,1) 100%)");
  $("#" + name).css("color", "#00569F");
}

function releaseButton(name) {    
  $("#" + name).css("background", "#FFFFFF");
  $("#" + name).css("color", "#233245");
}



function htmlBuscar() {
 if(!buscarOk) {
     navigator.notification.alert(
         'No se puede realizar busquedas, porque no tienen ninguna categoría selecionada.', // message
           null,            // callback to invoke with index of button pressed
         'Información de usabilidad',           // title
         'Cerrar' 
     );
     return;        
 }
 var txtBuscar= "";
  if(realizarBusqueda) {
    var query = getQuery();
       getEvents(function() {    
         txtBuscar += ftBuscar();
         realizarBusqueda = false;
    }, query);    
  }
  else {
  	txtBuscar += ftBuscar();
  }
  
  if(realizarBusqueda)  
  	txtBuscar+="<div style=\" position: absolute;	top: 50%;	left: 50%;	height: 30%;	width: 50%;	margin: -15% 0 0 -25%;\"><img  src=\"img/loading.gif\"/></div>";

  contenidoRotation.className = 'page transition left';
  contenidoCuerpo.className = 'page transition center';
  document.getElementById("contenidoCuerpo").innerHTML=txtBuscar;
  estado="cuerpo";
}




function getQuery() {
	
	var query = "Select distinct  E.ID, E.TITULO, E.DESCRIPCION_C, E.DESCRIPCION_L, E.FECHA_I, E.FECHA_F, E.CATEGORIA, E.PRECIO, E.DIRECCION, E.COORDENADAS, E.IMAGEN, E.TWITTER, E.FACEBOOK, E.WEB from EVENTOS E inner join REL_EVENTO_TAG R";	              
	var categoria = "";
	for(var i=1; i< 8; i++) {
		if(botStatus['botStatus' + i]==="1") {
			if(categoria !== "")
			    categoria += " or ";
			if(categoria === "")
			    categoria += " on (";
			categoria += "(E.CATEGORIA = " + i + ")";
		 }
	}
	if(categoria !== "") {
	   categoria += ") ";
	   query += categoria;
	}
	var zona = "and ((E.ZONA = 0)";
	for(var i=1; i< 6; i++) {		
		if(where['where' + i]) {
			zona += " or (E.ZONA = " + i + ")";
		 }
	}

    query += zona + ") ";

     query += "and (E.Fecha_F >=CURRENT_DATE)"; 

	return query;
	
}

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}




function removeEventListe() {
	document.removeEventListener('backbutton', onBackKeyDown);
}


function iniacilaMap() {       
  //$(document).ready(function(){       
    map = new GMaps({
      el: '#map',
      lat: 40.468923,
      lng: -3.881950,
      zoom: 13,
      zoomControl : true,
      zoomControlOpt: {
        style : 'SMALL', position: 'TOP_LEFT'
      },
      panControl : false
     });    

   var plantio = [[40.482601,-3.868861],[40.479859,-3.858218],[40.477574,-3.844185],[40.475583,-3.838735],[40.474767,-3.836288],[40.472808,-3.837447],[40.473624,-3.843756],[40.474408,-3.846331],[40.475191,-3.847747],[40.475354,-3.855085],[40.473951,-3.868089],[40.476497,-3.868518],[40.479794,-3.869720],[40.482568,-3.868840]];
   var monte = [[40.473624,-3.868217],[40.475126,-3.854957],[40.474930,-3.847446],[40.473918,-3.845129],[40.472743,-3.837533],[40.469282,-3.837061],[40.463732,-3.833971],[40.459879,-3.837404],[40.455242,-3.848305],[40.450409,-3.850107],[40.446163,-3.849850],[40.449952,-3.855686],[40.455503,-3.859119],[40.458213,-3.862295],[40.461773,-3.865900],[40.473526,-3.868217]];
   var carralero = [[40.446065,-3.850021],[40.445967,-3.855128],[40.445314,-3.859076],[40.444040,-3.861351],[40.443942,-3.867188],[40.444171,-3.872552],[40.444432,-3.878431],[40.447143,-3.879676],[40.451486,-3.878517],[40.454001,-3.878303],[40.457299,-3.879805],[40.463928,-3.883882],[40.467193,-3.887615],[40.470098,-3.891993],[40.466703,-3.884912],[40.464907,-3.881178],[40.461675,-3.872123],[40.461152,-3.869891],[40.460858,-3.865943],[40.458083,-3.862596],[40.455993,-3.860579],[40.454687,-3.858690],[40.451258,-3.856716],[40.449756,-3.855686],[40.446131,-3.850107]];
   var centro = [[40.461217,-3.866029],[40.461087,-3.867016],[40.461413,-3.870621],[40.461805,-3.872209],[40.464940,-3.880920],[40.466801,-3.884826],[40.470425,-3.892164],[40.474995,-3.894782],[40.478227,-3.895555],[40.476432,-3.891263],[40.475942,-3.889546],[40.476399,-3.886671],[40.473885,-3.882251],[40.473102,-3.879547],[40.474114,-3.874955],[40.473689,-3.868690],[40.465789,-3.867316],[40.461413,-3.866200]];
   var norte =  [[40.473951,-3.868604],[40.476693,-3.868926],[40.479810,-3.870106],[40.482732,-3.869119],[40.485865,-3.879933],[40.485114,-3.882337],[40.486600,-3.887444],[40.487628,-3.890598],[40.487775,-3.892293],[40.488999,-3.893344],[40.488966,-3.894997],[40.485833,-3.895125],[40.482471,-3.894975],[40.478782,-3.895490],[40.478537,-3.894675],[40.476448,-3.890555],[40.476203,-3.889160],[40.476660,-3.886950],[40.475599,-3.884354],[40.474359,-3.882723],[40.474097,-3.882143],[40.473428,-3.879848],[40.473347,-3.877745],[40.474440,-3.874762],[40.474048,-3.873174],[40.473873,-3.868582]];
   var plaza = [[40.470261,-3.892679],[40.478847,-3.896713],[40.482862,-3.895812],[40.488019,-3.895941],[40.492001,-3.894954],[40.495918,-3.902378],[40.496897,-3.902936],[40.495461,-3.906670],[40.494906,-3.909073],[40.493405,-3.911562],[40.490467,-3.918943],[40.489521,-3.920059],[40.486746,-3.921046],[40.484201,-3.922977],[40.483025,-3.924437],[40.482373,-3.922162],[40.479696,-3.917356],[40.478749,-3.915038],[40.477411,-3.912034],[40.473004,-3.898430],[40.470457,-3.892937]];
   
   
   if(where["where1"]) {
     map.drawPolygon({
      paths: norte,
      strokeColor: '#B45F04',
      strokeOpacity: 0.6,
      strokeWeight: 4,
      fillOpacity: 0.1,  
      fillColor: '#B45F04'
    });
    map.drawOverlay({
      lat: 40.479924,
      lng: -3.877487,
      content: '<h3>Norte</h>'
    });
   }
   if(where["where2"]) {   
     map.drawPolygon({
      paths: centro,
      strokeColor: '#04B431',
      strokeOpacity: 0.6,
      strokeWeight: 4,
      fillOpacity: 0.1,
      fillColor: '#04B431'
    });
    map.drawOverlay({
      lat: 40.470327,
      lng: -3.873882,
      content: '<h3>Centro</h>'
    });
   }
  
   if(where["where3"]) {     
     map.drawPolygon({
      paths: carralero,
      strokeColor: '#086A87',
      strokeOpacity: 0.6,
      strokeWeight: 4,
      fillOpacity: 0.1,
      fillColor: '#086A87'
    });
    map.drawOverlay({
     lat: 40.457266,
     lng: -3.870621,
     content: '<h3>Carralero</h>'
    });
   }

   if(where["where4"]) {
     map.drawPolygon({
       paths: monte,
       strokeColor: '#3A01DF',
       strokeOpacity: 0.6,
       fillOpacity: 0.1,
       strokeWeight: 4,       
       fillColor: '#3A01DF'
     });
     map.drawOverlay({
       lat: 40.470653,
       lng: -3.858862,
       content: '<h3>Monte del Pilar</h>'
     });
 }
   
  if(where["where5"]) {
    map.drawPolygon({
      paths: plantio,
      strokeColor: '#8A0808',
      strokeOpacity: 0.6,
      strokeWeight: 4,      
      fillOpacity: 0.1,
      fillColor: '#8A0808'  
    });  
    map.drawOverlay({
      lat: 40.479272,
      lng: -3.861952,
      content: '<h3>Plantío</h>'
    });
  }

  if(where["where6"]) {
    map.drawPolygon({
      paths: plaza,
      strokeColor: '#8A0808',
      strokeOpacity: 0.6,
      strokeWeight: 4,      
      fillOpacity: 0.1,
      fillColor: '#8A0808'
    });  
    map.drawOverlay({
      lat: 40.482601,
      lng: -3.905382,
      content: '<h3>Gran Plaza</h>'
    });
  }

  if(numEvents === -1) {
  for (var i=0; i < events.length; i++) {
    events[i]["geometry"] = events[i]["geometry"].replace(" ","");
    events[i]["geometry"] = events[i]["geometry"].replace(")","");
    events[i]["geometry"] = events[i]["geometry"].replace("(","");
    if((botStatus['botStatus1']==="1" && events[i]["id_categoria"]==="1") || (botStatus['botStatus2']==="1" && events[i]["id_categoria"]==="2") ||
     (botStatus['botStatus3']==="1" && events[i]["id_categoria"]==="3") || (botStatus['botStatus4']==="1" && events[i]["id_categoria"]==="4") ||
     (botStatus['botStatus5']==="1" && events[i]["id_categoria"]==="5") || (botStatus['botStatus6']==="1" && events[i]["id_categoria"]==="6")) {
       if(comprobarFecha(events[i]["start_date"],events[i]["end_date"],dateEvent) || dateEvent==='null' || dateEvent===null || dateEvent === "undefined") {
         var lt = events[i]["geometry"].substring(0, events[i]["geometry"].lastIndexOf(","));
         var lg = events[i]["geometry"].substring(events[i]["geometry"].lastIndexOf(",")+1, events[i]["geometry"].length);        
         map.addMarker({       	
           lat:  lt,
           lng: lg,         
           icon:"img/icoMap" +  events[i]["id_categoria"]  +".png",
           size: 'small',
           title: events[i]["nombre"],
           infoWindow: {                  
               content: "<a href=\"javascript:getAllEvent('" + i + "','map');\"><p><h1>"+events[i]["summary"]+"</h1> Fecha:" + events[i]["start_date"] + " - " + events[i]["end_date"] + "<h3> " + events[i]["coste"] + "</h3><img src=\"" + events[i]["usi_imagen"] + "\"  height=\"72\" width=\"72\"/></a></p><a  href=\"javascript:getRoute(" + lt + ", " + lg +", 'driving');\"><h4><b>Cómo llegar en...</b> </h4></a>&nbsp; <a  href=\"javascript:getRoute(" + lt + ", " + lg +", 'driving');\"> &nbsp; &nbsp;  <img src=img/car.png  height=\"32\" width=\"32\"/> &nbsp; &nbsp; &nbsp;</a><a  href=\"javascript:getRoute(" + lt + ", " + lg +", 'bicycling ');\">&nbsp;&nbsp;  &nbsp;<img src=img/bici.png  height=\"32\" width=\"32\"/>&nbsp; &nbsp; &nbsp; </a><a  href=\"javascript:getRoute(" + lt + ", " + lg +", 'walking');\"> &nbsp; &nbsp; &nbsp;<img src=img/mam.png  height=\"32\" width=\"32\"/></a>"
                                                                                                                                                                                                                                                                                                                          
           }
         });
       }
      }
    }    
      
    }
    else {
          events[numEvents]["geometry"] = events[numEvents]["geometry"].replace(" ","");
          events[numEvents]["geometry"] = events[numEvents]["geometry"].replace(")","");
          events[numEvents]["geometry"] = events[numEvents]["geometry"].replace("(","");;
          var lt = events[numEvents]["geometry"].substring(0, events[numEvents]["geometry"].lastIndexOf(","));
          var lg = events[numEvents]["geometry"].substring(events[numEvents]["geometry"].lastIndexOf(",")+1, events[numEvents]["geometry"].length);
          map.panTo( new google.maps.LatLng(lt, lg));
          map.addMarker({       	
           lat: lt,
           lng: lg,         
           icon:"img/icoMap" +  events[numEvents]["id_categoria"]  +".png",
           size: 'small',
           title: events[numEvents]["nombre"],
           infoWindow: {                  
               content: "<a href=\"javascript:getAllEvent('" + numEvents + "','map');\"><p><h3>"+events[numEvents]["summary"]+"</h3> Fecha:" + events[numEvents]["start_date"] + " - " + events[numEvents]["end_date"] + "<h4> " + events[numEvents]["coste"] + "</h4><img src=\"" + events[numEvents]["usi_imagen"] + "\"  height=\"72\" width=\"72\"/></a></p><a  href=\"javascript:getRoute(" + lt + ", " + lg +", 'driving');\"><h4><b>Cómo llegar en...</b></h4></a>&nbsp;<a  href=\"javascript:getRoute(" + lt + ", " + lg +", 'driving');\">&nbsp;&nbsp;<img src=img/car.png  height=\"32\" width=\"32\"/>&nbsp;&nbsp;&nbsp;</a><a  href=\"javascript:getRoute(" + lt + ", " + lg +", 'bicycling ');\">&nbsp;&nbsp;&nbsp;    <img src=img/bici.png  height=\"32\" width=\"32\"/>&nbsp;&nbsp;&nbsp;</a><a  href=\"javascript:getRoute(" + lt + ", " + lg +", 'walking');\"> &nbsp;&nbsp;&nbsp;  <img src=img/mam.png  height=\"32\" width=\"32\"/></a>"
           }
         });
    }
  
}


function getRoute(lat, lon, medio) {
    map.cleanRoute();
    map.drawRoute({
       origin: [lat, lon],
       destination: [userLat, userLon],
       travelMode: medio,
       strokeColor: '#0174DF',
      strokeOpacity: 0.8,
      strokeWeight: 6
    });    
}

function iniRotation(){
  $("#rotation").rotation({       
    autoRotate: true,
    interval: 5000
  });
}


function addComments() {
  if(idEvent !== null) {
    $("#menu").css("opacity", "1");
    $("#header").css("opacity", "1");   
    var nombreUser = document.getElementById("nickUserComment").value;
    var txtComment = document.getElementById("coments").value;
    var d = new Date();
    var mes = d.getMonth() + 1;
    var query = "INSERT INTO COMENTARIOS (ID_EVENTO, USUARIO, COMENTARIO, FECHA) VALUES (" + idEvent + ", \"" + nombreUser + "\", \"" +  txtComment + "\", \""  + d.getFullYear() + "-" + mes + "-" + d.getDate() + "\");";
      
    nextView = "getAllEvent(" + numEvent + ", 'comments')";
    setTimeout(nextView,0);      
      
    setTimeout(addComent(query),0);            	    
  }
	
}

function addComent(query) {
	  getId(function() {
      	setComment(query);
      });
     setLog("AppProsume", "Comment Added " + events[numEvent]["id_evento"]);
}

function cancelComment() {
      $("#menu").css("opacity", "1");
      $("#header").css("opacity", "1");
      nextView = "getAllEvent(" + numEvent + ", 'comments')";
      setTimeout(nextView,0);
}

      
function statusBoton(bot){    
  var id = $(bot).attr("id");
  if(botStatus[id]==="0"){
   $(bot).css("background", "#cedce7");
     
  }
  else{
      $(bot).css("background", "#2b72cc"); 
      $(bot).css("background-image", "-webkit-linear-gradient(top, #4c9bbc, #275c98)");
  
  }                 
}

function statusBotonSel(sel){   
  var id = $(sel).attr("id");
  if(preference[id]==="0"){
    $(sel).css("background", "#cedce7");

  }
  else{
     $(sel).css("background", "#2b72cc");
    $(sel).css("background-image", "-webkit-linear-gradient(top, #4c9bbc, #275c98)");
  }                 
}

function changeButton (bot){
  
  realizarBusqueda = true;
  var id = $(bot).attr("id");
  if(botStatus[id]==="0"){
       botStatus[id] = "1";
  }
  else{
    botStatus[id] = "0";
  }
  
  statusBoton($(bot));
  
  
  buscarOk = false;  
  for(var i=1; i<= 6; i++){

     if(botStatus["botStatus" + i] === "1") {
         buscarOk=true;         
         break;
     }     
  }
  

  if(!buscarOk){      
      document.getElementById('acceptCategori').disabled = true;
       $("#acceptCategori").css("background", "#cedce7");
       $("#acceptCategori").css("background-image", "-webkit-linear-gradient(top, #cedce7, #596a72)");
       
  }
  else {
      document.getElementById('acceptCategori').disabled = false;
      $("#acceptCategori").css("background", "#2b72cc");
      $("#acceptCategori").css("background-image", "-webkit-linear-gradient(top, #4c9bbc, #275c98)");
  }
 
}
             
function changeNick(name){
  nickUser=name.value;
  window.localStorage.setItem("nickUser", nickUser);
}

function changeDate(date){
  realizarBusqueda = true;
  bday=date.value;
  window.localStorage.setItem("bday", bday);
}

function changeDateEvent(){
  dateEvent = null;
  dateEventFormat = null;
  window.localStorage.setItem("dateEvent", dateEvent);
  window.localStorage.setItem("dateEventFormat", dateEventFormat);
  var contetRotation = getContentRotation();
  document.getElementById("contenidoRotation").innerHTML=contetRotation;        
  iniRotation();
  
  menu('menu');
}


function selectWhere(sel){
  realizarBusqueda = true;
  var id = $(sel).attr("id");
  if(where[id])  {
      where[id] = false;
  }
  else {
      where[id] = true;
  }
  window.localStorage.setItem(id, where[id]); 
}

function selectNumChildren(sel) {
	realizarBusqueda = true;
  var id = $(sel).attr("id");
  if(nChildren[id]===0) nChildren[id] = 1;
  else nChildren[id] = 0;
}

function selectpreference(sel) {
  realizarBusqueda = true;

  var id = $(sel).attr("id");
  
  if(preference[id]==="0") preference[id] = "1";
  else preference[id] = "0";
  

  window.localStorage.setItem(id, preference[id]);  
  statusBotonSel($("#"+id));
}

function selectChildren(sel)
{      
	realizarBusqueda = true;
  var id = $(sel).attr("id");
  if(id==="withChildren")
  {    
    $("#withChildren").prop("checked", true);
    $("#childless").prop("checked", false);
    $("#childrenAge").css("display", "block");
  }
  else
  {
    $("#withChildren").prop("checked", false);
    $("#childless").prop("checked", true);
    $("#childrenAge").css("display", "none");
  }
  window.localStorage.setItem("hijos", id);
}

function selectSex(sel)
{      
  realizarBusqueda = true;
  var id = $(sel).attr("id");
  if(id==="mam")
  {    
    $("#mam").prop("checked", true);
    $("#womam").prop("checked", false);
  }
  else
  {
    $("#mam").prop("checked", false);
    $("#womam").prop("checked", true);
  }
  window.localStorage.setItem("sex", id);
}


function initPreferences() {
    for(var i=1; i< 8; i++)
    {
      var id = "preference" + i;  
      if(preference[id]==="1") {
         botStatus['botStatus'+i] = "1";
         
      }         
      else {
         botStatus['botStatus'+i] = "0";
      }
    }  

}

function updateProfileUser() {

  if(window.localStorage.getItem("sex")==="mam")
  {
    $("#mam").prop("checked", true);
    $("#womam").prop("checked", false);
  }
  else
  {
    $("#mam").prop("checked", false);
    $("#womam").prop("checked", true);
  }
  if(window.localStorage.getItem("hijos")==="withChildren")
  {
    $("#withChildren").prop("checked", true);
    $("#childless").prop("checked", false);
    $("#childrenAge").css("display", "block");
  }
  else
  {
    $("#withChildren").prop("checked", false);
    $("#childless").prop("checked", true);
    $("#childrenAge").css("display", "none");
  }
  for(var n = 1; n <= 3; n++)
  {
    var id = "nChildren"+ n;
    if(nChildren[id]===1)
       $("#"+id).prop("checked", true);
    else
       $("#"+id).prop("checked", false);
    window.localStorage.setItem(id, nChildren[id]);
  }
}


function updateWhere(){
  
  for(var n = 1; n < 7; n++)
  {
    var id = "where"+ n;
    if(where[id])
      $("#"+id).prop("checked", true);
    else
      $("#"+id).prop("checked", false);
    window.localStorage.setItem(id, where[id]);
  }
}

function updateButtons(){
  for(var n = 1; n <= 6; n++)
  {
    
    var id = "botStatus"+ n;
    statusBoton($("#"+id));
     $("#"+id).val(bday);
    
  }
}

function updatepreference(){
  for(var n = 1; n <= 6; n++)
  {
    
    var id = "preference"+ n;
    statusBotonSel($("#"+id));
     $("#"+id).val(bday);
    
  }
}

function comprobarFecha(fechaAnterior,fechaPosterior,fecha)
{
  
  var fecAnt = fechaAnterior.split("/");
  var fecPos = fechaPosterior.split("/");
  var fAnt = new Date(fecAnt[2]+"-"+fecAnt[1]+"-"+fecAnt[0]);
  var fPos = new Date(fecPos[2]+"-"+fecPos[1]+"-"+fecPos[0]);
  var fAct = new Date(fecha);  
  fAct.setHours(0,0,0);
  if(fAct >= fAnt && fAct <= fPos)
    return true;
  else
    return false;
}

//calcular la edad de una persona 
//recibe la fecha como un string en formato español 
//devuelve un entero con la edad. Devuelve false en caso de que la fecha sea incorrecta o mayor que el dia actual 
function calcular_edad(fecha){ 


   	//calculo la fecha de hoy 
   	var hoy=new Date();

   	//calculo la fecha que recibo 
   	//La descompongo en un array 
   	var array_fecha = fecha.split("/");
   	//si el array no tiene tres partes, la fecha es incorrecta 
   	if (array_fecha.length!==3) 
      	 return false;

   	//compruebo que los ano, mes, dia son correctos 
   	var ano; 
   	ano = parseInt(array_fecha[2]); 
   	if (isNaN(ano)) 
      	 return false; 

   	var mes; 
   	mes = parseInt(array_fecha[1]); 
   	if (isNaN(mes)) 
      	 return false; 

   	var dia; 
   	dia = parseInt(array_fecha[0]);	
   	if (isNaN(dia)) 
      	 return false; 


   	//si el año de la fecha que recibo solo tiene 2 cifras hay que cambiarlo a 4 
   	if (ano<=99) 
      	 ano +=1900; 

   	//resto los años de las dos fechas 
   	edad=hoy.getYear()- ano - 1; //-1 porque no se si ha cumplido años ya este año 

   	//si resto los meses y me da menor que 0 entonces no ha cumplido años. Si da mayor si ha cumplido 
   	if (hoy.getMonth() + 1 - mes < 0) //+ 1 porque los meses empiezan en 0 
      	 return edad; 
   	if (hoy.getMonth() + 1 - mes > 0) 
      	 return edad+1; 

   	//entonces es que eran iguales. miro los dias 
   	//si resto los dias y me da menor que 0 entonces no ha cumplido años. Si da mayor o igual si ha cumplido 
   	if (hoy.getUTCDate() - dia >= 0) 
      	 return edad + 1; 

   	return edad; 
}

function ocultarRanting() {
     $('#ranting').hide();     
     window.localStorage.setItem("vRanting", "false");     
}

function onWebMajadahonda() {
  document.removeEventListener("backbutton", onBackKeyDown);
  var ref = window.open(encodeURI('http://www.majadahonda.org'), '_blank', 'location=yes');
}

jQuery(document).ready(function() {
  if ( navigator && navigator.userAgent && (lang = navigator.userAgent.match(/android.*\W(\w\w)-(\w\w)\W/i))) {
  lang = lang[1];
}
if (!lang && navigator) {
  if (navigator.language) {
    lang = navigator.language;
  }else if (navigator.browserLanguage) {
    lang = navigator.browserLanguage;
  }else if (navigator.systemLanguage) {
    lang = navigator.systemLanguage;
  }else if (navigator.userLanguage) {
    lang = navigator.userLanguage;
  }
  lang = lang.substr(0, 2);
}      
loadBundles(lang);
});

function loadBundles(lang) {  
  jQuery.i18n.properties({
  name:'Messages',
  path:'bundle/',
  mode:'both',
  encoding: 'UTF-8',  
  language:lang,
  callback: function() {
  document.getElementById("btHome").innerHTML=jQuery.i18n.prop('msg_HOME');
  document.getElementById("btMap").innerHTML=jQuery.i18n.prop('msg_MAP');
  document.getElementById("lbFoviteActivities").innerHTML=jQuery.i18n.prop('msg_FoviteActivities');
  document.getElementById("lbFestival").innerHTML=jQuery.i18n.prop('msg_Festival');
  document.getElementById("lbShopping").innerHTML=jQuery.i18n.prop('msg_Shopping');
  document.getElementById("lbCulture").innerHTML=jQuery.i18n.prop('msg_Culture');
  document.getElementById("lbCourse").innerHTML=jQuery.i18n.prop('msg_Course');
  document.getElementById("lbGastronomy").innerHTML=jQuery.i18n.prop('msg_Gastronomy');
  document.getElementById("lbSpecial").innerHTML=jQuery.i18n.prop('msg_Special');
  document.getElementById("desCategori").innerHTML=jQuery.i18n.prop('msg_ChooseCategoryDes');       
  document.getElementById("cWhere").innerHTML=jQuery.i18n.prop('msg_ChooseWhere');
  document.getElementById("desPrefeCategori").innerHTML=jQuery.i18n.prop('msg_ChooseCategoryDesPrefe');    
  document.getElementById("desWhere").innerHTML=jQuery.i18n.prop('msg_desWhere');    
  document.getElementById("desCategori").innerHTML=jQuery.i18n.prop('msg_desCategori');    
  document.getElementById("cCategori").innerHTML=jQuery.i18n.prop('msg_ChooseCategory');

  }
});
}

// Wait for device API libraries to load
//


function exportToCalendar(numEvent) {
   // prep some variables
   document.getElementById('idCalendar').src = "img/date_32_Activo.png"; 
   if((!versionV2)  && (!versionV444))
    sleep(1500);
   if((events[numEvent]["start_date"]!==null) && (events[numEvent]["end_date"]!==null)) {

     var sDate = events[numEvent]["start_date"].split("-");
     var eDate = events[numEvent]["end_date"].split("-");
     var startDate = new Date(sDate[0],sDate[1]-1,sDate[2],0,0,0,0,0); // beware: month 0 = january, 11 = december
     var endDate = new Date(eDate[0],eDate[1]-1,eDate[2],0,0,0,0,0);
     var title =  events[numEvent]["summary"];
     var location =  events[numEvent]["place"] ;
     var notes =  events[i]["nombre"];
     var success = function(message) { /*alert("Success: " + JSON.stringify(message));*/ };
     var error = function(message) { alert("Error: " + message); };
   
     window.plugins.calendar.createEventInteractively(title,location,notes,startDate,endDate,success,error);
     document.getElementById('idCalendar').src = "img/date_32.png"; 

   }
}

function getId(callback) {	
	$.getJSON('https://iescities.com/IESCities/api/entities/datasets/search?keywords=ComercialEvents&offset=0&limit=1000', function(data) {		       
       var empObject = eval('(' + JSON.stringify(data) + ')');
       numId = empObject[0].datasetId;       
       callback();
    }); 
}



function setLog(typeMes, message) {
  var d = new Date();
  var n = d.getTime();	
  n = n / 1000;    
  var urlAj = "https://iescities.com/IESCities/api/log/app/event/" + n + "/Majadahonda Leisure & Events/" + numSessionLog + "/"+ typeMes + "/" + message;
  $.ajax({        
 	url: urlAj,       
    type:'POST', 
    data: '', 
    dataType:'json', 
    error:function(jqXHR,text_status,strError){ 
      console.log("No connection IESCities api log");
    },
    success:function(data){
    
    }
  });  
}


function setComment(query) {
  $.ajax({        
    url:'https://iescities.com/IESCities/api/data/update/' + numId + '/sql',       
    type:'POST', 
    data: query, 
    // dataType:'json', 
    contentType:'text/plain',
    headers: {"Authorization": make_basic_auth('USUARIO', 'CLAVE')},

    error:function(jqXHR,text_status,strError){ 
     navigator.notification.alert(
          'No ha sido posible añadir tu comentario. Intenteló más tarde, sentimos las molestias.', // message
           null,            // callback to invoke with index of button pressed
          'Error de conexión el servidor',           // title
          'Cerrar' 
       );
    },
    success:function(data){

      var queryComent = "Select ID, ID_EVENTO, USUARIO, FECHA, COMENTARIO from COMENTARIOS order by FECHA";

navigator.notification.alert(
    'Su información ha sido incluida en el evento, Gracias', // message
     getComments(queryComent),            // callback to invoke with index of button pressed
    'Comentario añadido',           // title
    'Cerrar'
);
    
    }
  });  
}


function setGo(numEvent) {
    var update = null;
    if(gE) 
      update = 'update EVENTOS set NUM_ASISTENTES=NUM_ASISTENTES-1 where (ID='+events[numEvent]["id_evento"]+' and NUM_ASISTENTES > 1);';    
    else 
      update = 'update EVENTOS set NUM_ASISTENTES=NUM_ASISTENTES+1 where ID=' + events[numEvent]["id_evento"] +";";
    
  $.ajax({        
    url:'https://iescities.com/IESCities/api/data/update/' + numId + '/sql',       
    type:'POST', 
    data: update, 
     contentType:'text/plain',
    headers: {"Authorization": make_basic_auth('USER', 'CLAVE')},
    error:function(jqXHR,text_status,strError){ 

     navigator.notification.alert(
          'No ha sido posible modificar su asistencia. Intenteló más tarde, sentimos las molestias.', // message
           null,            // callback to invoke with index of button pressed
          'Error de conexión el servidor',           // title
          'Cerrar' 
       );
    },
    success:function(data){
        if(!gE)
        {
           document.getElementById('imgGo').src = "img/go_icon_ON.png";     
           goEvent.splice(0,0,events[numEvent]["id_evento"]); 
           gE = true;
       }
       else {
        document.getElementById('imgGo').src = "img/go_icon_OFF.png";    
        gE=false;
        for (i=0;i<goEvent.length;i++){
          if(goEvent[i]===events[numEvent]["id_evento"]) {          
            goEvent.splice(i,1);    
          }
        }       
       }
       window.localStorage.setItem("goEvent", JSON.stringify(goEvent));
       
      navigator.notification.alert(
        'Su interés acerca del evento ha sido modificado. Gracias', // message
         null,            // callback to invoke with index of button pressed
        'Información almacenada correctamente',           // title
        'Cerrar'
     );
    
    }
  });  
}


function make_basic_auth(user, password){
    var tok = user + ':' + password;
    var hash = Base64.encode(tok);
    return "Basic " + hash;
}

function getComments(query) {
  $.ajax({                 
 	url:'https://iescities.com/IESCities/api/data/query/' + numId + '/sql',
    type:'POST', 
    data: query, 
    contentType:'text/plain',
    error:function(jqXHR,text_status,strError){ 
      navigator.notification.alert(
          'Intenteló más tarde, sentimos las molestias', // message
           null,            // callback to invoke with index of button pressed
          'Error de conexión el servidor',           // title
          'Cerrar' 
       );
    },
    success:function(data){
    	
       var empObject = eval('(' + JSON.stringify(data) + ')');
        var empObjectLen = data.rows.length;      
        comments = new Array(empObjectLen);
        for (var i=0; i<empObjectLen; i++) {
          comments[i] = new Object();

          comments[i]["id"] =  empObject.rows[i].ID;
          comments[i]["id_evento"] =  empObject.rows[i].ID_EVENTO;
          comments[i]["usuario"] =  empObject.rows[i].USUARIO;
          var fIni =  empObject.rows[i].FECHA;
          var fecha = fIni.split('-');
          comments[i]["fecha"] =  fecha[2] + "/"  + fecha[1]  +  "/"+ fecha[0];                    
          comments[i]["comentario"] =  empObject.rows[i].COMENTARIO ;
        }
    }
  });  
  setLog("AppConsume", "The user has accessed the user comments");
}

   



function getEvents(callback, query) {
    

  $.ajax({   
 	url:'https://iescities.com/IESCities/api/data/query/' + numId + '/sql',

    type:'POST', 
    data: query, 
    contentType:'text/plain',

    error:function(jqXHR,text_status,strError){ 
       navigator.notification.alert(
          'Intenteló más tarde, sentimos las molestias', // message
           null,            // callback to invoke with index of button pressed
          'Error de conexión el servidor',           // title
          'Cerrar');
    },
    success:function(data){        
       var empObject = eval('(' + JSON.stringify(data) + ')');
        var empObjectLen = data.rows.length;      
        events = new Array(empObjectLen);        
        for (var i=0; i<empObjectLen; i++) {
            
          events[i] = new Object();
          
          events[i]["id_evento"] =  empObject.rows[i].ID;
          events[i]["id_tipo"] =  empObject.rows[i].id_tipo;
          events[i]["descripcion"] =  empObject.rows[i].DESCRIPCION_L;          
          events[i]["place"] =  empObject.rows[i].DIRECCION;
          if(empObject.rows[i].PRECIO !== "0" && empObject.rows[i].PRECIO !== "Free" && empObject.rows[i].PRECIO !== "Libre") {
          	events[i]["coste"] =  "Precio: " + empObject.rows[i].PRECIO + "€";
          }
          else {
          	events[i]["coste"] =  "Precio: Entrada libre";
          }                  
          events[i]["summary"] =  empObject.rows[i].DESCRIPCION_C;
          events[i]["usi_imagen"] =   "http://150.241.239.51/public/IESCities/PHP/archivos/" + empObject.rows[i].IMAGEN;        
          events[i]["id_categoria"] =  empObject.rows[i].CATEGORIA;
          events[i]["geometry"] =  empObject.rows[i].COORDENADAS; 
          events[i]["nombre"] =  empObject.rows[i].TITULO;
          events[i]["start_date"] =  empObject.rows[i].FECHA_I;
          events[i]["end_date"] =  empObject.rows[i].FECHA_F;    
          events[i]["twitter"] =  empObject.rows[i].TWITTER;
          events[i]["facebook"] =  empObject.rows[i].FACEBOOK;
          events[i]["web"] =  empObject.rows[i].WEB;
        }
        
         callback();

    }
  });  
}
