function capturePhoto() {

navigator.camera.getPicture(onSuccess, onFail, { quality: 50,targetWidth: 600,
  targetHeight: 600,
    destinationType: Camera.DestinationType.FILE_URI });
}

function getPhoto(source) {

    navigator.camera.getPicture(
                onPhotoURISuccess,
                function(message) { navigator.notification.alert(
                  'No ha sido posible obtener la foto, sentimos las molestias.', // message
                   null,            // callback to invoke with index of button pressed
                  'Captura de foto fallida',           // title
                  'Cerrar' 
               ); },
                {
                    quality         : 50,
                    targetWidth: 600,
                    targetHeight: 600,
                    destinationType : navigator.camera.DestinationType.FILE_URI,
                    sourceType      : navigator.camera.PictureSourceType.PHOTOLIBRARY
                }
            );

}

function onSuccess(imageURI) {    
    var image = document.getElementById('foto');
    image.src = imageURI;
    document.getElementById("foto").style.backgroundImage="url('"+imageURI+"')";
    document.getElementById("foto").style.backgroundSize="100% 130%";
    var options = new FileUploadOptions();
    options.fileKey="file";
    options.fileName=imageURI.substr(imageURI.lastIndexOf('/')+1);
    options.mimeType="image/jpeg";
    bOtenerIcon =true;
    if(idUser===null || idUser==="null") {          
      getId(function() {
        comprobarUser(function(){          
        var params = {};
        params.value1 = idUser;
        params.value2 = "param";
        options.params = params;
        iconoUser = "http://150.241.239.51/public/SportsNetwork/img/" + idUser + ".jpg";
        var ft = new FileTransfer();
        ft.upload(imageURI, encodeURI("http://150.241.239.51/public/SportsNetwork/upload.php"), win, fail, options);
       });});
    }
    else {            
        var params = {};            
        params.value1 = idUser;
        params.value2 = "param";
        options.params = params;
        iconoUser = "http://150.241.239.51/public/SportsNetwork/img/" + idUser + ".jpg";
        var ft = new FileTransfer();
        ft.upload(imageURI, encodeURI("http://150.241.239.51/public/SportsNetwork/upload.php"), win, fail, options);
    }    
    
}


function onPhotoURISuccess(imageURI) {
    document.getElementById("foto").style.backgroundImage="url('"+imageURI+"')";
    document.getElementById("foto").style.backgroundSize="100% 140%";
 
    var options = new FileUploadOptions();
    options.fileKey="file";
    options.fileName=imageURI.substr(imageURI.lastIndexOf('/')+1);
    options.mimeType="image/jpeg";
    bOtenerIcon =true;    
    if(idUser===null || idUser==="null") {          
      getId(function() {
        comprobarUser(function(){          
        var params = {};
        params.value1 = idUser;
        params.value2 = "param";
        options.params = params;
        iconoUser = "http://150.241.239.51/public/SportsNetwork/img/" + idUser + ".jpg";
        var ft = new FileTransfer();
        ft.upload(imageURI, encodeURI("http://150.241.239.51/public/SportsNetwork/upload.php"), win, fail, options);
       });});
    }
    else {            
        var params = {};            
        params.value1 = idUser;
        params.value2 = "param";
        options.params = params;
        iconoUser = "http://150.241.239.51/public/SportsNetwork/img/" + idUser + ".jpg";
        var ft = new FileTransfer();
        ft.upload(imageURI, encodeURI("http://150.241.239.51/public/SportsNetwork/upload.php"), win, fail, options);
    }    
}

function win(r) {
   // alert("Code = " + r.responseCode+"Response = " + r.response+"Sent = " + r.bytesSent);
    console.log("Code = " + r.responseCode);
    console.log("Response = " + r.response);
    console.log("Sent = " + r.bytesSent);
}

function fail(error) {
    //alert("An error has occurred: Code = " + error.code);
    console.log("upload error source " + error.source);
    console.log("upload error target " + error.target);
}

    
                          function onFail(message) {
                              console.log('Failed because: ' + message);
                          }

function onFail(message) {
    console.log('Failed because: ' + message);
}

function obtenerFoto(){
    // $.fn.ferroMenu.toggleMenu("#nav");
   var d = new Date();
   nextView = "configureUser()";
   document.getElementById('menu').style.visibility = 'visible';   
   document.getElementById("menu").innerHTML = "<img align='left' id='logoIzda' class='logo' src='" + iconoUser + "?" + d.getTime() + "' height='45' width='45'/><div class='menuTitle'>Cambiar icono perfil</div>";
   xhReq.open("GET", "pags/obtenerFoto.html", false);      
   xhReq.send(null);  
   document.getElementById("contenidoCuerpo").innerHTML=xhReq.responseText;   
   document.getElementById("foto").style.width = (document.getElementById("contenidoCuerpo").clientWidth-50)+"px";   
   document.getElementById("foto").style.height = (document.getElementById("contenidoCuerpo").clientWidth-50)+"px";   
   if(idUser===null || idUser==="null") { 
     document.getElementById("foto").style.backgroundImage="url('img/userIco.png')"; 
     document.getElementById("foto").style.backgroundSize="80% 80%";
   }
   else{
       if(iconoUser==="img/userIco.png"){
           $.ajax({
              type: "POST",
              url: "http://150.241.239.51/public/SportsNetwork/fileExists.php",
              data: {FileName:idUser},
                error:function(jqXHR,text_status,strError){ 
                var message = JSON.stringify(jqXHR);
                console.error(message);
                navigator.notification.alert(
                  'No ha sido posible modificar tus preferencicas. Intenteló más tarde, sentimos las molestias.', // message
                   null,            // callback to invoke with index of button pressed
                  'Error de conexión en el servidor',           // title
                  'Cerrar' 
               );
            },
            success:function(data){

              if(data==="true"){
                 document.getElementById("foto").style.backgroundImage="url('http://150.241.239.51/public/SportsNetwork/img/" + idUser + ".jpg')";
                 iconoUser = "http://150.241.239.51/public/SportsNetwork/img/" + idUser + ".jpg";
                 document.getElementById("foto").style.backgroundSize="100% 100%";
              }
              else{
                 document.getElementById("foto").style.backgroundImage="url('img/userIco.png')"; 
                 iconoUser = "img/userIco.png";
                 document.getElementById("foto").style.backgroundSize="80% 80%";
              }
              
             }   
            });
       }
       else {
           var d = new Date();
           document.getElementById("foto").style.backgroundImage="url('" + iconoUser + "?" + d.getTime() + "')";               
           document.getElementById("foto").style.backgroundSize="100% 130%";
       }
   }

}

function obtenerIcon(callback) {    
   if(iconoUser==="img/userIco.png"){
       $.ajax({
          type: "POST",
          url: "http://150.241.239.51/public/SportsNetwork/fileExists.php",
          data: {FileName:idUser},
            error:function(jqXHR,text_status,strError){ 
            var message = JSON.stringify(jqXHR);
            console.error(message);
            navigator.notification.alert(
              'No ha sido posible modificar tus preferencicas. Intenteló más tarde, sentimos las molestias.', // message
               null,            // callback to invoke with index of button pressed
              'Error de conexión en el servidor',           // title
              'Cerrar' 
           );
            callback();
        },
        success:function(data){

          if(data==="true"){
             iconoUser = "http://150.241.239.51/public/SportsNetwork/img/" + idUser + ".jpg";
             callback();
          }
          else{
             iconoUser = "img/userIco.png";
             callback();
          }
         }          
        });
    }    
}

function obtenerIconUsers(callback, iUser) {
       $.ajax({
          type: "POST",
          url: "http://150.241.239.51/public/SportsNetwork/fileExists.php",
          data: {FileName:iUser},
            error:function(jqXHR,text_status,strError){ 
            var message = JSON.stringify(jqXHR);
            console.error(message);
            navigator.notification.alert(
              'No ha sido posible modificar tus preferencicas. Intenteló más tarde, sentimos las molestias.', // message
               null,            // callback to invoke with index of button pressed
              'Error de conexión en el servidor',           // title
              'Cerrar' 
           );
            callback();
        },
        success:function(data){

          if(data==="true"){
             var icon = "http://150.241.239.51/public/SportsNetwork/img/" + iUser + ".jpg";
             document.getElementById("icon"+iUser).src = icon;
             callback();
          }
          else{
             document.getElementById("icon"+iUser).src = "img/userIco.png";
             callback();
          }
         }          
        });
   
}

function SinFoto() {
    iconoUser="img/userIco.png";
    document.getElementById("foto").style.backgroundImage="url('img/userIco.png')"; 
    iconoUser = "img/userIco.png";
    document.getElementById("foto").style.backgroundSize="80% 80%";
     $.ajax({
              type: "POST",
              url: "http://150.241.239.51/public/SportsNetwork/deleteFile.php",
              data: {FileName:idUser},
                error:function(jqXHR,text_status,strError){ 
                var message = JSON.stringify(jqXHR);
                console.error(message);
                
            }
        });
  
}


// // // FIN GESTION CÁMARA