setGeolocation = function(position) {
  userLat = position.coords.latitude;
  userLon = position.coords.longitude;
  map.panTo( new google.maps.LatLng( userLat, userLon) );
};

var infoWindow;

function iniMap(){
 //$.fn.ferroMenu.toggleMenu("#nav");
 nextView = "pagInicio()";
 var d = new Date();
 document.getElementById('menu').style.visibility = 'visible';   
 document.getElementById("menu").innerHTML = "<img align='left' id='logoIzda' class='logo' src='" + iconoUser + "?" + d.getTime() + "' height='45' width='45'/><div class='menuTitle'>Buscar eventos cercanos</div>";
 document.getElementById("contenidoCuerpo").innerHTML="<div id='menuOpMap'><a href='javascript:opcionMap();'><img  src='img/prefencias.png'  id='idPref' height='55' width='55'/></a></div><div id='panel'><input id='address' type='textbox' value='Majadahonda'><input type='button' value='Buscar' onclick='codeAddress()'></div><div id='map'></div>"; //<div id='menuMapa'><a href='javascript:menuBusqueda();'><img  src='img/iconBuscar.png'  id='idPref' height='55' width='70'/></a></div>";   
 //google.maps.event.addDomListener(window, 'load', initialize);
 initialize();
 getGps();
}

function initialize() {
  geocoder = new google.maps.Geocoder();
  var latlng = new google.maps.LatLng(userLat, userLon);
  
  var mapOptions = {
    zoom: 13,
    center: latlng
  };
  goMenu=false;
  map = new google.maps.Map(document.getElementById('map'), mapOptions);
  var query = "Select * from EVENTOS where DIA >=CURRENT_DATE order by DIA";  
  obtenerEventos(function() {  
     for(var x=0; x<eventos.length; x++) {
        var indexDeportes = ["", "Baloncesto","Tenis","Padel","Fútbol 7", "Fútbol 11", "Fútbol sala","Running", "Esgrima", "Patinaje", "Voleibol", "Escalada", "Golf"];
        
        var txtUrl = "img/icoMap" + indexDeportes.indexOf(eventos[x]["deporte"]) + ".png";  
             var image = {
                url: txtUrl,
                // This marker is 20 pixels wide by 32 pixels tall.
                size: new google.maps.Size(64, 64),
                // The origin for this image is 0,0.
                origin: new google.maps.Point(0,0)
                // The anchor for this image is the base of the flagpole at 0,32.

              };                         
          
              
              if(eventos[x]["coordenadas"].indexOf(',') > 0) 
              {
                 var dias_semana = new Array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado");
              var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre", "Diciembre");            
              var indexDeportes = ["", "Baloncesto","Tenis","Padel","Fútbol 7", "Fútbol 11", "Fútbol sala","Running", "Esgrima", "Patinaje", "Voleibol", "Escalada", "Golf"];
              var listEvents = "";            

                listEvents += "<div  class='modEvent'>";
                listEvents += "<div class='modLis1'>"; 
                listEvents += "<a href='javascript:exportToCalendar('" + x + "');'>";
                listEvents += "<time class='icon'>";
                var nd = new Date(eventos[x]["dia"])
                listEvents += "<em>" + dias_semana[nd.getDay()] + "<br>" + eventos[x]["hora"].substring(0, eventos[x]["hora"].length-3); + "</em>";
                listEvents += "<strong>" + meses[nd.getMonth()] + "</strong>";
                listEvents += "<span>" + nd.getDate() + "</span>";
     
                listEvents += "</time>";
                listEvents += "</a>";
                listEvents += "</div>";
                listEvents += "<div class='modLis2'>";
                listEvents += "<a href='javascript:verEvento('" + x +"');'>";                        
                listEvents += "<h2 style='color: white'>" + eventos[x]["titulo"] + "</h2>";
                listEvents += "<h3 style='color: white'>" + eventos[x]["lugar"] + "</h3>";
                
                listEvents += "</a>";
                listEvents += "</div>";
                listEvents += "<div class='modLis3'>";
                listEvents += "<img src='img/d" + indexDeportes.indexOf(eventos[x]["deporte"]) + ".png' />";
               listEvents += "</div>";
               listEvents += "</div><br><br>";               

         
                  var coord = eventos[x]["coordenadas"].split(',');
                  var myLatlng = new google.maps.LatLng(coord[0],coord[1]);
                  var marker = new google.maps.Marker({
                  position: myLatlng,
                  icon:txtUrl,
                  map: map,
                 title:eventos[x]["titulo"]
              });
              addInfoWindow(marker, listEvents);

              }
            }
  },query);
}

function initializeQuery(query) {
  geocoder = new google.maps.Geocoder();
  var latlng = new google.maps.LatLng(userLat, userLon);
  
  var mapOptions = {
    zoom: 13,
    center: latlng
  };
  goMenu=false;
  map = new google.maps.Map(document.getElementById('map'), mapOptions);
  
  obtenerEventos(function() {  
  
     if(eventos.length<=0)
          navigator.notification.alert(
                'No ha se ha encontrado eventos para los parámetros utilizados', // message
                null,            // callback to invoke with index of button pressed
                'No existe eventos',           // title
                'Cerrar' 
            );    
     for(var x=0; x<eventos.length; x++) {
        var indexDeportes = ["", "Baloncesto","Tenis","Padel","Fútbol 7", "Fútbol 11", "Fútbol sala","Running", "Esgrima", "Patinaje", "Voleibol", "Escalada", "Golf"];
        
        var txtUrl = "img/icoMap" + indexDeportes.indexOf(eventos[x]["deporte"]) + ".png";  
             var image = {
                url: txtUrl,
                // This marker is 20 pixels wide by 32 pixels tall.
                size: new google.maps.Size(64, 64),
                // The origin for this image is 0,0.
                origin: new google.maps.Point(0,0)
                // The anchor for this image is the base of the flagpole at 0,32.

              };                         
          
              
              if(eventos[x]["coordenadas"].indexOf(',') > 0) 
              {
                 var dias_semana = new Array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado");
              var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre", "Diciembre");            
              var indexDeportes = ["", "Baloncesto","Tenis","Padel","Fútbol 7", "Fútbol 11", "Fútbol sala","Running", "Esgrima", "Patinaje", "Voleibol", "Escalada", "Golf"];
              var listEvents = "";            

                listEvents += "<div  class='modEvent'>";
                listEvents += "<div class='modLis1'>"; 
                listEvents += "<a href='javascript:exportToCalendar('" + x + "');'>";
                listEvents += "<time class='icon'>";
                var nd = new Date(eventos[x]["dia"]);
                listEvents += "<em>" + dias_semana[nd.getDay()] + "<br>" + eventos[x]["hora"].substring(0, eventos[x]["hora"].length-3); + "</em>";
                listEvents += "<strong>" + meses[nd.getMonth()] + "</strong>";
                listEvents += "<span>" + nd.getDate() + "</span>";
     
                listEvents += "</time>";
                listEvents += "</a>";
                listEvents += "</div>";
                listEvents += "<div class='modLis2'>";
                listEvents += "<a href='javascript:verEvento('" + x +"');'>";                        
                listEvents += "<h2 style='color: white'>" + eventos[x]["titulo"] + "</h2>";
                listEvents += "<h3 style='color: white'>" + eventos[x]["lugar"] + "</h3>";
                
                listEvents += "</a>";
                listEvents += "</div>";
                listEvents += "<div class='modLis3'>";
                listEvents += "<img src='img/d" + indexDeportes.indexOf(eventos[x]["deporte"]) + ".png' />";
               listEvents += "</div>";
               listEvents += "</div><br><br>";               

         
                  var coord = eventos[x]["coordenadas"].split(',');
                  var myLatlng = new google.maps.LatLng(coord[0],coord[1]);
                  var marker = new google.maps.Marker({
                  position: myLatlng,
                  icon:txtUrl,
                  map: map,
                 title:eventos[x]["titulo"]
              });
              addInfoWindow(marker, listEvents);

              }
            }
  },query);
}

function addInfoWindow(marker, message) {
// infoWindow.close();
            infoWindow = new google.maps.InfoWindow({
                content: message
            });

            google.maps.event.addListener(marker, 'click', function () {                
                infoWindow.open(map, marker);
            });
        }

function initialize2() {
    geocoder = new google.maps.Geocoder();
  var latlng = new google.maps.LatLng(userLat, userLon);
  var mapOptions = {
    zoom: 15,
    center: latlng
  };
  map2 = new google.maps.Map(document.getElementById('map2'), mapOptions);
}

function initializeMap2(latLon) {
  var coord = latLon.split(',');
  var latlng = new google.maps.LatLng(coord[0],coord[1]);
  var mapOptions = {
    zoom: 15,
    center: latlng
  };
  map2 = new google.maps.Map(document.getElementById('map2'), mapOptions);
}

function codeAddress() {
  var address = document.getElementById('address').value;
  geocoder.geocode( { 'address': address}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      map.setCenter(results[0].geometry.location);
      var marker = new google.maps.Marker({
          map: map,
          position: results[0].geometry.location
      });
    } else {
        navigator.notification.alert(
                'No ha sido posible localizar la dirección, intentalo de nuevo por favor', // message
                null,            // callback to invoke with index of button pressed
                'Dirección no encontrada',           // title
                'Cerrar' 
            );     
    }
  });
}

function opcionMap() {    
      if(estado!=="preferencias"){
        cuerpo.className = 'page transition right';
    
      //  docMenu.className = 'page transition right';
        contenidoMenu.className = 'page transition center';
        contenidoCuerpo.className = 'page transition left';
        document.getElementById("idPref").src ="img/prefencias_OF.png";  
        estado="preferencias";
      }
      else{
        cuerpo.className = 'page transition center';
    //    docMenu.className = 'page transition center';
    
        contenidoMenu.className = 'page transition left';
        contenidoCuerpo.className = 'page transition center';
        document.getElementById("idPref").src ="img/prefencias.png";          
        estado="cuerpo";
      }      
}


function buscarMap(){
    
    cuerpo.className = 'page transition center';    
    
    contenidoMenu.className = 'page transition left';
    contenidoCuerpo.className = 'page transition center';
    document.getElementById("idPref").src ="img/prefencias.png";          
    estado="cuerpo";
    var compDia = ">=";
    if(document.getElementById("ckEventosHoy").checked)
        compDia = "=";
        
    var query = "Select * from EVENTOS where ((DIA " + compDia + " CURRENT_DATE) ";
    
    if("Deportes..." !==  document.getElementById("selDeporteMap").value)
       query += " and (DEPORTE = '" + document.getElementById("selDeporteMap").value + "')";
    if(document.getElementById("ckMisDeportes").checked){
         var comparador = "and (";

       if(arrDeportes!==null)
        {       


            for (var i=1;i<arrDeportes.length;i++){        

                if(arrDeportes[i]!=="8"){
                    query += comparador + "(DEPORTE='" + indexDeportes[i] + "')";
                    comparador = " or ";
                }
            }
            if(comparador === " or ")
                query += ")";
        }
        else{
            
        }
    }   
    query += ")";

    initializeQuery(query);
        
}

function centrarPos(address) { 
    geocoder.geocode( { 'address': address}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      posicionEvento =results[0].geometry.location; 
      map2.setCenter(results[0].geometry.location);      
      var marker = new google.maps.Marker({
          map: map,
          position: results[0].geometry.location
      });                 
    } else {
       navigator.notification.alert(
                'No ha sido posible localizar la dirección, intentalo de nuevo por favor', // message
                null,            // callback to invoke with index of button pressed
                'Dirección no encontrada',           // title
                'Cerrar' 
       );      
    }
  });
}

