var xhReq = new XMLHttpRequest();
var numId, numIdData, allUsers, sexo;
var geocoder;
var allRecintos=null;
var contenidoMenu = document.getElementById("contenidoMenu");
var cuerpo = document.getElementById("contenidoCuerpo");
var docMenu = document.getElementById("menu");
var map, map2;
var botStatus = {
  "botStatus1" : (window.localStorage.getItem("botStatus1")!==null)?window.localStorage.getItem("botStatus1"):"0",
  "botStatus2" : (window.localStorage.getItem("botStatus2")!==null)?window.localStorage.getItem("botStatus2"):"0",
  "botStatus3" : (window.localStorage.getItem("botStatus3")!==null)?window.localStorage.getItem("botStatus3"):"0",
  "botStatus4" : (window.localStorage.getItem("botStatus4")!==null)?window.localStorage.getItem("botStatus4"):"0",
  "botStatus5" : (window.localStorage.getItem("botStatus5")!==null)?window.localStorage.getItem("botStatus5"):"0",
  "botStatus6" : (window.localStorage.getItem("botStatus6")!==null)?window.localStorage.getItem("botStatus6"):"0",
  "botStatus7" : (window.localStorage.getItem("botStatus7")!==null)?window.localStorage.getItem("botStatus7"):"0",
  "botStatus8" : (window.localStorage.getItem("botStatus8")!==null)?window.localStorage.getItem("botStatus8"):"0",
  "botStatus9" : (window.localStorage.getItem("botStatus9")!==null)?window.localStorage.getItem("botStatus9"):"0",
  "botStatus10" : (window.localStorage.getItem("botStatus10")!==null)?window.localStorage.getItem("botStatus10"):"0",
  "botStatus11" : (window.localStorage.getItem("botStatus11")!==null)?window.localStorage.getItem("botStatus11"):"0",
  "botStatus12" : (window.localStorage.getItem("botStatus12")!==null)?window.localStorage.getItem("botStatus12"):"0",
  "botStatus13" : (window.localStorage.getItem("botStatus13")!==null)?window.localStorage.getItem("botStatus13"):"0",

};
var deportes = (window.localStorage.getItem("deportes")!==null)?window.localStorage.getItem("deportes"):null;
var numDeportes = 12;
var arrDeportes = new Array(numDeportes); 
var user = (window.localStorage.getItem("user")!==null)?window.localStorage.getItem("user"):null;
var pass = (window.localStorage.getItem("pass")!==null)?window.localStorage.getItem("pass"):null;
var idUser = (window.localStorage.getItem("idUser")!==null)?window.localStorage.getItem("idUser"):null;
var nombre = (window.localStorage.getItem("nombre")!==null)?window.localStorage.getItem("nombre"):null;
var apellidos = (window.localStorage.getItem("apellidos")!==null)?window.localStorage.getItem("apellidos"):null;
var telefono = (window.localStorage.getItem("telefono")!==null)?window.localStorage.getItem("telefono"):null;
var sexo = (window.localStorage.getItem("sexo")!==null)?window.localStorage.getItem("sexo"):"2";
var localizacion = (window.localStorage.getItem("localizacion")!==null)?window.localStorage.getItem("localizacion"):null;
var puntuacion = (window.localStorage.getItem("puntuacion")!==null)?window.localStorage.getItem("puntuacion"):null;
var organizados = (window.localStorage.getItem("organizados")!==null)?window.localStorage.getItem("organizados"):null;
var iconoUser = (window.localStorage.getItem("iconoUser")!==null)?window.localStorage.getItem("iconoUser"):"img/userIco.png";
var userLat = 40.474218;
var userLon = -3.869158;
var pictureSource;
var posicionEvento= null;
var eventos = null;
var listEventos = null;
var goMenu = false;
var eventos = null;
var userEventos = null;
var estado;
var gcmId="";
var idenEvento;
var numEvento;
var comments = null;
var competiciones = null;
var temporadas = null;
var jornadas = null;
var resultados = null;
var bOtenerIcon = true;
var nextView = "paginamenu('menu')";
var nextConf = "deportConfig()";
var numSessionLog=1;
var indexDeportes = ["", "Baloncesto","Tenis","Padel","Fútbol 7", "Fútbol 11", "Fútbol sala","Running", "Esgrima", "Patinaje", "Voleibol", "Escalada", "Golf"];  


var app = {
    // Application Constructor
    initialize: function() {          
       numId = 219;      

startRatingSurvey();
      if((deportes !== null) && (deportes !== "null"))
      {
           arrDeportes = deportes.split(',');
      }
      else
      {
         for (var i=0; i<=numDeportes; i++) {        
                arrDeportes[i] = "8";          
         }
      }
      estado="cuerpo";
      contenidoMenu.className = 'page transition left';
      xhReq.open("GET", "pags/opcionesMap.html", false);
      xhReq.send(null);
      document.getElementById("contenidoMenu").innerHTML=xhReq.responseText;
      
      

       obtenerIcon(function() {
          document.getElementById('menu').style.visibility = 'visible';   
          var d = new Date();
          document.getElementById("menu").innerHTML = "<img align='left' id='logoIzda' class='logo' onclick=\"configureUser();\" src='" + iconoUser + "?" + d.getTime() + "' height='45' width='45'/><div class='menuTitle'>Menú</div><img  id='logoDcha' onclick=\"changeImage(this,'exit2.png','exit.png');setTimeout(function(){exit();});\" src='img/exit.png' height='45' width='45'/>";

        });         
        pagInicio(); 
        getRecintos();
        
       this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {        
        pictureSource=navigator.camera.PictureSourceType; 
        destinationType=navigator.camera.DestinationType;
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var pushNotification = window.plugins.pushNotification;        
        pushNotification.register(app.successHandler, app.errorHandler,{"senderID":"655950926696","ecb":"app.onNotificationGCM"});

        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');


        console.log('Received Event: ' + id);
    },
    successHandler: function(result) {
        console.log('Todo Correcto! Resultado = '+result);
    },
    errorHandler:function(error) {
        console.log(error);
    },
    onNotificationGCM: function(e) {

    switch( e.event ) {
        case 'registered':
            if ( e.regid.length > 0 ) {
                console.log("Id registro: " + e.regid);
                gcmId=e.regid;
                console.log('Id registro:  '+e.regid);
                
            }
        break;
        case 'message':
            console.log('Mensaje:  '+e.message+' Contenido: '+e.msgcnt);
        break;
        case 'error':
            console.log('GCM error = '+e.msg);
        break;
        default:
           console.log('Ocurrió un evento desconocido de GCM');
        break;
    }
}

};



function startRatingSurvey2() {
    startRatingSurvey("true");
}

function deselTodos(){
    if("Deportes..." !==  document.getElementById("selDeporteMap").value){
       document.getElementById("ckTodosDeportes").checked = false; 
       document.getElementById("ckMisDeportes").checked = false;
   }
}

function selMisDeportes(){
    if(document.getElementById("ckMisDeportes").checked){
       document.getElementById("selDeporteMap").value ="Deportes...";        
       document.getElementById("ckTodosDeportes").checked = false;          
   }
}


function selTodos(){
    if(document.getElementById("ckTodosDeportes").checked){
       document.getElementById("selDeporteMap").value ="Deportes...";        
       document.getElementById("ckMisDeportes").checked = false;          
   }
}

function crearCuenta() {        
    var d = new Date();
    document.getElementById('menu').style.visibility = 'visible';   
    document.getElementById("menu").innerHTML = "<img align='left' id='logoIzda' class='logo' src='" + iconoUser + "?" + d.getTime() + "' height='45' width='45'/><div class='menuTitle'>Datos alta registro</div><img  id='logoDcha' onclick=\"changeImage(this,'exit2.png','exit.png');setTimeout(function(){exit();});\" src='img/exit.png' height='45' width='45'/>";

    xhReq.open("GET", "pags/datosUser.html", false);      
    xhReq.send(null);
    document.getElementById("contenidoCuerpo").innerHTML=xhReq.responseText;
    goMenu=false;
}

function mensajePrivacidad() {
    navigator.notification.alert(
        'Las leyes sobre la privacidad es el área del derecho que se ocupa de la protección y preservación de los derechos a la privacidad de las personas. Cada vez con mayor asiduidad los gobiernos y otras organizaciones tanto públicas como privadas recolectan grandes cantidades de información personal sobre las personas para una gran variedad de propósitos. Las leyes sobre la privacidad regulan que tipo de información puede ser recolectada y como es que dicha información debe ser utilizada y almacenada. El ámbito de aplicabilidad de las leyes sobre la privacidad a veces es denominado expectativa de privacidad.', 
        null,         
        'Politica de privacidad',           
        'Aceptar'
    );
    
}

function registrarUser() {
    
    var contrasena = document.getElementById("contrasena").value;
    var contrasenaConf = document.getElementById("contrasenaConf").value;
    if(contrasena.length<4) {
       navigator.notification.alert(
          'Por favor, introduzca una contraseña de más de 4 caracteres', 
          null,         
          'contraseña demasiado corta',           
          'Aceptar'
        );
    }
    else
    {
        if(contrasena !== contrasenaConf)
        {          
            navigator.notification.alert(
              'Las contraseñas deben de coincidir. Vuelve a introducirlas, por favor', 
              null,         
              'La contraseña no coinciden',           
              'Aceptar'
            );
        }else
        {
            getId(function() {
                crearUser(function(){          
            });});
        
            // GESTIONAR ALTA!!! pagInicio();
        }
    }
}

function guardarCambiosUser() {
    
    var contrasena = document.getElementById("confContrasena").value;
    var contrasenaConf = document.getElementById("ConfContrasenaConf").value;

    if((contrasena.length==0) && (contrasenaConf.length==0)) {
         modificarUsuario();
    }
    else {
        if(contrasena.length<4) {
           navigator.notification.alert(
              'Por favor, introduzca una contraseña de más de 4 caracteres', 
              null,         
              'contraseña demasiado corta',           
              'Aceptar'
            );
        }
        else
        {
            if(contrasena !== contrasenaConf)
            {          
                navigator.notification.alert(
                  'Las contraseñas deben de coincidir. Vuelve a introducirlas, por favor', 
                  null,         
                  'La contraseña no coinciden',           
                  'Aceptar'
                );
            }else
            {
                modificarUsuario();
            }
        }
    }
}


function iniSession() {
    var contrasena = document.getElementById("contrasenaInicio").value;
    if(contrasena.length<4) {
       navigator.notification.alert(
          'Por favor, introduzca una contraseña de más de 4 caracteres', 
          null,         
          'contraseña demasiado corta',           
          'Aceptar'
        );
    }
    else
    {
        comprobarUser(function(){
        }); 
    }
}

function pagInicio() { 
   if(user===null || pass===null || user==="0" && pass==="0" || user==="null" || pass==="null") {         
       xhReq.open("GET", "pags/pagInicio.html", false);      
       xhReq.send(null);   
       document.getElementById("contenidoCuerpo").innerHTML=xhReq.responseText; 
       document.getElementById('menu').style.visibility = 'hidden';
       goMenu=true;
   }
   else
   {
      menu();    
   }   
}

function exit() {    
    
    user = null;
    window.localStorage.setItem("user", null); 
    pass =null;
    window.localStorage.setItem("pass", null); 
    idUser = null;
    window.localStorage.setItem("idUser", null); 
    nombre = null;
    window.localStorage.setItem("nombre", null); 
    apellidos = null;
    window.localStorage.setItem("apellidos", null); 
    deportes = null;
    window.localStorage.setItem("deportes", null); 
    iconoUser = "img/userIco.png";
    window.localStorage.setItem("iconoUser", "img/userIco.png");     
    
    pagInicio();
}

function selectLicencia() {
    if(aceptLicencia.checked) {
        document.getElementById('btRegistro').disabled = false;        
        document.getElementById("btRegistro").style.background = "#2B72CC";
        document.getElementById("btRegistro").style.background = "-webkit-linear-gradient(top, #4c9bbc, #275c98)";
    }
    else {
        document.getElementById('btRegistro').disabled = true;
        document.getElementById("btRegistro").style.background =  "#cedce7";
        document.getElementById("btRegistro").style.background =  "-webkit-linear-gradient(top, #cedce7, #596a72)";    
    }
}

function iniciarSesion(){
   
   nextView = "deportConfig()";
   if((user===null || pass===null) || (user==="0" && pass==="0") || (user==="null" || pass==="null")) {  
        xhReq.open("GET", "pags/inicioSesion.html", false);      
        xhReq.send(null);   
        document.getElementById("contenidoCuerpo").innerHTML=xhReq.responseText;
        document.getElementById('menu').style.visibility = 'hidden';        
        goMenu=false;
   }
   else {
        menu();
        if(bOtenerIcon) {
          obtenerIcon(function() {
            document.getElementById('menu').style.visibility = 'visible';   
            var d = new Date();
            document.getElementById("menu").innerHTML = "<img align='left' id='logoIzda' class='logo' onclick=\"configureUser();\" src='" + iconoUser + "?" + d.getTime() + "' height='45' width='45'/><div class='menuTitle'>Menú</div><img  id='logoDcha' onclick=\"changeImage(this,'exit2.png','exit.png');setTimeout(function(){exit();});\" src='img/exit.png' height='45' width='45'/>";
          }); 
          bOtenerIcon=false;
        } else {
            document.getElementById('menu').style.visibility = 'visible';   
            var d = new Date();
            document.getElementById("menu").innerHTML = "<img align='left' id='logoIzda' class='logo' onclick=\"configureUser();\" src='" + iconoUser + "?" + d.getTime() + "' height='45' width='45'/><div class='menuTitle'>Menú</div><img  id='logoDcha' onclick=\"changeImage(this,'exit2.png','exit.png');setTimeout(function(){exit();});\" src='img/exit.png' height='45' width='45'/>";
        }
   }       
}

function deportConfig() {        
        nextView = "pagInicio()";
        nextConf = "deportConfig()";
        alerNone();
        var d = new Date();
        document.getElementById('menu').style.visibility = 'visible';         
        document.getElementById("menu").innerHTML = "<img align='left' id='logoIzda' class='logo' src='" + iconoUser + "?" + d.getTime() + "' height='45' width='45'/><div class='menuTitle'>¿Qué deporte practicas?</div><img  id='logoDcha' onclick=\"changeImage(this,'exit2.png','exit.png');setTimeout(function(){exit();});\" src='img/exit.png' height='45' width='45'/>";
        xhReq.open("GET", "pags/deportConfig.html", false);    
        xhReq.send(null);   
        document.getElementById("contenidoCuerpo").innerHTML=xhReq.responseText;
        goMenu=false;
        selectDeportes();
}

function deportConfigUser() {
        nextView = "configureUser()";
        nextConf = "deportConfigUser()";
        var d = new Date();
        alerNone();
        document.getElementById('menu').style.visibility = 'visible';         
        document.getElementById("menu").innerHTML = "<img align='left' id='logoIzda' class='logo' src='" + iconoUser + "?" + d.getTime() + "' height='45' width='45'/><div class='menuTitle'>¿Qué deporte practicas?</div><img  id='logoDcha' onclick=\"changeImage(this,'exit2.png','exit.png');setTimeout(function(){exit();});\" src='img/exit.png' height='45' width='45'/>";
        xhReq.open("GET", "pags/deportConfig.html", false);    
        xhReq.send(null);   
        document.getElementById("contenidoCuerpo").innerHTML=xhReq.responseText;
        goMenu=false;
        selectDeportes();
}

function loading() {
    
    document.getElementById("contenidoCuerpo").innerHTML="<div align=\"center\" style=\" position: absolute;	top: 50%; left:50%;	height: 30%;	width: 50%;	margin: -15% 0 0 -25%;\" ><img  src=\"img/loading2.gif\" /></div>";
}


function listaEventos() {
        loading();   
        nextView = "pagInicio()";
        var query = "Select * from EVENTOS where ((DIA >=CURRENT_DATE) "; //(ADMINISTRADOR='" + user + "') ";  

       var comparador = "and (";

       if(arrDeportes!==null)
        {       


            for (var i=1;i<arrDeportes.length;i++){        

                if(arrDeportes[i]!=="8"){
                    query += comparador + "(DEPORTE='" + indexDeportes[i] + "')";
                    comparador = " or ";
                }
            }
            if(comparador === " or ")
                query += ")";
        }
        else{
            
        }
            
        query += ") order by DIA";

  
        obtenerEventos(function() {  
            var d = new Date();            
            document.getElementById('menu').style.visibility = 'visible';         
            document.getElementById("menu").innerHTML = "<img align='left' id='logoIzda' class='logo' src='" + iconoUser + "?" + d.getTime() + "' height='45' width='45'/><div class='menuTitle'>Eventos de tu interés</div><img  id='logoDcha' onclick=\"changeImage(this,'exit2.png','exit.png');setTimeout(function(){exit();});\" src='img/exit.png' height='45' width='45'/>";
            xhReq.open("GET", "pags/listaEventos.html", false);    
            xhReq.send(null);   
            document.getElementById("contenidoCuerpo").innerHTML=xhReq.responseText;
            var dias_semana = new Array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado");
            var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre", "Diciembre");            
            var listEvents = "";            
            for(var x=0; x<eventos.length; x++) {
                listEvents += "<a href=javascript:verEvento('" + x +"');>";  
                listEvents += "<div  class='modEvent'>";
                listEvents += "<div class='modLis1'>"; 
                listEvents += "<time class='icon'>";
                var nd = new Date(eventos[x]["dia"]);
                listEvents += "<em>" + dias_semana[nd.getDay()] + "<br>" + eventos[x]["hora"].substring(0, eventos[x]["hora"].length-3); + "</em>";
                listEvents += "<strong>" + meses[nd.getMonth()] + "</strong>";
                listEvents += "<span>" + nd.getDate() + "</span>";
     
                listEvents += "</time>";
                listEvents += "</div>";
                listEvents += "<div class='modLis2'>";
                      
                listEvents += "<h2 style='color: white'>" + eventos[x]["titulo"] + "</h2>";
                listEvents += "<h3 style='color: white'>" + eventos[x]["lugar"] + "</h3>";
                
                
                listEvents += "</div>";
                listEvents += "<div class='modLis3'>";
                listEvents += "<img src='img/d" + indexDeportes.indexOf(eventos[x]["deporte"]) + ".png' />";
               listEvents += "</div>";
               listEvents += "</div><br><br>";               
               listEvents += "</a>";
               
            } 
            document.getElementById("listaEventos").innerHTML=listEvents;
            goMenu=false;
        }, query);  
        
}

function exportToCalendar(numEvent,resumen) {

     var sDate = eventos[numEvent]["dia"].split("-");
     var eDate = eventos[numEvent]["dia"].split("-");

     var startDate = new Date(sDate[0],sDate[1]-1,sDate[2],0,0,0,0,0); // beware: month 0 = january, 11 = december
     var endDate = new Date(eDate[0],eDate[1]-1,eDate[2],0,0,0,0,0);
     var title =  eventos[numEvent]["titulo"];
     var location =  eventos[numEvent]["lugar"] ;
     var notes =  resumen;
     var success = function(message) { /*alert("Success: " + JSON.stringify(message));*/ };
     var error = function(message) { alert("Error: " + message); };

     window.plugins.calendar.createEventInteractively(title,location,notes,startDate,endDate,success,error);


}

function verEvento(numEvent) {    
    nextView = "listaEventos()";
    loading();
     var d = new Date();           
           
     getUsersEventos(function() {       
        idenEvento=eventos[numEvent]["id"];
        numEvento = numEvent;
        document.getElementById('menu').style.visibility = 'visible';         
        document.getElementById("menu").innerHTML = "<img align='left' id='logoIzda' class='logo' src='" + iconoUser + "?" + d.getTime() + "' height='45' width='45'/><div class='menuTitle'>Información del evento</div><img  id='logoDcha' onclick=\"changeImage(this,'exit2.png','exit.png');setTimeout(function(){exit();});\" src='img/exit.png' height='45' width='45'/>";
        xhReq.open("GET", "pags/verEvento.html", false);    
        xhReq.send(null);   
        document.getElementById("contenidoCuerpo").innerHTML=xhReq.responseText;
        var dias_semana = new Array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado");
        var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre", "Diciembre");            
        var contEvent = "";            
        contEvent += "<div  class='modEvent'>";
        contEvent += "<div class='modLis1'>";      
        contEvent += "<time class='icon'>";
        var nd = new Date(eventos[numEvent]["dia"]);
        contEvent += "<em>" + dias_semana[nd.getDay()] + "<br>" + eventos[numEvent]["hora"].substring(0, eventos[numEvent]["hora"].length-3) + "</em>";
        contEvent += "<strong>" + meses[nd.getMonth()] + "</strong>";
        contEvent += "<span>" + nd.getDate() + "</span>";
        contEvent += "</time>";    
        contEvent += "</div>";
        contEvent += "<div class='modLis2'>";                          
        contEvent += "<h2 style='color: white'>" + eventos[numEvent]["titulo"] + "</h2>";
        contEvent += "<h3 style='color: white'>" + eventos[numEvent]["lugar"] + "</h3>";
        contEvent += "</div>";
        contEvent += "<div class='modLis3'>";
        contEvent += "<img src='img/d" + indexDeportes.indexOf(eventos[numEvent]["deporte"]) + ".png' />";
        contEvent += "</div><br><br>";
        contEvent += "<div id='map2'></div><br><br>";
        var userApunt = false;

         // Iconos de redes sociales
        var resumen = eventos[numEvent]["titulo"]+ ", el " +   dias_semana[nd.getDay()] + " " + nd.getDate() + " " + meses[nd.getMonth()] + " a las " +  eventos[numEvent]["hora"].substring(0, eventos[numEvent]["hora"].length-3);

        contEvent += "<img src='img/compartir.png' height='65' width='65' onclick=\"changeImage(this,'compartir2.png','compartir.png');setTimeout(function(){window.plugins.socialsharing.share('" +eventos[numEvent]["titulo"]+ ", el " +   dias_semana[nd.getDay()] + " " + nd.getDate() + " " + meses[nd.getMonth()] + " a las " +  eventos[numEvent]["hora"].substring(0, eventos[numEvent]["hora"].length-3) +"', '"+eventos[numEvent]["titulo"]+"')},400);\";\">";
        contEvent += "<img src='img/twitter.png' height='65' width='65' onclick=\"changeImage(this,'twitter2.png','twitter.png');setTimeout(function(){window.plugins.socialsharing.shareViaTwitter('" +eventos[numEvent]["titulo"]+ ", el " +   dias_semana[nd.getDay()] + " " + nd.getDate() + " " + meses[nd.getMonth()] + " a las " +  eventos[numEvent]["hora"].substring(0, eventos[numEvent]["hora"].length-3) +"', null /* img */, null /* url */, function() {console.log('share ok')}, function(errormsg){alertAppNoDisponible('Twitter')})},400);\";\">";
        contEvent += "<img src='img/facebook.png' height='65' width='65' onclick=\"changeImage(this,'facebook2.png','facebook.png');setTimeout(function(){window.plugins.socialsharing.shareViaFacebook('" +eventos[numEvent]["titulo"]+ ", el " +   dias_semana[nd.getDay()] + " " + nd.getDate() + " " + meses[nd.getMonth()] + " a las " +  eventos[numEvent]["hora"].substring(0, eventos[numEvent]["hora"].length-3) +"', null /* img */, null /* url */, function() {console.log('share ok')}, function(errormsg){alertAppNoDisponible('Facebook')})},400);\";\">";
        contEvent += "<img src='img/whatsapp.png' height='65' width='65' onclick=\"changeImage(this,'whatsapp2.png','whatsapp.png');setTimeout(function(){window.plugins.socialsharing.shareViaWhatsApp('" +eventos[numEvent]["titulo"]+ ", el " +   dias_semana[nd.getDay()] + " " + nd.getDate() + " " + meses[nd.getMonth()] + " a las " +  eventos[numEvent]["hora"].substring(0, eventos[numEvent]["hora"].length-3) +"', null /* img */, null /* url */, function() {console.log('share ok')}, function(errormsg){alertAppNoDisponible('Whatsapp')})},400);\";\">";
 
    
        if(userEventos!==null) {        
            var cont = 0;
            for(var i=0; i<userEventos.length; i++){        
                if((userEventos[i]["id_evento"]===eventos[numEvent]["id"]) && (userEventos[i]["id_usuario"]===idUser)) {
                    userApunt = true;                
                }

                if((userEventos[i]["id_evento"]===eventos[numEvent]["id"])) {
                    if(cont===0)
                      contEvent += "<h3 style='color: white'>Participantes : </h3>";
                    cont++;
                    if(cont%2)
                       contEvent += "<div style='float: left; width: 50%;'>";
                    else 
                        contEvent += "<div style='float: right;  width: 50%;'>";
                    contEvent += "<img  id='icon" + userEventos[i]["id_usuario"] + "' class='logoUser' src='img/userIco.png' height='65' width='65'/>";
                    contEvent += "<div style='color: white'>" + userEventos[i]["nombre"]  + "</div>";
                    contEvent +="<br></div>";
                    if(!cont%2)
                        contEvent +="<br>";
                   obtenerIconUsers(function() {                
                   }, userEventos[i]["id_usuario"]);
                }
            }
         }        
 
        
         if(userApunt)
            contEvent += "<br><br><br><br><button id='btApuntarEvento' type='button' value='Borrarme del evento' onclick='javascript:setTimeout(function(){borrarEvento(" + eventos[numEvent]["id"] + ");}, 400);' >Borrarme del evento</button><br><br>";
        else
            contEvent += "<br><br><br><br><button id='btApuntarEvento' type='button' value='Apuntame al evento' onclick='javascript:setTimeout(function(){apuntarEvento(" + eventos[numEvent]["id"] + ");}, 400);' >Apuntame al evento</button><br><br>";
        
        contEvent += "<div style='float: left; width: 50%;'><img src='img/calendar.png' height='65' width='65' onclick=\"changeImage(this,'calendar2.png','calendar.png');setTimeout(function(){javascript:exportToCalendar('" + numEvent + "','" + resumen + "');},400);\";\"></div>";
        
        contEvent += "<div style='float: right;  width: 50%;'><a href=\"javascript:addComent();\"><img  class=\"imCommnet\" src=\"img/comment.png\"height=\"48\" width=\"120\" ></p></a></div>";
        contEvent += "<div id='idComent'></div>";
        getComments(function() {
          var contEv="";
          if(comments!==null) {
          for (var i=0; i < comments.length; i++) {
            if(comments[i]["id_evento"] === eventos[numEvent]["id"]) {
                contEv += "<div style='color: white'>Usuario: <b>" + comments[i]["usuario"] + "</b> fecha: " + comments[i]["fecha"] + "</div>";
                contEv += "<p class=\"triangle-border top\">" + comments[i]["comentario"] + "</p>";
             }      
           }
          }   
          document.getElementById("idComent").innerHTML =contEv;},eventos[numEvent]["id"]);       
       
        contEvent += "<br><br><br><br></div>";
        // MOSTRAR Y CREAR FORO EVENTO!!!!

        document.getElementById("listaEvento").innerHTML=contEvent;
        
        setLog("AppConsume", "Event information");
        
        initializeMap2(eventos[numEvent]["coordenadas"]); 
          var coord = eventos[numEvent]["coordenadas"].split(',');
          var myLatlng = new google.maps.LatLng(coord[0],coord[1]);
          var txtUrl = "img/icoMap" + indexDeportes.indexOf(eventos[numEvent]["deporte"]) + ".png";  
          var marker = new google.maps.Marker({
                      position: myLatlng,
                      icon:txtUrl,
                      map: map2,
                     title:eventos[numEvent]["titulo"]
  });
    goMenu=false;
     },eventos[numEvent]["id"]);
    
}

function alertAppNoDisponible(nombreApp) {
    
    navigator.notification.alert(
        'Debes de tener la aplicación de ' + nombreApp + ' instalada para poder compartir', // message
        null,            // callback to invoke with index of button pressed
        'Aplicación de ' + nombreApp + ' no instalada',           // title
        'Cerrar');
    
}

 function changeImage(element, img, img2) {
     element.src = "img/" + img;
     setTimeout(function(){element.src = "img/" + img2;},400);
 }

function addComent() {        
    var d = new Date();
    document.getElementById('menu').style.visibility = 'visible';   
    document.getElementById("menu").innerHTML = "<img align='left' id='logoIzda' class='logo' src='" + iconoUser + "?" + d.getTime() + "' height='45' width='45'/><div class='menuTitle'>Añadir comentario evento</div><img  id='logoDcha' onclick=\"changeImage(this,'exit2.png','exit.png');setTimeout(function(){exit();});\" src='img/exit.png' height='45' width='45'/>";
    xhReq.open("GET", "pags/comentarios.html", false);      
    xhReq.send(null);
    document.getElementById("contenidoCuerpo").innerHTML=xhReq.responseText;
    document.getElementById("nickUserComment").value = nombre;
 
}



function configureUser() {
        nextView = "pagInicio()";        
        var d = new Date();
        document.getElementById('menu').style.visibility = 'visible';   
        document.getElementById("menu").innerHTML = "<img align='left' id='logoIzda' class='logo' src='" + iconoUser + "?" + d.getTime() + "' height='45' width='45'/><div class='menuTitle'>Configuración del perfil</div><img  id='logoDcha' onclick=\"changeImage(this,'exit2.png','exit.png');setTimeout(function(){exit();});\" src='img/exit.png' height='45' width='45'/>";
        
        xhReq.open("GET", "pags/configureUser.html", false);    
        xhReq.send(null);   
        document.getElementById("contenidoCuerpo").innerHTML=xhReq.responseText;  
        goMenu=false;
        if(nombre !== null)
            document.getElementById("confNombreUser").value = nombre; // gcmId;
        if(apellidos !== null)            
            document.getElementById("confApellidosUser").value = apellidos;
        
        if(iconoUser=="img/userIco.png")
            document.getElementById("logoUser").src = "img/editUser.png";
        else
            document.getElementById("logoUser").src = iconoUser + "?" + d.getTime();

        if(telefono !== null)        
            document.getElementById("confTelefonoUser").value = telefono;
  
        if(sexo !== null)            
        {
            if(sexo===0)
            {
                document.getElementById("rMasculino").checked=true;
            }
            else
            {
                document.getElementById("rFemenino").checked=true;
            }
        }

        var sinDeportes = true;
        if(arrDeportes!==null)
        {            
            var divDep = " <div  class='mod2'><div class='wrapper'>";

            for (i=1;i<arrDeportes.length;i++){                

                if(arrDeportes[i]!=="8"){
                    sinDeportes = false;
                    divDep+="<a href='javascript:deportConfigUser();' class='internal'><img src='img/d" + i + ".png' /><img src='img/nivel" + arrDeportes[i] + ".png' /></a>";
                    
                }
             }
             divDep+="</div></div>";
        }
        if(sinDeportes) {
            divDep = "<div style='alignment-adjust: center;'>";
            divDep += "<button id='close' type='button' value='AddDeporte' onclick='javascript:setTimeout(function(){deportConfigUser();}, 400);' >Añadir deportes</button>"; 
            divDep += "</div>";
        }
        
        document.getElementById("modD2").innerHTML = divDep;
}




function infotfn() {    
    navigator.notification.alert(
            'El número de teléfono sólo será visible, por el administrador del evento al que te apuntes', 
             null, 
             'Información número teléfono', 
             'Cerrar');             
}

function selectDeportes() {

    for (i=1;i<arrDeportes.length;i++){
        var id = "botStatus" + i;
        if(arrDeportes[i]==="8"){
            document.getElementById(id).style.background = "#ffffff";     
        }
        else{
            document.getElementById(id).style.background = "#2B72CC";
            document.getElementById(id).style.background = "-webkit-linear-gradient(top, #4c9bbc, #275c98)";
        }
    }
}

function getGps() { 
    var locFail = function() {
        navigator.notification.confirm (
          '¡La ubicación no esta activa, o no tiene cobertura GPS!. Puede que la localización no sea exacta ', // message
           null,            // callback to invoke with index of button pressed
          'Ubicación desactivada',           // title
           'Aceptar'
       );
    };
    navigator.geolocation.getCurrentPosition(setGeolocation, locFail,{enableHighAccuracy: false, timeout: 10000});
} 



function  guaradarConfDep(idem){    
    var id = "botStatus" + idem;

    if(document.getElementById("cbNivel").checked){
        botStatus[id] = "1";    
        arrDeportes[idem] = document.getElementById("inNivel").value;
    }
    else {
        arrDeportes[idem] = "8";
        botStatus[id] = "0";
    }
    deportes = arrDeportes.toString();

    window.localStorage.setItem("deportes", deportes);    
    statusBoton(id);   

    document.getElementById("divAlerts").style.display = "none";
    document.getElementById("contenidoCuerpo").style.opacity = "1";  
    goMenu=false;

}


function cargarRecintos() {
    
    document.getElementById('direccion').value= '';
    var vRecintos="<select id='selRecintos' onchange='cargarDireccion(this)' class='selButton2'>";   
    var cargarD = true;
    for (var i=0; i<allRecintos.length; i++) {   
        if(allRecintos[i]["deportes"].indexOf(',') < 0) {            
               if(allRecintos[i]["deportes"] ===  document.getElementById("selDeporte").value){
                    vRecintos += "<option value='" + allRecintos[i]["ubicacion"] +"'>" + allRecintos[i]["nombre"] + "</option>";                         
                    if(cargarD){
                        document.getElementById('direccion').value=allRecintos[i]["ubicacion"];
                        centrarPos(allRecintos[i]["ubicacion"]);
                        cargarD = false;
                    }
               }            
        }
        else
        {
            var arrDeport = allRecintos[i]["deportes"].split(',');        
            for(var h=0; h<arrDeport.length; h++) {              
               if(arrDeport[h] ===  document.getElementById("selDeporte").value){
                    vRecintos += "<option value='" + allRecintos[i]["ubicacion"] +"'>" + allRecintos[i]["nombre"] + "</option>";   
                    if(cargarD){
                        document.getElementById('direccion').value=allRecintos[i]["ubicacion"];
                        centrarPos(allRecintos[i]["ubicacion"]);
                        cargarD = false;
                    }
                    break;
               }
            }
        }        
    }
    vRecintos +="</select>";
    document.getElementById("selRecintos").innerHTML=vRecintos;
}



function cargarDireccion(element) {
    
    var idx=element.selectedIndex;
    var direcc=element.options[idx].value;
    document.getElementById('direccion').value=direcc;
    centrarPos(direcc);  
}


function changeButton (idem){  
    
    indice = arrDeportes[idem];
    nextView = nextConf;
    document.getElementById("divAlerts").style.display = "block";
    document.getElementById("contenidoCuerpo").style.opacity = "0.2";   

    var vAler = "<div class='cabeceraConfig'><img class='logoConfDep' src='img/d" + idem + ".png' align='left'/><div class='switch demo3' style='text-align: left; margin:5%; top:15%;' ><input type='checkbox' id='cbNivel' checked><label><i></i></label></div><a href='#' onclick='javascript:alerNone()'><img class='logoClose' src='img/icoClose.png' align='right'  /></a></div>";
    
    vAler += "<div class='cuerpoConfig'>";
    vAler += "<div style='text-align: left; margin-left:10%;'> <h3>Selecciona tu nivel:<h3></div>";
    if(indice==="8")
        indice = "2";

    vAler += "<div class='clrange'><div class='range-container'><div class='slider-container'><div class='range-fill'></div><input class='slider' id='inNivel' type='range' name='testerslider' min='0' max='4' value='" + indice + "' step='0.01' list='testlist'></div><div class='label1-container'><div class='label1'>Primera vez</div><div class='label1'>Principiante</div><div class='label1'>Medio</div><div class='label1'>Avanzado</div><div class='label1'>Pro</div></div></div></div>";
    vAler += "</div>";
    vAler += "<div class='pieConfig'>";
    vAler += "<button id='accept' type='button' value='Aceptar' onclick='javascript:setTimeout(function(){javascript:guaradarConfDep("+idem+");}, 400);' >Guardar</button>";
  
    vAler+="</div>";    
    document.getElementById("divAlerts").innerHTML=vAler;
    iniBar();
  
     $(".label1:nth-child(" + (parseInt(indice) + 1) + ")").css("opacity", "1");
  
 
}



function alerNone() {
    
    document.getElementById("divAlerts").style.display = "none";
    document.getElementById("contenidoCuerpo").style.opacity = "1";   
    
}
  
function statusBoton(id){ 
    if(botStatus[id]==="0"){
    document.getElementById(id).style.background = "#ffffff";
  }
  else{

      document.getElementById(id).style.background = "#2B72CC";
      document.getElementById(id).style.background = "-webkit-linear-gradient(top, #4c9bbc, #275c98)";
  }                 
}

function menu() {
   var d = new Date();
   document.getElementById('menu').style.visibility = 'visible';   
   document.getElementById("menu").innerHTML = "<img align='left' id='logoIzda' class='logo' onclick=\"configureUser();\" src='" + iconoUser + "?" + d.getTime() + "' height='45' width='45'/><div class='menuTitle'>Menú</div><img  id='logoDcha' onclick=\"changeImage(this,'exit2.png','exit.png');setTimeout(function(){exit();});\" src='img/exit.png' height='45' width='45'/>";
   xhReq.open("GET", "pags/menu.html", false);      
   xhReq.send(null);  
   document.getElementById("contenidoCuerpo").innerHTML=xhReq.responseText;
   goMenu=true;
}

function crearEvento(){    
   
   var d = new Date();
   nextView = "pagInicio()";  
   document.getElementById('menu').style.visibility = 'visible';   
   document.getElementById("menu").innerHTML = "<img align='left' id='logoIzda' class='logo' src='" + iconoUser + "?" + d.getTime() + "' height='45' width='45'/><div class='menuTitle'>Organizar evento</div><img  id='logoDcha' onclick=\"changeImage(this,'exit2.png','exit.png');setTimeout(function(){exit();});\" src='img/exit.png' height='45' width='45'/>";
   xhReq.open("GET", "pags/altaEvento.html", false);      
   xhReq.send(null);  
   document.getElementById("contenidoCuerpo").innerHTML=xhReq.responseText;
   goMenu=false;
   
   $( "#slider" ).slider({
	range: true,
	values: [1, 5]
});

var steps = [
    "Primera vez",
    "Iniciación",
    "Avanzado",
    "Experto",
    "Pro"
];

 $("#slider-r").slider({
      range: true,
        value: 1,
        min: 0,
        max: 4,        
        step: 1,
        slide: function(event, ui) {
//            $("#amount").val(steps[ui.value]);
            $( "#amount" ).val(steps[ui.values[0]] + " - " + steps[ui.values[1]] );
        }
    });
    $("#amount").val(steps[$("#slider").slider("value",0)]);

   initialize2();

}


function altaEvento() {
  
      var nombreEvento = document.getElementById("nombreEvent").value;
      var selDeporte = document.getElementById("selDeporte").value;
      var direccionEvento = document.getElementById("direccion").value;
      var diaEvento = document.getElementById("dia").value;

      var horaEvento = document.getElementById("hora").value;

  
      var bMasculino = document.getElementById("radio1").checked;
      var bFemenino = document.getElementById("radio2").checked;
      var bMixto = document.getElementById("radio3").checked;
      var sexEvent = 2;
      if(bMasculino)
          sexEvent=0;
      if(bFemenino)
          sexEvent=1;
  
      var bInscripcion =  false; //document.getElementById("roundedOne").checked;
      var bRecordar = false; // document.getElementById("roundedOne3").checked;
  
      var nivelesEvento =  document.getElementById("amount").value;  
      var niveles = '';
      if(nivelesEvento === "Primera vez")
          niveles = "1";
      if(nivelesEvento === "Primera vez - Iniciación")
          niveles = "0,1";
      if(nivelesEvento === "Primera vez - Avanzado")
          niveles = "0,1,2";
      if(nivelesEvento === "Primera vez - Experto")
          niveles = "0,1,2,3";
      if(nivelesEvento === "Primera vez - Pro")
          niveles = "0,1,2,3,4";      
      if(nivelesEvento === "Iniciación")
          niveles = "1";
      if(nivelesEvento === "Iniciación - Avanzado")
          niveles = "1,2";
      if(nivelesEvento === "Iniciación - Experto")
          niveles = "1,2,3";
      if(nivelesEvento === "Iniciación - Pro")
          niveles = "1,2,3,4";    
      if(nivelesEvento === "Avanzado")
          niveles = "2";
      if(nivelesEvento === "Avanzado - Experto")
          niveles = "2,3";
      if(nivelesEvento === "Avanzado - Pro")
          niveles = "2,3,4";       
      if(nivelesEvento === "Experto")
          niveles = "3";
      if(nivelesEvento === "Experto - Pro")
          niveles = "3,4"; 
      if(nivelesEvento === "Pro")
          niveles = "4";      

      if(selDeporte === "Deportes...") {
           navigator.notification.alert(
                'Debe seleccionar un deporte para poder crear un evento', // message
                null,            // callback to invoke with index of button pressed
                'Deporte no selccionado',           // title
                'Cerrar' 
            );
            return false;
      }
      if(direccionEvento === null) {
              navigator.notification.alert(
                'Debe indicar la dirección donde se va a organizar el evento', // message
                null,            // callback to invoke with index of button pressed
                'Dirección no indicacada',           // title
                'Cerrar' 
            );
            return false;
      } 

      if(validarFechaMenorActual(diaEvento))
      {
        navigator.notification.alert(
                'La fecha del evento no puede ser inferior a la actual', // message
                null,            // callback to invoke with index of button pressed
                'Fecha de evento erronea',           // title
                'Cerrar' 
            );    
            return false;
      }     

      geocoder.geocode( { 'address': direccionEvento}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {              
              

              var coord1 = results[0].geometry.location;

              var coord =  coord1 + "";              
              var coordf = coord.substring(1, coord.length-1);

              addEvento(nombreEvento, selDeporte, direccionEvento, coordf, diaEvento, horaEvento, sexEvent , niveles, bInscripcion, bRecordar, false);
          }
          else{
            addEvento(nombreEvento, selDeporte, direccionEvento, "", diaEvento, horaEvento, sexEvent , niveles, bInscripcion, bRecordar, false);
        }
    });
      
}

function validarFechaMenorActual(date){
      var x=new Date();
      var fecha = date.split("-");
      x.setFullYear(fecha[0],fecha[1]-1,fecha[2]);
      var today = new Date();
 
      if (x >= today)
        return false;
      else
        return true;
}


function setLog(typeMens, message) {
  var d = new Date();
  var n = d.getTime();	
  n = n / 1000;      
  var urlAj = "https://iescities.com/IESCities/api/log/app/event/" + n + "/Majadahonda Sports4U/" + numSessionLog + "/" + typeMens + "/" + message;
  $.ajax({        
 	url: urlAj,       
    type:'POST', 
    data: '', 
    dataType:'json', 
    error:function(jqXHR,text_status,strError){         
      console.log('No connection IESCities api log');

    },
    success:function(data){      

    }
  });  
}


function iniBar() {
    
   $(document).ready(function(){
   var $slider = $('input[type=range]');
   var max = parseInt($slider.attr('max'));
   var lastChangeHandled;
 
    function convertTouchXToFraction(touchX) {
        //Figure out where it will be on the slider
        var sliderOffset = $slider.offset();
        var normalizedX = touchX - sliderOffset.left;
        return normalizedX / $slider.width();
    }

    function changeHandler(event) {
        var value = $slider.val();
        if (lastChangeHandled !== value) {
            lastChangeHandled = value;
            console.log(value);
            var frac = value / max;
            var rangeWidth = (frac * 100) + "%";
            $('.range-fill').css({"width": rangeWidth, "min-width": "3%", "max-width": "98%"});
            $slider.hide().show(0);
        }
    }
    var lastSnap;

    function setLastSnap() {
        lastSnap = (new Date()).getTime();
    };
    setLastSnap();

    var touchRegexp = /^touch/;

    function syncValToXCreator(snap) {
        return function(event) {
            var epoch = (new Date()).getTime();
            var timeDiff = epoch - lastSnap;
            // We don't handle any move events for THIS NUMBER of milliseconds after a snap.
            // This is required because on iPad you get:
            //  touchEnd -> 300ms delay -> mouseMove
            var throttleMs = 400;
            if (timeDiff > throttleMs &&
                  (event.which === 1 || touchRegexp.test(event.type))) {

                var touchX = event.originalEvent.clientX || event.originalEvent.changedTouches[0].clientX;
                var frac = convertTouchXToFraction(touchX);
                var inputValue;

                inputValue = frac * max
                if (snap) {
                    inputValue = Math.round(inputValue);
                    setLastSnap();
                    $slider.trigger("shp.snap", [inputValue]);
                }

                $slider.val(inputValue);
                // jQuery does not trigger change for non-user interactions.
                changeHandler(event);
            }
        }
    }
    $slider.on("shp.snap", function(event, value){
        $(".label1").css("opacity", "0");
        $(".label1:nth-child(" + (value + 1) + ")").css("opacity", "1");
    });

    // Snap to closest value
	$slider.on("touchend mouseup click", syncValToXCreator("snap"));
    $(".label1").on("touchend mouseup click", syncValToXCreator("snap"));

    // Modify range-fill as ball move.
	$slider.on("touchmove mousemove click", syncValToXCreator());
    $(".label1").on("touchend mouseup click", syncValToXCreator("snap"));
});
}


//app.initialize();

