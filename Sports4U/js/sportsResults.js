function getResultados(){  
    
   nextView = "pagInicio()";
   Restlogging.appLog("AppConsume", "Sports results");
   
   document.getElementById('menu').style.visibility = 'visible';   
   var d = new Date();
   document.getElementById("menu").innerHTML = "<img align='left' id='logoIzda' class='logo' src='" + iconoUser + "?" + d.getTime() + "' height='45' width='45'/><div class='menuTitle'>Resultados y clasificaciones</div><img  id='logoDcha' onclick=\"changeImage(this,'exit2.png','exit.png');setTimeout(function(){getResultados();});\" src='img/exit.png' height='45' width='45'/>";

   xhReq.open("GET", "pags/resultados.html", false);      
   xhReq.send(null);  
   document.getElementById("contenidoCuerpo").innerHTML=xhReq.responseText;
   goMenu=false;
  // document.getElementById("tablaResultados").innerHTML="<div align=\"center\" style=\" position: absolute;	top: 200px; left:50%;	height: 30%;	width: 50%;	margin: -15% 0 0 -25%;\" ><img  src=\"img/loading2.gif\" /></div>";
   
  // var vSeleccion = null;
   
//   vSeleccion = "<select id='comboClasResul' onchange='cargarTemporadas(this)' class='selButton2'>";
//   vSeleccion += "<option value='Clasificacion'>Clasificación</option>"; 
//   vSeleccion += "<option value='Resultados'>Resultados</option>"; 
//   vSeleccion +="</select>"; 
//   document.getElementById("comboClasResul").innerHTML=vSeleccion;
//   cargarTemporadas(this);
//onClickClasificacion(); 
   
}

function onClickClasificacion(){
    document.getElementById("rClasificicaion").checked=true;
    document.getElementById("rResultados").checked=false;
     document.getElementById("tablaResultados").innerHTML="<div align=\"center\" style=\" position: absolute;	top: 200px; left:50%;	height: 30%;	width: 50%;	margin: -15% 0 0 -25%;\" ><img  src=\"img/loading2.gif\" /></div>";
     obtenerTemporadasClasificacion(function() {
        if(temporadas!==null) {
            vTemporadas="<select id='comboTemporadas' onchange='cargarCompeticionClasificacion(this)' class='selButton2'>";  
            for (var i=0; i<temporadas.length; i++) {
                vTemporadas += "<option value='" + temporadas[i] +"'>Temporada : " + temporadas[i] + "</option>";                  
            }                       
            selCompeticionClasificacion(temporadas[0]);
        }
        else {
            vTemporadas  = "¡SIN RESULTADOS!";
            // Añadir mensaje error!!!
        }
        vTemporadas +="</select>"; 
        document.getElementById("comboTemporadas").innerHTML=vTemporadas;        
    });       
}

function onClickResultados(){
    document.getElementById("rClasificicaion").checked=false;
    document.getElementById("rResultados").checked=true;
     document.getElementById("tablaResultados").innerHTML="<div align=\"center\" style=\" position: absolute;	top: 200px; left:50%;	height: 30%;	width: 50%;	margin: -15% 0 0 -25%;\" ><img  src=\"img/loading2.gif\" /></div>";
     obtenerTemporadas(function() {
        if(temporadas!==null) {
            vTemporadas="<select id='comboTemporadas' onchange='cargarCompeticion(this)' class='selButton2'>";  
            for (var i=0; i<temporadas.length; i++) {
                vTemporadas += "<option value='" + temporadas[i] +"'>Temporada : " + temporadas[i] + "</option>";                  
            }                       
            selCompeticion(temporadas[0]);
        }
        else {
            vTemporadas  = "¡SIN RESULTADOS!";
            // Añadir mensaje error!!!
        }
        vTemporadas +="</select>"; 
        document.getElementById("comboTemporadas").innerHTML=vTemporadas;
        
    });   
}



function cargarTemporadas(element) { 
    
   var vTemporadas=null;   
   var operacion = element.selectedIndex;
   if(operacion === 1) {
   obtenerTemporadas(function() {
        if(temporadas!==null) {
            vTemporadas="<select id='comboTemporadas' onchange='cargarCompeticion(this)' class='selButton2'>";  
            for (var i=0; i<temporadas.length; i++) {
                vTemporadas += "<option value='" + temporadas[i] +"'>Temporada : " + temporadas[i] + "</option>";                  
            }                       
            selCompeticion(temporadas[0]);
        }
        else {
            vTemporadas  = "¡SIN RESULTADOS!";
            // Añadir mensaje error!!!
        }
        vTemporadas +="</select>"; 
        document.getElementById("comboTemporadas").innerHTML=vTemporadas;
        
    });   
   }else
   {
      obtenerTemporadasClasificacion(function() {
        if(temporadas!==null) {
            vTemporadas="<select id='comboTemporadas' onchange='cargarCompeticionClasificacion(this)' class='selButton2'>";  
            for (var i=0; i<temporadas.length; i++) {
                vTemporadas += "<option value='" + temporadas[i] +"'>Temporada : " + temporadas[i] + "</option>";                  
            }                       
            selCompeticionClasificacion(temporadas[0]);
        }
        else {
            vTemporadas  = "¡SIN RESULTADOS!";
            // Añadir mensaje error!!!
        }
        vTemporadas +="</select>"; 
        document.getElementById("comboTemporadas").innerHTML=vTemporadas;
        
    });   
       
   }
}

function cargarCompeticion(element) {   
   document.getElementById("tablaResultados").innerHTML="<div align=\"center\" style=\" position: absolute;	top: 200px; left:50%;	height: 30%;	width: 50%;	margin: -15% 0 0 -25%;\" ><img  src=\"img/loading2.gif\" /></div>";
   var idx=element.selectedIndex;
   var temporada = element.options[idx].value;    
   var vCompeticiones=null;
    obtenerCompeticiones(function() {
        if(competiciones!==null) {
            vCompeticiones="<select id='comboCompeticiones' onchange=\"cargarJornadas(this,  '" + temporada + "')\" class='selButton2'>";  
      //     vCompeticiones += "<option value='" + competiciones[0] +"'>Clasificación</option>";
            for (var i=0; i<competiciones.length; i++) {
                vCompeticiones += "<option value='" + competiciones[i] +"'>" + competiciones[i] + "</option>";                  
            }
            selJornadas(temporada, competiciones[0]);
        }
        else {
             vCompeticiones = "¡SIN RESULTADOS!";
            // Añadir mensaje error!!!
        }
        vCompeticiones +="</select>"; 
        document.getElementById("comboCompeticiones").innerHTML=vCompeticiones;
        
    },temporada);   
}

function cargarCompeticionClasificacion(element) {   
   document.getElementById("tablaResultados").innerHTML="<div align=\"center\" style=\" position: absolute;	top: 200px; left:50%;	height: 30%;	width: 50%;	margin: -15% 0 0 -25%;\" ><img  src=\"img/loading2.gif\" /></div>";
   var idx=element.selectedIndex;
   var temporada = element.options[idx].value;    
   var vCompeticiones=null;
    obtenerCompeticionesClasificacion(function() {
        if(competiciones!==null) {
            vCompeticiones="<select id='comboCompeticiones' onchange=\"cargarJornadasClasificacion(this,  '" + temporada + "')\" class='selButton2'>";  
      //     vCompeticiones += "<option value='" + competiciones[0] +"'>Clasificación</option>";
            for (var i=0; i<competiciones.length; i++) {
                vCompeticiones += "<option value='" + competiciones[i] +"'>" + competiciones[i] + "</option>";                  
            }
            selJornadasClasificacion(temporada, competiciones[0]);
        }
        else {
             vCompeticiones = "¡SIN RESULTADOS!";
            // Añadir mensaje error!!!
        }
        vCompeticiones +="</select>"; 
        document.getElementById("comboCompeticiones").innerHTML=vCompeticiones;
        
    },temporada);   
}


function selCompeticion(temporada) {   
   document.getElementById("tablaResultados").innerHTML="<div align=\"center\" style=\" position: absolute;	top: 200px; left:50%;	height: 30%;	width: 50%;	margin: -15% 0 0 -25%;\" ><img  src=\"img/loading2.gif\" /></div>";
   var vCompeticiones=null;

    obtenerCompeticiones(function() {
        if(competiciones!==null) {
            vCompeticiones="<select id='comboCompeticiones' onchange=\"cargarJornadas(this,  '" + temporada + "')\" class='selButton2'>";  
            //vCompeticiones += "<option value='" + competiciones[0] +"'>Clasificación</option>";
            for (var i=0; i<competiciones.length; i++) {
                vCompeticiones += "<option value='" + competiciones[i] +"'>" + competiciones[i] + "</option>";                  
            }
            selJornadas(temporada,competiciones[0]);
           
        }
        else {
             vCompeticiones = "¡SIN RESULTADOS!";
            // Añadir mensaje error!!!
        }
        vCompeticiones +="</select>";         
        document.getElementById("comboCompeticiones").innerHTML=vCompeticiones;
        
    },temporada);   
}


function selCompeticionClasificacion(temporada) {   
   document.getElementById("tablaResultados").innerHTML="<div align=\"center\" style=\" position: absolute;	top: 200px; left:50%;	height: 30%;	width: 50%;	margin: -15% 0 0 -25%;\" ><img  src=\"img/loading2.gif\" /></div>";
   var vCompeticiones=null;

    obtenerCompeticionesClasificacion(function() {
        if(competiciones!==null) {
            vCompeticiones="<select id='comboCompeticiones' onchange=\"cargarJornadasClasificacion(this,  '" + temporada + "')\" class='selButton2'>";  
            //vCompeticiones += "<option value='" + competiciones[0] +"'>Clasificación</option>";
            for (var i=0; i<competiciones.length; i++) {
                vCompeticiones += "<option value='" + competiciones[i] +"'>" + competiciones[i] + "</option>";                  
            }
            selJornadasClasificacion(temporada,competiciones[0]);
           
        }
        else {
             vCompeticiones = "¡SIN RESULTADOS!";
            // Añadir mensaje error!!!
        }
        vCompeticiones +="</select>";         
        document.getElementById("comboCompeticiones").innerHTML=vCompeticiones;
        
    },temporada);   
}

function cargarJornadas(element, temporada) {  
    document.getElementById("tablaResultados").innerHTML="<div align=\"center\" style=\" position: absolute;	top: 200px; left:50%;	height: 30%;	width: 50%;	margin: -15% 0 0 -25%;\" ><img  src=\"img/loading2.gif\" /></div>";
    var idx=element.selectedIndex;
    var competicion = element.options[idx].value;    
    var vJornadas=null;
    obtenerJornadas(function() {
        if(jornadas!==null) {
            vJornadas="<select id='comboJornadas' onchange=\"cargarResultados(this, '" + temporada + "', '" + competicion + "')\" class='selButton2'>";  
            //vJornadas += "<option value='" + jornadas[0] +"'>CLASIFICACIÓN</option>";  
            for (var i=0; i<jornadas.length; i++) {
                vJornadas += "<option value='" + jornadas[i] +"'> Jornada " + jornadas[i] + "</option>";   
            }
            selResultados(temporada,jornadas[0],competicion);
            //selClasificacion(temporada,competicion);
        }
        else {
             vJornadas = "¡SIN RESULTADOS!";
            // Añadir mensaje error!!!
        }
        vJornadas +="</select>";  
         
        document.getElementById("comboJornadas").innerHTML=vJornadas;
    },temporada, competicion);       
}


function selJornadas(temporada, competicion) {    
    document.getElementById("tablaResultados").innerHTML="<div align=\"center\" style=\" position: absolute;	top: 200px; left:50%;	height: 30%;	width: 50%;	margin: -15% 0 0 -25%;\" ><img  src=\"img/loading2.gif\" /></div>";
    var vJornadas=null;
    obtenerJornadas(function() {
        if(jornadas!==null) {
            vJornadas="<select id='comboJornadas' onchange=\"cargarResultados(this, '" + temporada + "', '" + competicion + "')\" class='selButton2'>";  
            //vJornadas += "<option value='" + jornadas[0] +"'>CLASIFICACIÓN</option>";  
            for (var i=0; i<jornadas.length; i++) {
                vJornadas += "<option value='" + jornadas[i] +"'> Jornada " + jornadas[i] + "</option>";                  
            }
            selResultados(temporada,jornadas[0],competicion);
           // selClasificacion(temporada,competicion);
        }
        else {
            vJornadas = "¡SIN RESULTADOS!";
            // Añadir mensaje error!!!
        }
        vJornadas +="</select>";  
        
        document.getElementById("comboJornadas").innerHTML=vJornadas;
    },temporada, competicion);       
}



function cargarJornadasClasificacion(element, temporada) {  
    document.getElementById("tablaResultados").innerHTML="<div align=\"center\" style=\" position: absolute;	top: 200px; left:50%;	height: 30%;	width: 50%;	margin: -15% 0 0 -25%;\" ><img  src=\"img/loading2.gif\" /></div>";
    var idx=element.selectedIndex;
    var competicion = element.options[idx].value;    
    var vJornadas=null;
    obtenerJornadasClasificacion(function() {
        if(jornadas!==null) {
            vJornadas="<select id='comboJornadas' onchange=\"cargarClasificacion(this, '" + temporada + "', '" + competicion + "')\" class='selButton2'>";  
            //vJornadas += "<option value='" + jornadas[0] +"'>CLASIFICACIÓN</option>";  
            for (var i=0; i<jornadas.length; i++) {
                vJornadas += "<option value='" + jornadas[i] +"'> Jornada " + jornadas[i] + "</option>";   
            }
            selClasificacion(temporada,jornadas[0],competicion);
            //selClasificacion(temporada,competicion);
        }
        else {
             vJornadas = "¡SIN RESULTADOS!";
            // Añadir mensaje error!!!
        }
        vJornadas +="</select>";  
         
        document.getElementById("comboJornadas").innerHTML=vJornadas;
    },temporada, competicion);       
}


function selJornadasClasificacion(temporada, competicion) {    
    document.getElementById("tablaResultados").innerHTML="<div align=\"center\" style=\" position: absolute;	top: 200px; left:50%;	height: 30%;	width: 50%;	margin: -15% 0 0 -25%;\" ><img  src=\"img/loading2.gif\" /></div>";
    var vJornadas=null;
    obtenerJornadasClasificacion(function() {
        if(jornadas!==null) {
            vJornadas="<select id='comboJornadas' onchange=\"cargarClasificacion(this, '" + temporada + "', '" + competicion + "')\" class='selButton2'>";  
            //vJornadas += "<option value='" + jornadas[0] +"'>CLASIFICACIÓN</option>";  
            for (var i=0; i<jornadas.length; i++) {
                vJornadas += "<option value='" + jornadas[i] +"'> Jornada " + jornadas[i] + "</option>";                  
            }
            selClasificacion(temporada,jornadas[0],competicion);
           // selClasificacion(temporada,competicion);
        }
        else {
            vJornadas = "¡SIN RESULTADOS!";
            // Añadir mensaje error!!!
        }
        vJornadas +="</select>";  
        
        document.getElementById("comboJornadas").innerHTML=vJornadas;
    },temporada, competicion);       
}

function cargarResultados(element,temporada,competicion) {
    document.getElementById("tablaResultados").innerHTML="<div align=\"center\" style=\" position: absolute;	top: 200px; left:50%;	height: 30%;	width: 50%;	margin: -15% 0 0 -25%;\" ><img  src=\"img/loading2.gif\" /></div>";
    var idx=element.selectedIndex;
    var jornada = element.options[idx].value;

    var vResultados=null;
    obtenerResultados(function() {
        if(resultados!==null) {
            vResultados = "<div class='CSSTableGenerator'>";
            vResultados += "<table>";
            vResultados += "<tr>";
            vResultados += "<td>Partido</td>";
            vResultados += "<td>Res.</td>";
            //vResultados += "<td>Estado</td>";
            vResultados += "<td>Fecha</td>";
            vResultados += "<td>Hora</td>";
            vResultados += "<td>Recinto</td>";
            vResultados += "</tr>";
            for(var i=0; i<resultados.length;i++) {
                vResultados += "<tr>";
                vResultados += "<td>" + resultados[i]["equipolocal"] + " - " + resultados[i]["equipovisitante"] +"</td>";
                vResultados += "<td>" + resultados[i]["resultado"] + "</td>";
               // vResultados += "<td>" + resultados[i]["estado"] + "</td>";
                vResultados += "<td>" + resultados[i]["fecha"] + "</td>";
                vResultados += "<td>" + resultados[i]["hora"] + "</td>";
                vResultados += "<td>" + resultados[i]["recinto"] + "</td>";
                vResultados += "</tr>";    
            }
            vResultados += "</tr>";
            
            
            vResultados += "</table>";
            vResultados += "</div>";            
        }
        else {
            vResultados = "¡SIN RESULTADOS!";
            // Añadir mensaje error!!!
        }       
       document.getElementById("tablaResultados").innerHTML=vResultados;
    },temporada,competicion,jornada);       
}


function selResultados(temporada,jornada,competicion) {
    document.getElementById("tablaResultados").innerHTML="<div align=\"center\" style=\" position: absolute;	top: 200px; left:50%;	height: 30%;	width: 50%;	margin: -15% 0 0 -25%;\" ><img  src=\"img/loading2.gif\" /></div>";
    var vResultados=null;
    obtenerResultados(function() {
         if(resultados!==null) {
            vResultados = "<div class='CSSTableGenerator'>";
            vResultados += "<table>";
            vResultados += "<tr>";
            vResultados += "<td>Partido</td>";
            vResultados += "<td>Res.</td>";
            //vResultados += "<td>Estado</td>";
            vResultados += "<td>Fecha</td>";
            vResultados += "<td>Hora</td>";
            vResultados += "<td>Recinto</td>";
            vResultados += "</tr>";
            for(var i=0; i<resultados.length;i++) {
                vResultados += "<tr>";
                vResultados += "<td>" +  resultados[i]["equipolocal"] + " - " + resultados[i]["equipovisitante"] +"</td>";
                vResultados += "<td>" + resultados[i]["resultado"] + "</td>";
               // vResultados += "<td>" + resultados[i]["estado"] + "</td>";
                vResultados += "<td>" + resultados[i]["fecha"] + "</td>";
                vResultados += "<td>" + resultados[i]["hora"] + "</td>";
                vResultados += "<td>" + resultados[i]["recinto"] + "</td>";
                vResultados += "</tr>";    
            }
            vResultados += "</tr>";
            
            
            vResultados += "</table>";
            vResultados += "</div>";            
        }
        else {
             vResultados = "¡SIN RESULTADOS!";
            // Añadir mensaje error!!!
        }
       
       document.getElementById("tablaResultados").innerHTML=vResultados;
    },temporada,competicion,jornada);       
}



function cargarClasificacion(element,temporada,competicion) {
    document.getElementById("tablaResultados").innerHTML="<div align=\"center\" style=\" position: absolute;	top: 200px; left:50%;	height: 30%;	width: 50%;	margin: -15% 0 0 -25%;\" ><img  src=\"img/loading2.gif\" /></div>";
    var idx=element.selectedIndex;
    var jornada = element.options[idx].value;

    var vResultados=null;
    obtenerClasificacion(function() {
        if(resultados!==null) {
            vResultados = "<div class='CSSTableGenerator'>";
            vResultados += "<table>";
            vResultados += "<tr>";
            vResultados += "<td>Pº</td>";
            vResultados += "<td>Equipo</td>";
            //vResultados += "<td>Estado</td>";
            vResultados += "<td>Pt</td>";
            vResultados += "<td>J</td>";
            vResultados += "<td>G</td>";
            vResultados += "<td>E</td>";
            vResultados += "<td>P</td>";
            vResultados += "<td>Sc</td>";
            vResultados += "</tr>";
            for(var i=0; i<resultados.length;i++) {
                vResultados += "<tr>";
                vResultados += "<td>" + resultados[i]["posicion"]  +"</td>";
                vResultados += "<td>" + resultados[i]["equipo"] + "</td>";
                vResultados += "<td>" + resultados[i]["puntos"] + "</td>";
                vResultados += "<td>" + resultados[i]["jugados"] + "</td>";
                vResultados += "<td>" + resultados[i]["ganados"] + "</td>";
                vResultados += "<td>" + resultados[i]["empatados"] + "</td>";
                vResultados += "<td>" + resultados[i]["perdidos"] + "</td>";
                vResultados += "<td>" + resultados[i]["sancion"] + "</td>";
                vResultados += "</tr>";    
            }
            vResultados += "</tr>";
            
            
            vResultados += "</table>";
            vResultados += "</div>";            
        }
        else {
            vResultados = "¡SIN RESULTADOS!";
            // Añadir mensaje error!!!
        }       
       document.getElementById("tablaResultados").innerHTML=vResultados;
    },temporada,competicion, jornada);       
}

function selClasificacion(temporada,jornada, competicion) {
    document.getElementById("tablaResultados").innerHTML="<div align=\"center\" style=\" position: absolute;	top: 200px; left:50%;	height: 30%;	width: 50%;	margin: -15% 0 0 -25%;\" ><img  src=\"img/loading2.gif\" /></div>";
    var vResultados=null;
    obtenerClasificacion(function() {
        if(resultados!==null) {
            vResultados = "<div class='CSSTableGenerator'>";
            vResultados += "<table>";
            vResultados += "<tr>";
            vResultados += "<td>Pº</td>";
            vResultados += "<td>Equipo</td>";
            //vResultados += "<td>Estado</td>";
            vResultados += "<td>Pt</td>";
            vResultados += "<td>J</td>";
            vResultados += "<td>G</td>";
            vResultados += "<td>E</td>";
            vResultados += "<td>P</td>";
            vResultados += "<td>Sc</td>";
            vResultados += "</tr>";
            for(var i=0; i<resultados.length;i++) {
                vResultados += "<tr>";
                vResultados += "<td>" + resultados[i]["posicion"]  +"</td>";
                vResultados += "<td>" + resultados[i]["equipo"] + "</td>";
                vResultados += "<td>" + resultados[i]["puntos"] + "</td>";
                vResultados += "<td>" + resultados[i]["jugados"] + "</td>";
                vResultados += "<td>" + resultados[i]["ganados"] + "</td>";
                vResultados += "<td>" + resultados[i]["empatados"] + "</td>";
                vResultados += "<td>" + resultados[i]["perdidos"] + "</td>";
                vResultados += "<td>" + resultados[i]["sancion"] + "</td>";
                vResultados += "</tr>";    
            }
            vResultados += "</tr>";
            
            
            vResultados += "</table>";
            vResultados += "</div>";            
        }
        else {
             vResultados = "¡SIN RESULTADOS!";
            // Añadir mensaje error!!!
        }
       
       document.getElementById("tablaResultados").innerHTML=vResultados;
    },temporada,competicion,jornada);       
}