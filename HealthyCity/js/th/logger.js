// This will be the REST calling JS api
// npm install jquery

// Need a simple test whether we are running under Node.js, so that we can 
// load code differently when running under phonegap
// If true we are running within Node.js, so 
var inNode = false;
if (typeof window === 'undefined') {
    
    console.log ("Assuming we are running in Node.js test environment");
    inNode = true;
    
} else {
    console.log ("Assuming we are running in a browser/Phonegap application");
    inNode = false;
}


if (inNode) {
    
    // We need to create a DOM outside of jquery, now
    var jsdom = require("jsdom"); 
    var jq = require("jquery")(jsdom.jsdom().createWindow()); 
    var https = require('https');

    var request = require('request'),  
    sys = require('sys');
}

else {
    // Link to the jquery library, should be loaded directly by the
    // toplevel index.html file
    var jq = jQuery;
}
 
// Set up a global conection structure
var Restlogging = { 

    // Defaults
    connection: {server: "http://212.58.34.56:8080",
		 basedir: "/ies/log/app",
		 appname: "Majadahonda Healthy City",
		 sessionid: 0
		},

    init: function (server) {

	// Set up with a fresh (random) sessionid
	var sess = Math.floor(Math.random() * 100000);

	//Grab the app name from the top level document
	//Assume that it lives in the app div in the first h1 block
	try {
	    var apptxt = document.getElementsByClassName('app');
	    var header = apptxt[0].getElementsByTagName("h1");
	    var appname = header[0].innerHTML;
	}
	catch (err) {
	    //Do something if this is not found...
	    appname = "Majadahonda Healthy City";
	}

	console.log("Initialising app '" + appname + " (session id " + sess + "') connecting to server " + server);

	this.connection.appname = appname;
	this.connection.server = server; 
	this.connection.sessionid = sess;
	this.bindEvents();
    },

    // Bind common events
    // Note that we handle switching away from the app as app close
    // and resuming as app restart
    // This should allow us to monitor screen time of the app
    bindEvents: function() {

	document.addEventListener('deviceready', this.onDeviceReady, false);
	document.addEventListener('pause', this.onStop, false);
	document.addEventListener('backbutton', this.onStop, false);
	document.addEventListener('resume', this.onStart, false);
	document.addEventListener('menubutton', this.onStop, false);
    },

    onDeviceReady: function() {
        
	Restlogging.testAppAPI();
        Restlogging.appStart();
    },	

    // Not technically closing the app, but we  need to register when the
    // user switches away from it.
    onStop: function () {
	
	Restlogging.appClose();
    },

    onStart: function () {
	
	Restlogging.appStart();
    },	

    // Note we do not currently use posting for the logging API
    postURL: function (url, postdata) {

	console.log ("Testing post mechanism <" + url + ">");
	// Post and print drop the return result
	
	if (inNode) {

	    request.post(url); //.form({key:'value'})
	}
	
	else {
	    jq.ajax ({
		dataType: "text",
		type: "POST",
		url: url,
		crossDomain: true ,
		data: postdata,   
		cache:false,
                async:false,
                //xhrFields: {
                  //  withCredentials: true
                //},
		success: function(data) {
		    console.log("success: " + data);
		},
		error: function (jqXHR, textStatus, errorThrown) {
		
		    console.log("Mistake: " +  errorThrown);
		}
	    });
	}
    },

    // Get request to the url, checking the returned results against expected
    // We use GET since it is easier to test
    getURL: function (url, expected) {

	if (inNode) {
	    
	    // Use the request library to get the call working in Node
	    request({ uri:url }, function (error, response, body) {  
		
		if (error && response.statusCode !== 200) {
		    console.log('Error logging')
		}
		else {
		    //console.log("This is the body I get <" + body  + ">");
		    
		    if (body==expected) {
			
			console.log("Request:" + url + ":Got expected response");
		    }
		    else {
			
			console.log("Error:" + url + ":Got unexpected response" + body);
		    }
		}
	    });
	}
	else {
	    
	    jq.ajax ({
		dataType: "text",
		type: "GET",
		url: url,
		crossDomain: true ,
		cache:false,
                async:false,
                //xhrFields: {
                  //  withCredentials: true
                //},
		success: function(data) {
		    console.log("success: " + data);
		},
		error: function (jqXHR, textStatus, errorThrown) {
		
		    console.log("Mistake: " +  errorThrown);
		}
	    });
	}
    },


    restPost: function (url) {

	this.postURL (this.connection.server + this.connection.basedir + url);
    },

    restGet: function (url, response) {

	this.getURL (this.connection.server + this.connection.basedir + url, response);
    },

    // Test to see if the api is present where we think if is
    testAppAPI: function () {

	this.restGet("/here", "yes");
    },


    // This does app level logging
    // Note we do not restrict event types here for now
    appLog: function (type, message) {
   
	//Get the current time
	time = (new Date).getTime()/1000 //Keep as seconds, with millsecond resolution
	
	this.restGet ("/stamp/" + time + "/" + this.connection.appname 
		      + "/" + this.connection.sessionid + "/" 
		      + type + "/" + message, "logged");
    },

    appCustom: function (message){

	this.appLog ("AppCustom", message);
    },

    appStart: function () {
	
	console.log("Appstart");
	this.appLog ("Appstart", "Phone event");
    },
    
    appClose: function () {

	console.log("AppClose");
	this.appLog ("Appclose", "Phone event");
    }
};


if (inNode) {

    //Export the interface
    exports.Restlogging = Restlogging;
}






