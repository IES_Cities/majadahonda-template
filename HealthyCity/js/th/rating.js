/*
 * Current version: 21/05/2014
 * Author: TOSHIBA 
 * Contact: stefanos.vatsikas@toshiba-trel.com
 * 
 * This file handles the user rating/feedback functionality of an IES Cities app.
 * It requires the presence of jquery, jquery-impromptu.js and rating.css files.
 * 
 * To integrate in your code, include the relevant js files and then call the 
 * function startRatingSurvey(). Also make sure the questions.js file is present.
 * 
 * The user can choose to rate the app either on Google Play or inside the app itself.
 * If GooglePlay rating is chosen, then the user is redirected to the relevant section
 * on Google Play store. As no IES apps have been published yet, the user is redirected
 * to the GoogleMaps section - just to demonstrate the functionality. When an IES app
 * wants to use this rating functionality, the appropriate url (ie.market://....) should
 * be used.
 * 
 * If the user opts to use the in-app rating/feedback functionality, a popup window is 
 * displayed and used to collect the user's feedback.  Currently, this version supports 
 * questions retrieved from the questions.json file (in JSON format obviously),
 * but future versions will be able to retrieve 
 * the questions from a relevant IES Cities REST service. Also, currently the responses
 * of the users are sent via the REST interface to the rating database on the IES platform.
 * 
 * The popup window is currently triggered by the number of times the app has been launched.
 * This functionality is controlled by the launcuCounter() function and can be easily altered
 * to support a different policy.
 * 
 * */

var htmlQuestions = new Array();
var surveyStates = new Array();
var surveyComplete = false;
var cityName = "SomeCity";
var appName = "SomeApp";
var IESServerRatingURL ="http://150.241.239.65:8080/IESCities/api/rating/response/";
var appGooglePlayURL = "market://details?id=com.google.android.apps.maps";
// Testing url: http://192.168.20.119:8080/IESCities/api/log/rating/response/


/* Load the JSON file that contains the in-app questions */
function loadJSONfile() {
    
  
    jQuery.getJSON("js/th/questions.json", function(data){           
        createHTMLquestions(data);         
    });
  
    
}

/* Parse the JSON question data and create the HTML-formatted questions and responses.
The retrieved object (after the in-app questions have been answered) will capture the
ANSWER.ID (for single choice questions) or the text entered (for freetext questions).
*/
function createHTMLquestions(data){

    var questions = data.questionnaire.questions;

    for (var i=0; i< questions.length; i++){

        var htmlQuestion = '<p>'+questions[i].text+'</p>';
        
        // Create HTML object for questions with free from text responses
        if (questions[i].type == "freetext")
        {
            htmlQuestion = htmlQuestion + '<div class="field"><textarea name="' +questions[i].id+'"></textarea></div>';
        }
        
        // Create HTML object for questions with single choice responses
        else if (questions[i].type == "singlechoice"){
            for (var j=0; j<questions[i].answers.length; j++){
                // check if this response should be the default (i.e. pre-selected/checked)
                if (questions[i].answers[j].default == "yes"){
                    htmlQuestion = htmlQuestion + '<div class="field"><input type="radio" name="'
                        +questions[i].id+'" value="'
                        +questions[i].answers[j].id+'" checked="checked" class="radioinput" /><label>'
                        +questions[i].answers[j].text+'</label></div>';
                }
                else {
                    htmlQuestion = htmlQuestion + '<div class="field"><input type="radio" name="'
                        +questions[i].id+'" value="'
                        +questions[i].answers[j].id+'" class="radioinput" /><label>'
                        +questions[i].answers[j].text+'</label></div>';
                }
            }
        }
        else {
            alert("response type not recognised");
        }
        htmlQuestions[i] = htmlQuestion;
    }
    launchRating(data.questionnaire.city, data.questionnaire.app, data.questionnaire.IESServerRatingURL, data.questionnaire.appGooglePlayURL); 

}

/*
 * Uses a file stored on the device in order to count the number of times the app has
 * been launched. 
 * */
function launchRating(cName, aName, ServerRatingURL, GooglePlayURL) {    
	cityName = cName;;
	appName = aName;
    IESServerRatingURL = ServerRatingURL;
    appGooglePlayURL = GooglePlayURL;
	if (typeof (Storage) !== "undefined") {
		if (localStorage.launchCount) {
			localStorage.launchCount = Number(localStorage.launchCount) + 1;
		} else {
			localStorage.launchCount = 1;
		}
		//alert("The app has now been launched " + localStorage.launchCount + " times...");
		
        
        if (Number(localStorage.launchCount) > 2) {	//enable to make testing easier
        //if ((Number(localStorage.launchCount) == 5) || (Number(localStorage.launchCount) == 20)) {     //disable for testing
            buildSurvey(htmlQuestions);
		}
	} else {
		console.log("Sorry, your device does not support local storage...");
	}
}


/* Sends the user responses back to the IES platform via the REST API */
function sendRating(theURL){
    
	$.ajax({
		async: false, //!!asynchronous ajax call, so that the app is NOT blocked while waiting for the query
		type: "POST",
		data: "",
		url: theURL,
	}).then(function (results) {
		console.log("Successfully saved user feedback with url: "+ theURL);
	}, function(jqxhr,textStatus,errorThrown){
		console.log(jqxhr.status+", "+ textStatus +", " + errorThrown+", "+' Error saving feeback to IES platform');
		console.log("Error url for request was: "+theURL);
		});        	
}  

/* Builds the html form(s) that contain the questions (i.e. states) for the user */
function buildSurvey(htmlQuestions){    
    surveyStates = [
        {   // This is the opening state/form
            name: "start",
            title: appName + " estudio",
            html : '<p>¿Le gustaría calificar esta aplicación? Usted puede hacer eso en Google Play o en la propia aplicación.</p>',
            buttons : {
                No : -1,
                'En Google Play' : 1,
                'En la App' : 0			
            },
            focus : 2,
            submit : function(e, v, m, f) {
                if (v == -1)
                    return true;
                else if (v == 0)
                    $.prompt.nextState();// go  forward
                else if (v == 1)
                    //window.open("market://details?id=com.google.android.apps.maps","_system");
                    window.open(appGooglePlayURL,"_system");
                return false;
            }
        },
        {   // This is the last state/form
            name: "finish",
            title: appName + " estudio - preguntas "+htmlQuestions.length+" de "+htmlQuestions.length,
            html : htmlQuestions[htmlQuestions.length-1], // Populate the final state with the last question from the json file
            buttons : {
                Atrás : -1,
                Cancelar : 0,
                Terminar : 1
            },
            focus : 2,
            submit : function(e, v, m, f) {
                if (v == 0)
                    $.prompt.close()
                else if (v == 1) {
                    surveyComplete = true;
                    ocultarRanting();
                    return true; // we're done
                }
                else if (v = -1)
                    $.prompt.prevState();// go back
                return false;
            }
        }

    ];
    
    // This builds the rest of the forms between the first and the last
    for (var i=0;i<htmlQuestions.length-1;i++){
        var tempState = 
        {
            name: "start"+i,
            title: appName + " estudio - pregunta "+(i+1)+" de "+htmlQuestions.length,
            html : htmlQuestions[i],
            buttons : {
				Cancelar : -1,
				Atrás : 0,
				Siguiente : 1		
            },
            focus : 2,
            submit : surveySubmitFunc
        }
        surveyStates.splice(i+1,0, tempState);  // add the new states/forms to the existing ones
    }
    
    if (htmlQuestions.length>1) // only start the survey of more than 1 questions are going to be asked.
        startSurvey();
    else
        console.log("Not enough questions supplied for the survey");
}

/* Handles the button functionality on some of the states/forms */
var surveySubmitFunc = function(e,v,m,f){
    if (v == -1)
        return true;
    else if (v == 0)
        $.prompt.prevState();// go  back
    else if (v == 1)
        $.prompt.nextState();// go  forward
    return false;
};

/* Fires up the survey (overlay html form over app/website) */
function startSurvey(){

    //var IESServerRatingURL = "http://150.241.239.65:8080/IESCities/api/rating/stamp/";
	$.prompt(surveyStates, {
        
		close : function(e, v, m, f) { // Handles the closing of the overlay window (i.e. the in-app questionnaire).
			var questionID = 0;
            var answerID = 0;
            var responses = new Array();
            var cnt = 0;
            
            // Iterate through the user responses.
			$.each(f, function(i, obj) {
                var freetext = "No free text response required";
                
                // Check answerID (i.e. obj): if number, then it's indeed an answerID.
                // If a string, it's a free text response.
                // If empty, then the user didn't respond to this particular question.
                if (obj != "" && !isNaN(obj))
                {
                    answerID = parseInt(obj);
                }
                else {
                    answerID = 1;
                    if (obj===""){
                        freetext = "No response given";
                    }
                    else {
                        freetext = obj;
                    }
                }
				questionID++;
                
                // Form the url for submitting each response to the IES Platform
                responses[cnt] = IESServerRatingURL+"Majadahonda Healthy City/"+questionID+"/"+answerID+"/"+freetext;
                cnt++;
			});

			if (surveyComplete) { // only submit responses if survey has been completed
				for (var j=0; j< responses.length; j++) {
                    //console.log(responses[j]);
                    sendRating(responses[j]);  //Send response to IES Platform
                }
				Restlogging.AppQuestionnaire(); //Log the relevant event (works only if the Logging component of IES is present in the app/website)
			} else
				console.log("Survey incomplete");

		}
	});
}

function startRatingSurvey(){
    loadJSONfile();
}
