// Declaración de variables globales
var cuerpo, menuprincipal, wrapper, estado;// myScrollMenu, menuperfil;

// Guardamos en variables elementos para poder rescatarlos después sin tener que volver a buscarlos
cuerpo = document.getElementById("cuerpo"),
menuprincipal = document.getElementById("menuprincipal"),
//menuperfil = document.getElementById("menuperfil"),
wrapper = document.getElementById("wrapper");

var xhReq = new XMLHttpRequest();

var lang;
var vRanting = (window.localStorage.getItem("vRanting")!==null)?window.localStorage.getItem("vRanting"):"true";
var day = null;
var weather = null;
var temperature = null;
var thermal = null;
var rh = null;
var uv = null;
var temperatureMaxMin = null;
var viento = null;
var humedad = null;
var coordenadasUser = null;
var infUser = null;
var infoSensorAyun = null;
var infoSensorMonte = null;
var map=null;
var icoAlert = "alertaVerde";
var menAlert =  " Sin riesgo. No hay avisos meteorológicos.";
var indMaxMin = 0;
var indn = 0;
var indW = 0;
var ind = 0;
var imageweather = "";
var temperatureNow = "";
var numId=222;
var numIdCom =222;
var numSessionLog=1;

var botStatus = {
  "botStatus1" : 0,
  "botStatus2" : 0,
  "botStatus3" : 0,
  "botStatus4" : 0,
  "botStatus5" : 0,
  "botStatus6" : 0
};
var iconoTiempo = ["despejado.png","despejado.png", "intervalosNubosos.png", "cubierto.png", "nubosoLluviaescasa.png", "muynubosoLluvia.png", "nieve.png"];

var preference = {	
  "box_1" : (window.localStorage.getItem("box_1")!==null)?window.localStorage.getItem("box_1"):1,
  "box_2" : (window.localStorage.getItem("box_2")!==null)?window.localStorage.getItem("box_1"):1,
  "box_3" : (window.localStorage.getItem("box_3")!==null)?window.localStorage.getItem("box_1"):0,
  "box_4" : (window.localStorage.getItem("box_4")!==null)?window.localStorage.getItem("box_1"):0,
  "box_5" : (window.localStorage.getItem("box_5")!==null)?window.localStorage.getItem("box_1"):0,
  "box_6" : (window.localStorage.getItem("box_6")!==null)?window.localStorage.getItem("box_1"):0,
  "box_7" : (window.localStorage.getItem("box_7")!==null)?window.localStorage.getItem("box_1"):1
};

var app = {
	
    // Constructor de la app
    initialize: function() {

    estado="cuerpo";
    numSessionLog = Math.floor((Math.random() *  2147483647) + 1);
    
    // Creamos el elemento style, lo añadimos al html y creamos la clase cssClass para aplicarsela al contenedor wrapper
    var heightCuerpo=window.innerHeight-46;
    var style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = '.cssClass { position:absolute; z-index:2; left:0; top:46px; width:100%; height: '+heightCuerpo+'px; overflow:auto;}';
    document.getElementsByTagName('head')[0].appendChild(style);
        
    // Añadimos las clases necesarias
    cuerpo.className = 'page center';
    menuprincipal.className = 'page center';
    wrapper.className = 'cssClass';
      
    // Leemos por ajax el archivos menu.html de la carpeta opciones
    xhReq.open("GET", "opciones/preferencias.html", false);
    xhReq.send(null);
    document.getElementById("contenidoMenu").innerHTML=xhReq.responseText; 
    
    preference[0] = window.localStorage.getItem("box_1");
    preference[1] = window.localStorage.getItem("box_2");
    preference[2] = window.localStorage.getItem("box_3");
    preference[3] = window.localStorage.getItem("box_4");
    preference[4] = window.localStorage.getItem("box_5");
    preference[5] = window.localStorage.getItem("box_6");
    preference[6] = window.localStorage.getItem("box_7");
    
    var start = new Date().getTime();
    setLog("AppStart", start);
    
    SocialSharing.install();

    pressButton("btHome");  
    updateButtons();   
   
    
   getId(function() {
        getInfoSensor(function() {
             var contetBody = getContentStations();
      document.getElementById("contenidoCuerpo").innerHTML=contetBody;
        });    
        getIdCom(function() {           
      	   getInfoUser(); });
    });

    getAemetData(function() {
     
    });

    getGps(); 
       
    }
};


// Función para añadir clases css a elementos
function addClass( classname, element ) {
    var cn = element.className;
    if( cn.indexOf( classname ) !== -1 ) {
      return;
    }
    if(cn !== '') {
      classname = ' '+classname;
    }
    element.className = cn+classname;
}

function selectpreference(sel) {
  var id = $(sel).attr("id");
  if(preference[id]===0) preference[id] = 1;
  else preference[id] = 0;
  window.localStorage.setItem(id, preference[id]);
  //initPreferences();
  var contentBody = getContentStations();
  document.getElementById("contenidoCuerpo").innerHTML=contentBody;
}

function updatepreferences(id) {  
    if(preference[id]===1)
       $("#"+id).prop("checked", true);
    else
       $("#"+id).prop("checked", false);    
    window.localStorage.setItem(id, preference[id]); 
}


function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}


// Función para eliminar clases css a elementos
function removeClass( classname, element ) {
    var cn = element.className;
    var rxp = new RegExp( "\\s?\\b"+classname+"\\b", "g" );
    cn = cn.replace( rxp, '' );
    element.className = cn;
}

function menu(opcion) {
  $("#menu").css("opacity", "1");
  $("#header").css("opacity", "1");
  // Si pulsamos en el botón de "menu" entramos en el if
  switch(opcion){
    case "menu":
        pressButton("btHome");
        releaseButton("btMap");
        releaseButton("btPrediccion");        
        var contentBody = getContentStations();
        document.getElementById("contenidoCuerpo").innerHTML=contentBody;
        cuerpo.className = 'page transition center';
        estado="cuerpo";        
        break;        
    case "update":        
        document.getElementById("update").src ="img/update2.gif";
        getId(function() {
        getInfoSensor(function() {
             var contetBody = getContentStations();
           document.getElementById("contenidoCuerpo").innerHTML=contetBody;
           document.getElementById("update").src ="img/update.png";           
        });         
      });      
      break;
    case "prediccion":
      pressButton("btPrediccion");
      releaseButton("btMap");
      releaseButton("btHome");        
      var contentPrediction = getContentPrediction();
      document.getElementById("contenidoCuerpo").innerHTML=contentPrediction;  
      cuerpo.className = 'page transition center';
      estado="cuerpo";        
      setLog("AppConsume", "Weather query");
      break;
    case "mapa":
      var vMap = "<div id=\"map\"></div>";
      document.getElementById("contenidoCuerpo").innerHTML=vMap;
      pressButton("btMap");
      releaseButton("btPrediccion");
      releaseButton("btHome");  
      iniacializaMap();
      break;  
    case "sensor1":
      var vMap = "<div id=\"map\"></div>";
      document.getElementById("contenidoCuerpo").innerHTML=vMap;
      pressButton("btMap");
      releaseButton("btPrediccion");
      releaseButton("btHome");  
      iniacializaMap();
      map.panTo( new google.maps.LatLng(40.463432,-3.847241));
      map.setZoom(16);
      break; 
    case "sensor2":
      var vMap = "<div id=\"map\"></div>";
      document.getElementById("contenidoCuerpo").innerHTML=vMap;
      pressButton("btMap");
      releaseButton("btPrediccion");
      releaseButton("btHome");        
      iniacializaMap();
      map.panTo( new google.maps.LatLng(40.47283,-3.872248));
      map.setZoom(16);
      break; 
    case "infoUser":       
        sleep(1000);
       getGps();
       xhReq.open("GET", "opciones/preditionUser.html", false);
       xhReq.send(null);
       $("#menu").css("opacity", "0.37");
       $("#header").css("opacity", "0.37");
       document.getElementById("contenidoCuerpo").innerHTML=xhReq.responseText;     
       if((vRanting!=="true") || (vRantging==="true"))
       {
         $('#ranting').hide();
       }
       break;  
    case "preferencias":
      if(estado!=="preferencias"){
        $("#menu").css("opacity", "0.37");
        $("#header").css("opacity", "0.37");
        cuerpo.className = 'page transition right';
        estado="preferencias";
      }
      else{
        cuerpo.className = 'page transition center';
        estado="cuerpo";
      }
      break;
    case "compartir":
      sleep(1000);
      var tiempo = "La temperatura en Majadahonda es de :"  + infoSensorAyun["temperatura"] + "°";
       var imgWether = null;
       
       if(imageweather!==null)
           imgWether = "http://150.241.239.51/public/IESCities/Apps/img/" + imageweather;       
      // window.plugins.socialsharing.share(tiempo, null, null,null);

      window.plugins.socialsharing.share(tiempo, null, imgWether, "http://www.majadahonda.org/");      
      
      // Restlogging.appCustom("The user to share the weather");
      break;
    case "enviarDatosUser":
       var estadoTiem = 1;
       for(var n = 1; n <= 6; n++) {
  	       var ib = "botStatus"+ n;
  	       if(botStatus[ib] === 1)
  	          estadoTiem = n;
  		}  
  	  var d = new Date();
  	  var mes = d.getMonth() + 1;          
      var query = "INSERT INTO PREDICCION_USUARIO (ID_USER, ID_EST_TIE, GRADOS, COORDENADAS, DATE, TIME) VALUES (1, " + estadoTiem + ", \"" +  document.getElementById("selGrados").value + "\", \"" + coordenadasUser + "\",\""  + d.getFullYear() + "-" + mes + "-" + d.getDate() + "\",\"" + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds() + "\")";
                      
      setTimeout(getIdCom(function() {setEstadoTiempo(query);}),0);
      setTimeout(menu("menu"),0);
 
      Restlogging.appCustom("The user has sent a statement of the time since the coordinates: " +coordenadasUser);
      break;
    default:
        // Añadimos la clase al li presionado
        addClass('li-menu-activo' , document.getElementById("ulMenu").getElementsByTagName("li")[opcion]);
        // Quitamos la clase añadida al li que hemos presionado
        setTimeout(function() {
          removeClass('li-menu-activo' , document.getElementById("ulMenu").getElementsByTagName("li")[opcion]);
        }, 300);
  }
} 


function ocultarRanting() {
     $('#ranting').hide();     
     window.localStorage.setItem("vRanting", "false");     
}

function getGps() {
	navigator.geolocation.getCurrentPosition(getGpsonSuccess,getGpsonError); 

} 
function getGpsonSuccess(position) {
	coordenadasUser = position.coords.latitude + "," + position.coords.longitude;	
}
 
function getGpsonError(error) { alert('code: ' +error.code+ '\n' + 'message: ' +error.message+ '\n'); }

function getContentBody() {	
  var txtContenido="";	
  var d = new Date();
  var day = d.getDate();
  var hora = d.getHours();
  var fecha = weather[0]["dia"].split("-");
  
  indW = 0;

  for(var a=0; a<weather.length; a++ ){  	
    var dw = new Date(weather[a]["dia"]);
    if(dw.getDate() === day) {  	
      if((hora >= 0) && (hora <6) && (weather[a]["periodo"]==="00-06"))
        indW = a;
      if((hora >= 6) && (hora <12) && (weather[a]["periodo"]==="06-12"))
        indW = a;
      if((hora >= 12) && (hora <18) && (weather[a]["periodo"]==="12-18"))
        indW = a;
      if((hora >= 18) && (hora <24) && (weather[a]["periodo"]==="18-24"))
       indW = a; 
   }
  }

  ind = 0;
  for (var i=0; i < temperature.length; i++) { 
      var dw = new Date(temperature[i]["dia"]);
      if(dw.getDate() === day) {      
      	  indMaxMin = i;
      	  if(hora < 9)
  	         ind = i;  
           if((hora >= 9) && (hora <14))
  	          ind = i + 1;  
          if((hora >= 14) && (hora <20)) 
  	          ind = i + 2; 
          if(((hora >= 20) && (hora <22)) || ((hora >= 0) && (hora < 3)))
  	          ind = i + 3;      	 
  	      break;
      }
  }
  indMaxMin = 0;
  for (var i=0; i < temperatureMaxMin.length; i++) { 
    var dw = new Date(temperatureMaxMin[i]["dia"]);
    if(dw.getDate() === day) {      
      indMaxMin = i;      	 
    }
  }
  
  imageweather = getIcono(weather[indW]["descripcion"]);

  var imagUV = "uv0.png";
  imagUV = "uv" + uv[0]["uv"] + ".png";
      
  txtContenido += "<div id=\"contenido\">";
  txtContenido += "<div id=\"cabecera\">";  
  txtContenido += "<span id=\"title\" class=\"nomCiudad\">MAJADAHONDA</span><span id=\"title\" class=\"fecha\" >" + MostrarFecha(); + "<span>";  
  txtContenido += "</div>";
  txtContenido += "<div id=\"main\">";
  txtContenido += "<div id=\"column_left\">";
  txtContenido += "<img style=\"color:#233245; \" src=\"img/" + imageweather + "\" height=\"120\" width=\"120\" />";
  txtContenido += "</div>";
  txtContenido += "<div id=\"column_right\">";
  if(temperature[ind]["temperatura"]!==null) {
    txtContenido += "<span style=\"font-size:62px; color:#233245;\">" + temperature[ind]["temperatura"] + "&ordm;</span>";	
    temperatureNow = temperature[ind]["temperatura"];
  }
  if(temperatureMaxMin[0]["minima"]!==null)  {
    txtContenido += "<h2><span style='color:#808080'>Min. " + temperatureMaxMin[indMaxMin]["minima"] +  "°</span> <span style='color:#233245'>  Max." + temperatureMaxMin[indMaxMin]["maxima"] + "°</span></h2></p>";	
  }
  txtContenido += "</div>";
  txtContenido += "<br style=\"clear:both;\">";
  txtContenido += "</div>";
  txtContenido += "<div id=\"footer\">";
  txtContenido += "<h3><span style='color:#808080'>Humedad: </span> <span style='color:#233245'>" +  humedad[ind]["humedad"] + "%</span></h3>";
  txtContenido += "<h3><span style='color:#808080'>Presión: </span> <span style='color:#233245'>1050 mbar</span></h3>";
  txtContenido += "<div id=\"tabla\" >"; 
  if((viento[ind]["velocidad"]!== null) && (viento[indW]["direccion"] !== null) && (viento[indW]["direccion"]!=='')) { // && (viento[indW]["direccion"]!=='C')) {		
    txtContenido += "<div  class=\"fila\"><div  class=\"column_1\"><img  src=\"img/" + viento[indW]["direccion"] + ".png\" height=\"32\" width=\"32\" /></div><div style='color:#808080' class=\"column_2\"><h3>viento </h3></div><div style='color:#233245'  class='column_3'><h3>" +  viento[indW]["velocidad"] + " km/h</h3></div></div>";
    txtContenido += "<hr>";
   }
   if(uv[0]["uv"]!==null)     
     txtContenido += "<div  class=\"fila\"><img src=\"img/" + imagUV + "\" height=\"34\" width=\"300\" onclick=\"onWebIndiceUV()\" /></div>";
   txtContenido += "</div>";    
   txtContenido += "</div>";
   txtContenido += "<img src=\"img/alertaVerde.png\" style='position:absolute; left:0; bottom:30px; height: 40px; alignment-adjust:left;' /><div>"+menAlert+"</div>";
   txtContenido += "</div>";

  return txtContenido;

}



function getContentStations() {	
  var txtContenido="";	

  var imagUV = "uv0.png";
  if(infoSensorMonte["radiacion"] !== '')
   imagUV = "uv" + infoSensorMonte["radiacion"] + ".png";
      
  txtContenido += "<div class=\"contenidoEstaciones\">";
  txtContenido += "<div class=\"monte\">";
  txtContenido += "<div class=\"monteCabecera\">";  
  txtContenido += "<span class=\"nomCiudad\">ESTACIÓN MONTE PILAR</span>";//<span  class=\"fecha\" >" + infoSensor[0]["fechahora"].substring(0,infoSensor[0]["fechahora"].indexOf(" ")) + "<span>";  
  txtContenido += "</div>";
  txtContenido += "<div styletext-align: center;' id=\"tabla\" >";
  txtContenido += "<div class=\"monteCuerpo\">";
  txtContenido += "<div class=\"monteCuerpoIzq\">";
  txtContenido += "<span style=\"font-size:48px; color:#233245;\">" +  infoSensorMonte["temperatura"] + "&ordm;c</span>";	
  txtContenido += "<h3> Humedad:<span style=\"font-size:16px; color:#233245;\">" +  infoSensorMonte["humedad"] + "%</span></h3>";    
  txtContenido += "</div>";
  txtContenido += "<div class=\"monteCuerpoDcha\">";
  txtContenido += "<div class=\"fechaCuerpo\">";
 txtContenido += "<span>" + infoSensorMonte["fecha"] + "<span><br>";
  txtContenido += "<span>" + infoSensorMonte["hora"] + "<span>"; 
  txtContenido += "</div>";
  txtContenido += "<div class=\"iconCuerpo\">";  
  txtContenido += "<a href=\"javascript:menu('update');\"><img class=\"update\" id=\"update\" src=\"img/update.png\" height=\"42\" width=\"42\"></a>";
  txtContenido += "</div>";
  txtContenido += "<div class=\"iconLocal\">";  
  txtContenido += "<a href=\"javascript:menu('sensor1');\"><img class=\"ir_mapa\" src=\"img/sensorMapaR.png\" align=\"middle\" height=\"36\" width=\"44\"></a>";
  txtContenido += "</div>";  
  txtContenido += "</div>";  
  txtContenido += "</div>";
  txtContenido += "<div class=\"montePie\">";
  if(infoSensorMonte["radiacion"]!==null)     
     txtContenido += "<div  class=\"fila\"><img src=\"img/" + infoSensorMonte["radiacion"] + "\" height=\"34\" width=\"300\" onclick=\"onWebIndiceUV()\" /></div>";
  txtContenido += "</div>";
  txtContenido += "<div id=\"footer\">";    
  txtContenido += "<span class=\"nomCiudad\">ESTACIÓN AYUNTAMIENTO</span>"; //<span  class=\"fecha\" >" + infoSensor[0]["fechahora"].substring(0,infoSensor[0]["fechahora"].indexOf(" ")) + "<span>";  
    txtContenido += "<div styletext-align: center;' id=\"tabla\" >";
  txtContenido += "<div class=\"monteCuerpo\">";
  txtContenido += "<div class=\"monteCuerpoIzq\">";
  txtContenido += "<span style=\"font-size:48px; color:#233245;\">" +  infoSensorAyun["temperatura"] + "&ordm;c</span>";	
  txtContenido += "<h3> Humedad:<span style=\"font-size:16px; color:#233245;\">" +  infoSensorAyun["humedad"] + "%</span></h3>";  
  txtContenido += "<h3> Presión:<span style=\"font-size:16px; color:#233245;\">" +  infoSensorAyun["presionatmosferica"] + " hPa</span></h3>"; 
  txtContenido += "</div>";
  txtContenido += "<div class=\"monteCuerpoDcha\">";
  txtContenido += "<div class=\"fechaCuerpo\">";
 txtContenido += "<span>" + infoSensorAyun["fecha"] + "<span><br>";
  txtContenido += "<span>" + infoSensorAyun["hora"] + "<span>"; 
  txtContenido += "</div>";
  txtContenido += "<div class=\"iconCuerpo\">";  
//  txtContenido += "<a href=\"javascript:menu('sensor1');\"><img class=\"update\" src=\"img/update.png\" height=\"42\" width=\"42\"></a>";
   txtContenido += "<a href=\"javascript:menu('sensor2');\"><img class=\"ir_mapa\" src=\"img/sensorMapaR.png\" align=\"middle\" height=\"36\" width=\"44\"></a>";
  txtContenido += "</div>";
  txtContenido += "<div class=\"iconLocal\">";  
    if((infoSensorAyun["anemometro"]!== null) && (infoSensorAyun["direcciondelviento"] !== null) && (infoSensorAyun["direcciondelviento"]!=='')) { // && (viento[indW]["direccion"]!=='C')) {		
    txtContenido += "<h3><img style='vertical-align:middle' src=\"img/" + infoSensorAyun["direcciondelviento"] + ".png\" height=\"42\" width=\"42\" />viento " +  infoSensorAyun["anemometro"] + " km/h</h3>";
   }
  txtContenido += "</div>";  
  txtContenido += "<h3>Pluviometro:<span style=\"font-size:16px; color:#233245;\">" +   infoSensorAyun["pluviometro"] + "mm/m.</span></h3>";
 
  txtContenido += "</div>"; 
 
  txtContenido += "</div>";
  txtContenido += "</div>";


  txtContenido += "<img src=\"img/"+icoAlert+".png\" style='position:absolute; left:0; bottom:30px; height: 40px; alignment-adjust:left;' /><div id='alerts'><marquee>" +menAlert+" </marquee></div>";  
 
 
  return txtContenido;
}

function getContentPrediction() {
	
  var txtContenido="";	
  var d = new Date();
  var day = d.getDate();
  var hora = d.getHours();
  var fecha = weather[0]["dia"].split("-");
  
  indW = 0;

  for(var a=0; a<weather.length; a++ ){  	
  	var dw = new Date(weather[a]["dia"]);
  	if(dw.getDate() === day) {  	
  	  if((hora >= 0) && (hora <6) && (weather[a]["periodo"]==="00-06"))
  	    indW = a;
      if((hora >= 6) && (hora <12) && (weather[a]["periodo"]==="06-12"))
        indW = a;
  	  if((hora >= 12) && (hora <18) && (weather[a]["periodo"]==="12-18"))
  	    indW = a;
  	  if((hora >= 18) && (hora <24) && (weather[a]["periodo"]==="18-24"))
  	    indW = a; 
  	  }
  }

  ind = 0;
  for (var i=0; i < temperature.length; i++) { 
    var dw = new Date(temperature[i]["dia"]);
    if(dw.getDate() === day) {      
      indMaxMin = i;
      if(hora < 9)
  	    ind = i;  
      if((hora >= 9) && (hora <14))
  	    ind = i + 1;  
      if((hora >= 14) && (hora <20)) 
  	    ind = i + 2; 
      if(((hora >= 20) && (hora <22)) || ((hora >= 0) && (hora < 3)))
  	    ind = i + 3;      	 
  	    break;
    }
  }
  indMaxMin = 0;
  for (var i=0; i < temperatureMaxMin.length; i++) { 
    var dw = new Date(temperatureMaxMin[i]["dia"]);
    if(dw.getDate() === day) {      
      indMaxMin = i;      	 
    }
  }
   
  imageweather = getIcono(weather[indW]["descripcion"]);
    
  txtContenido += "<div id=\"contenido\">";
  txtContenido += "<div id=\"cabecera\">";  
  txtContenido += "<span id=\"title\" style=\"float:left\">MAJADAHONDA</span><span id=\"title\" style=\"float:right\">" + MostrarFecha(); + "<span>";  
  txtContenido += "</div>";
  txtContenido += "<div id=\"main2\">";
  txtContenido += "<div id=\"column_left\">";
  txtContenido += "<img style=\"color:#233245; \" src=\"img/" + imageweather + "\"  height=\"120\" width=\"120\" />";
  txtContenido += "</div>";
  txtContenido += "<div id=\"column_right\">";
  if(temperature[ind]["temperatura"]!==null) {
    txtContenido += "<span style=\"font-size:62px; color:#233245;\">" + temperature[ind]["temperatura"] + "&ordm;</span>";	
  }
  if(temperatureMaxMin[indMaxMin]["minima"]!==null) {
    txtContenido += "<h2><span style='color:#808080'>Min. " + temperatureMaxMin[indMaxMin]["minima"] +  "°</span> <span style='color:#233245'>  Max." + temperatureMaxMin[indMaxMin]["maxima"] + "°</span></h2></p>";	
  }
  txtContenido += "</div>";
  txtContenido += "<br style=\"clear:both;\">";
  txtContenido += "</div>";
  txtContenido += "<div id=\"footer2\">";    
  txtContenido += "<div styletext-align: center;' id=\"tabla\" >";

  var ud = new Date(weather[0]["dia"]).getDay();
  var ind = 1;
      
  for (var i=0; i < weather.length; i++) {     	
    var nombres_dias = new Array("Dom", "Lun", "Mar", "Mir", "Jue", "Vie", "Sab");
    var d = new Date(weather[i]["dia"]);
    var dn = new Date();        
    if((dn.getDay() !== d.getDay()) && (d.getDay() !== ud))  {
      txtContenido += "<div  class=\"day_" + ind + "\"><h3>" + nombres_dias[d.getDay()] + "</h3><br><img  width='48' height='48' src=\"img/" + getIcono(weather[i]["descripcion"]) + "\" />";
      if((temperatureMaxMin[ind]["minima"] !== null) && (temperatureMaxMin[ind]["maxima"] !== null)) 
        txtContenido += "<br>" +  temperatureMaxMin[ind]["minima"] + "°/" + temperatureMaxMin[ind]["maxima"] + "°";
        txtContenido += "</div>";
        ud=d.getDay();
        ind = ind + 1;
        if(ind > 5)
          break;
       }
     }; 
     
     txtContenido += "</div>";  
     txtContenido += "<span style=\"font-size:16px; margin:5px; color:#6E6E6E;\">Información elaborada utilizando, entre otras, la obtenida de la Agencia Estatal de Meteorología. Ministerio de Agricultura, Alimentación y Medio Ambiente</span>";
     txtContenido += "</div>";
     //txtContenido += "<img src=\"img/"+icoAlert+".png\" style='position:absolute; left:0; bottom:30px; height: 40px; alignment-adjust:left;' /><div style='position:absolute; left:80%; bottom:10px; height: 40px; width:90%; alignment-adjust:right;'><marquee>" +menAlert+" </marquee><div>";
//     txtContenido += "<img src=\"img/"+icoAlert+".png\" style='position:absolute; left:0; bottom:30px; height: 40px; alignment-adjust:left;' /><div id='alerts'><marquee>" +menAlert+" </marquee></div>";
     txtContenido += "<img src=\"img/"+icoAlert+".png\" style='position:absolute; left:0; bottom:30px; height: 40px; alignment-adjust:left;' /><div id='alerts'><marquee>" +menAlert+" </marquee></div>";
     //txtContenido += "<div style='position:absolute; bottom:30px;  height: 100%; width:90%;'><marquee><img src=\"img/"+icoAlert+".png\" style='height: 40px; ' />" +menAlert+" </marquee></div>";
     txtContenido += "</div>";     

  return txtContenido;

}


function getTemperature() {	
  ind = 0;
  for (var i=0; i < temperature.length; i++) { 
    var dw = new Date(temperature[i]["dia"]);
    if(dw.getDate() === day) {      
      indMaxMin = i;
      if(hora < 9)
    	   ind = i;  
      if((hora >= 9) && (hora <14))
    	   ind = i + 1;  
      if((hora >= 14) && (hora <20)) 
    	   ind = i + 2; 
      if(((hora >= 20) && (hora <22)) || ((hora >= 0) && (hora < 3)))
    	   ind = i + 3;      	 
    	   break;
    }
  }  
  return(temperature[ind]["temperatura"]); 
}


function getIcono(wetherValue) {
   var d = new Date();
  var hora = d.getHours();

    var imageweather = "pocoNuboso.png";   
  if(wetherValue.indexOf("Despejado")>=0)  
       imageweather="despejado.png";
  if((wetherValue.indexOf("Despejado")>=0) && ((hora < 8) || (hora>20)))
       imageweather="despejado.png";
  if(wetherValue.indexOf("Intervalos nubosos")>=0)  
       imageweather="intervalosNubosos.png";
  if(wetherValue.indexOf("Cubierto")>=0)  
       imageweather="cubierto.png";
  if(wetherValue.indexOf("Nuboso")>=0)
      imageweather="cubierto.png";  
  if(wetherValue.indexOf("Poco nuboso")>=0)
       imageweather="pocoNuboso.png";
  if((wetherValue.indexOf("Poco nuboso")>=0) && ((hora < 8) || (hora>20)))
       imageweather="pocoNubosoNight.png";   
  if(wetherValue.indexOf("Muy nuboso")>=0)  
       imageweather="muyNuboso.png";
  if(wetherValue.indexOf("Intervalos nubosos con lluvia escasa")>=0)  
       imageweather="nubosoLluviaescasa.png";
  if(wetherValue.indexOf("Cubierto con lluvia")>=0)  
       imageweather="cubiertoConLluvia.png";
  if(wetherValue.indexOf("Intervalos nubosos con lluvia")>=0)  
       imageweather="nubosoLluviaescasa.png";
  if(wetherValue.indexOf("Muy nuboso con lluvia escasa")>=0)  
       imageweather="muynubosoLluviaescasa.png";                                         
  if(wetherValue.indexOf("Muy nuboso con lluvia")>=0)  
       imageweather="muynubosoLluvia.png";
  if(wetherValue.indexOf("Intervalos nubosos con nieve")>=0)  
       imageweather="nuvosoNieve.png";
  if(wetherValue.indexOf("Nuboso con nieve")>=0)  
       imageweather="nieve.png";
  if(wetherValue.indexOf("lluvia")>=0)
    imageweather="cubiertoConLluvia.png";
  if(wetherValue.indexOf("nieve")>=0)
    imageweather="nubosoNieve.png";
   return imageweather;      
}



function MostrarFecha()
{
  var nombres_dias = new Array("Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado");
  var nombres_meses = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
  var fecha_actual = new Date();
  
  dia_mes = fecha_actual.getDate(); //dia del mes
  dia_semana = fecha_actual.getDay(); //dia de la semana
  mes = fecha_actual.getMonth() + 1;
  anio = fecha_actual.getFullYear();
  
  return(nombres_dias[dia_semana] + ", " + dia_mes + " de " + nombres_meses[mes - 1] + " de " + anio);
}


function iniacializaMap() {
	
  //$(document).ready(function(){
  	
    map = new GMaps({
      el: '#map',
      lat: 40.463383,
      lng: -3.863017,
      zoom: 13,
      zoomControl : true,
      zoomControlOpt: {
        style : 'SMALL', position: 'TOP_LEFT'
      },
      panControl : false
     });
     
     map.addMarker({       	
         lat: 40.463432,
         lng: -3.847241,
         icon: "img/sensorMapaR.png",
         title: "Estación el Monte del Pilar",
         infoWindow: {     
             content: "<span style=\"font-size:48px; color:#233245;\">" +  infoSensorMonte["temperatura"] + "&ordm;c</span><span>" + infoSensorMonte["hora"] + "<span><h3> Humedad:<span style=\"font-size:16px; color:#233245;\">" +  infoSensorMonte["humedad"] + "%</span></h3><div  class=\"fila\"><img src=\"img/" + infoSensorMonte["radiacion"] + "\" height=\"34\" width=\"300\" onclick=\"onWebIndiceUV()\" /></div>"
         }
         
       });
       
     map.addMarker({       	
         lat:  40.47283,
         lng: -3.872248,
         icon: "img/sensorMapaR.png",
         title: "Estación Ayuntamiento",
         infoWindow: {     
             content: "<span style=\"font-size:48px; color:#233245;\">" +  infoSensorAyun["temperatura"] + "&ordm;c</span><span>" + infoSensorAyun["hora"] + "<span><h3> Humedad:<span style=\"font-size:16px; color:#233245;\">" +  infoSensorAyun["humedad"] + "%</span><h3> Presión:<span style=\"font-size:16px; color:#233245;\">" +  infoSensorAyun["presionatmosferica"] + "kPa</span></h3><h3><img style='vertical-align:middle'  src=\"img/" + infoSensorAyun["direcciondelviento"] + ".png\" height=\"32\" width=\"32\" />viento " +  infoSensorAyun["anemometro"] + " km/h</h3>"
         }
         
       });
       
     
     for (var i=0; i < infUser.length; i++) {     	
        infUser[i]["coordenadas"] = infUser[i]["coordenadas"].replace(" ","");
        infUser[i]["coordenadas"] = infUser[i]["coordenadas"].replace(")","");
        infUser[i]["coordenadas"] = infUser[i]["coordenadas"].replace("(","");
//       if((dateEvent==='null') || comprobarFecha(events[i]["start_date"],events[i]["end_date"],dateEvent)){                
       map.addMarker({       	
         lat:  infUser[i]["coordenadas"].substring(0, infUser[i]["coordenadas"].lastIndexOf(",")),
         lng: infUser[i]["coordenadas"].substring(infUser[i]["coordenadas"].lastIndexOf(",")+1, infUser[i]["coordenadas"].length),
         icon: "img/usuarioMapa.png",
         title: "Información usuario",
         infoWindow: {     
             content: "<h3>En Majadahonda ahora hace : "+  infoSensorAyun["temperatura"]  + "°</h3><HR width=50% align=\"center\"><h3>Grados: " + infUser[i]["grados"] + "<br>Fecha: " +   infUser[i]["date"] + "  Hora: " + infUser[i]["time"] +"</h3><img src=\"img/" + iconoTiempo[infUser[i]["id_est_tie"]] + "\"  height=\"64\" width=\"64\"/>"
             //content: "<h3>En Majadahonda ahora hace : "+  infoSensorAyun["temperatura"]  + "°</h3><img src=\"img/" +imageweather + "\"  height=\"64\" width=\"64\"/><HR width=50% align=\"center\"><h3>Grados: " + infUser[i]["grados"] + "<br>Fecha: " +   infUser[i]["date"] + "  Hora: " + infUser[i]["time"] +"</h3><img src=\"img/" + iconoTiempo[infUser[i]["id_est_tie"]] + "\"  height=\"64\" width=\"64\"/>"
         }
         
       });
  //    }
    }
}


jQuery(document).ready(function() {
  if ( navigator && navigator.userAgent && (lang = navigator.userAgent.match(/android.*\W(\w\w)-(\w\w)\W/i))) {
  lang = lang[1];
}
if (!lang && navigator) {
  if (navigator.language) {
    lang = navigator.language;
  }else if (navigator.browserLanguage) {
    lang = navigator.browserLanguage;
  }else if (navigator.systemLanguage) {
    lang = navigator.systemLanguage;
  }else if (navigator.userLanguage) {
    lang = navigator.userLanguage;
  }
  lang = lang.substr(0, 2);
}	
loadBundles(lang);
});


function loadBundles(lang) {  
  jQuery.i18n.properties({
  name:'Messages',
  path:'bundle/',
  mode:'both',
  language:lang,
  callback: function() {
  document.getElementById("btHome").innerHTML=jQuery.i18n.prop('msg_HOME');
  document.getElementById("btMap").innerHTML=jQuery.i18n.prop('msg_MAP');
  document.getElementById("btPrediccion").innerHTML=jQuery.i18n.prop('msg_Predicion');
  document.getElementById("lbsky").innerHTML=jQuery.i18n.prop('msg_Sky');
  document.getElementById("lbTemperature").innerHTML=jQuery.i18n.prop('msg_Temperature');

  }
});
}


function onWebMajadahonda() {
  // external url
   document.removeEventListener("backbutton", onBackKeyDown);
  var ref = window.open(encodeURI('http://www.majadahonda.org'), '_blank', 'location=yes');
}


function onWebIndiceUV() {
  // external url
  sleep(1000);
  var ref = window.open(encodeURI('http://es.wikipedia.org/wiki/Índice_UV'), '_blank', 'location=yes');
}


function pressButton(name) {
  $("#" + name).css("border-top-color", "#415c7f");
  $("#" + name).css("background", "-moz-linear-gradient(top, rgba(255,255,255,1) 0%, rgba(30,105,222,1) 78%, rgba(30,105,222,1) 100%)");
  $("#" + name).css("background", " -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,255,255,1)), color-stop(77%,rgba(255,255,255,1)), color-stop(78%,rgba(0,86,159,1)), color-stop(100%,rgba(0,86,159,1)))");
  $("#" + name).css("background", "-webkit-linear-gradient(top, rgba(255,255,255,1) 0%,rgba(255,255,255,1) 77%, rgba(30,105,222,1) 78%,rgba(0,86,159,1) 100%)");
  $("#" + name).css("background", "-o-linear-gradient(top, rgba(255,255,255,1) 0%,rgba(255,255,255,1) 77%, rgba(30,105,222,1) 78%,rgba(0,86,159,1) 100%)");
  $("#" + name).css("background", "-o-linear-gradient(top, rgba(255,255,255,1) 0%,rgba(255,255,255,1) 77%, rgba(30,105,222,1) 78%,rgba(0,86,159,1) 100%)");
  $("#" + name).css("background", "linear-gradient(to bottom, rgba(255,255,255,1) 0%,rgba(255,255,255,1) 77%,rgba(30,105,222,1) 78%,rgba(0,86,159,1) 100%)");
  $("#" + name).css("color", "#00569F");
}

function releaseButton(name) {   
    $("#" + name).css("background", "#FFFFFF");
  $("#" + name).css("color", "#233245");
}

function changeButton (bot){
	
 for(var n = 1; n <= 6; n++) {
   var ib = "botStatus"+ n;  	
    botStatus[ib] = 0;
 }
   
  //realizarBusqueda = true;
  var id = $(bot).attr("id");
  if(botStatus[id]===0){
    botStatus[id] = 1;       
  }
  else{
    botStatus[id] = 0;
  }
  updateButtons();
}

function updateButtons(){
  for(var n = 1; n <= 6; n++)
  { 
    var id = "botStatus"+ n;
    statusBoton($("#"+id));     
  }
}

function statusBoton(bot){
  var id = $(bot).attr("id");
  if(botStatus[id]===0){  	
    $(bot).css("background", "rgb(242,246,248)");
    $(bot).css("-webkit-border-radius", "0px 0px 0px 0px");
  }
  else{  	
    $(bot).css("background", "#2b72cc");
    $(bot).css("background-image", "-webkit-linear-gradient(top, #82b9ff, #2b72cc)");
    $(bot).css("background-image", "-moz-linear-gradient(top, #82b9ff, #2b72cc)");
    $(bot).css("background-image", "-ms-linear-gradient(top, #82b9ff, #2b72cc)");
    $(bot).css("background-image", "-o-linear-gradient(top, #82b9ff, #2b72cc)");
    $(bot).css("background-image", "linear-gradient(top, #82b9ff, #2b72cc)");
    $(bot).css("-webkit-border-radius", "5px 5px 5px 5px");
  }                 
}

function getAemetData(callback) {
    weather = new Array();
    temperature = new Array();
    humedad = new Array();
    temperatureMaxMin = new Array();
    viento = new Array();
    thermal = new Array();
    rh = new Array();
    rain = new Array();
    uv = new Array();
    var i = 0;
    var j = 0;
    var k = 0;
    var t = 0;
    var r = 0;
    var p = 0;
    var u = 0;  
    var f = 0;
    var v = 0;
     $.get("http://www.aemet.es/xml/municipios/localidad_28080.xml", function (xml) { // $.get("ejem2.xml", function (xml) { //           		
      $(xml).find('dia').each(function(){
    	var dia = $(this).attr('fecha');   		
        $(this).find('estado_cielo').each(function(){
         weather[i] = new Array();
         var periodo = $(this).attr('periodo');
         var descripcion = $(this).attr('descripcion');
         weather[i]["dia"]= dia;         
         weather[i]["periodo"] = periodo;         
         weather[i]["descripcion"] = descripcion;                  
         i+=1;
         });
         $(this).find('temperatura').each(function(){
         	temperatureMaxMin[j]= new Array();         	
         	var max = $(this).find('maxima').text();         	
            var min = $(this).find('minima').text();            
            temperatureMaxMin[j]["maxima"] = max;
            temperatureMaxMin[j]["minima"] = min;
            temperatureMaxMin[j]["dia"] = dia;
            j += 1;
            $(this).find('dato').each(function(){
            	temperature[k] = new Array();
            	var temp = $(this).text();
         		var hora = $(this).attr('hora');
         		temperature[k]["temperatura"]=temp;
         		temperature[k]["dia"]=dia;
         		temperature[k]["hora"]=hora;     	
         		k+=1;
            });                     
         });
         $(this).find('viento').each(function(){
         	viento[v]= new Array();
         	var periodo = $(this).attr('periodo');   	
         	var direccion = $(this).find('direccion').text();         	
            var velocidad = $(this).find('velocidad').text();
            viento[v]["perido"] = periodo;
            viento[v]["direccion"] = direccion;
            viento[v]["velocidad"] = velocidad;
            v += 1;                      
         });
         $(this).find('sens_termica').each(function(){
         	thermal[t]= new Array();         	
         	var max = $(this).find('maxima').text();         	
            var min = $(this).find('minima').text();            
            thermal[t]["maxima"] = max;
            thermal[t]["minima"] = min;
            thermal[t]["dia"] = dia;
            t += 1;                            
         });
         $(this).find('humedad_relativa').each(function(){
         	rh[r]= new Array();         	
         	var max = $(this).find('maxima').text();         	
            var min = $(this).find('minima').text();            
            rh[r]["maxima"] = max;
            rh[r]["minima"] = min;
            rh[r]["dia"] = dia;
            r += 1;     
            $(this).find('dato').each(function(){
            	humedad[f] = new Array();
            	var temp = $(this).text();
         		var hora = $(this).attr('hora');
         		humedad[f]["humedad"]=temp;
         		humedad[f]["dia"]=dia;
         		humedad[f]["hora"]=hora;     	
         		f+=1;
            });                         
         });
         $(this).find('prob_precipitacion').each(function(){
           rain[p] = new Array();
           var periodo = $(this).attr('periodo');
           var valor = $(this).text();
           rain[p]["dia"]= dia;         
           rain[p]["periodo"] = periodo;         
           rain[p]["valor"] = valor;                     
           p+=1;
         });
         $(this).find('uv_max').each(function(){
         	uv[u] = new Array();
         	var indiR = $(this).text();
         	uv[u]["uv"] = indiR;
         	uv[u]["dia"] = dia;
         	u +=1;
         });
       });
 
       callback();
    });
}

  function getInfoSensor(callback) {
      
  var query = "select d.id_data, d.id_sensor, d.fechahora, d.temperatura, d.humedad, d.radiacion, d.presionatmosferica, d.anemometro, d.pluviometro, d.direcciondelviento, d.fechahoraservidor from data d inner join(   select id_sensor, max(fechahora) fh   from data   group by id_sensor) dd on d.id_sensor = dd.id_sensor and d.fechahora = dd.fh";  
  $.ajax({  
        url:'http://150.241.239.65:8080/IESCities/api/data/query/' + numId + '/sql',	
    type:'POST', 
    data: query, 
    contentType:'text/plain',
    //dataType:'json', 
    error:function(jqXHR,text_status,strError){       
       navigator.notification.alert(
          'No se puede obtener la información de los sensores, intenteló más tarde. Sentimos las molestias', // message
           null,            // callback to invoke with index of button pressed
          'Error de conexión el servidor',           // title
          'Cerrar' 
       );
    },
    success:function(data){           
      var empObject = eval('(' + JSON.stringify(data) + ')');  
      var empObjectLen = data.rows.length;               
      infoSensorAyun =  new Object();
      infoSensorMonte = new Object();
      
      icoAlert = "alertaVerde";
      menAlert =  " Sin riesgo. No hay avisos meteorológicos.";
      for (var i=0; i<empObjectLen; i++) {                
        if(empObject.rows[i].id_sensor === "THA")  // SENSOR AYUNTAMIENTO
        {            
            infoSensorAyun["id_data"] =  empObject.rows[i].id_data;        
            infoSensorAyun["fechahoraservidor"] =  empObject.rows[i].fechahoraservidor;
            infoSensorAyun["id_sensor"] =  empObject.rows[i].id_sensor;          
            infoSensorAyun["direcciondelviento"] =  empObject.rows[i].direcciondelviento;   
            infoSensorAyun["anemometro"] =  empObject.rows[i].anemometro; 
            if(empObject.rows[i].temperatura.indexOf(".")!==-1)
                infoSensorAyun["temperatura"] =  empObject.rows[i].temperatura.substring(0, empObject.rows[i].temperatura.indexOf(".")+2);       
            else
                 infoSensorAyun["temperatura"] =  empObject.rows[i].temperatura;            

            if(infoSensorAyun["anemometro"] > 65) {
                icoAlert = "alertaRoja";
                menAlert = "<div style='color:red;'><strong> ¡Peligro, viento extremo! </strong></div>";                
            }
            else {
                if(infoSensorAyun["anemometro"] >= 40){
                    if(icoAlert !== "alertaRoja")                
                       icoAlert = "alertaAmarilla";
                   if(menAlert.indexOf("Sin riesgo")!==-1) 
                     menAlert = "<div style='color:red'><strong>  ¡Precaución, viento fuerte! </strong></div>";
                   else                   
                     menAlert += "<div style='color:red'><strong>  ¡Precaución, viento fuerte! </strong></div>";
                }            
            }
            if(empObject.rows[i].fechahora.indexOf(".")!==-1)
                infoSensorAyun["fechahora"] =  empObject.rows[i].fechahora.substring(0, empObject.rows[i].fechahora.indexOf("."));           
            else
                infoSensorAyun["fechahora"] =  empObject.rows[i].fechahora;  
            var fechaEsp = infoSensorAyun["fechahora"].substring(0,infoSensorAyun["fechahora"].indexOf(" ")).split("-");
            infoSensorAyun["fecha"] =  fechaEsp[2] + "/" + fechaEsp[1] + "/" + fechaEsp[0];// infoSensor[i]["fechahora"].substring(0,infoSensor[i]["fechahora"].indexOf(" "));
            infoSensorAyun["fechahoraservidor"] =  empObject.rows[i].fechahora;        
            infoSensorAyun["hora"] = infoSensorAyun["fechahora"].substring(infoSensorAyun["fechahora"].indexOf(" ")+1,infoSensorAyun["fechahora"].length);        

            if(empObject.rows[i].humedad.indexOf(".")!==-1)
                infoSensorAyun["humedad"] =  empObject.rows[i].humedad.substring(0, empObject.rows[i].humedad.indexOf("."));           
            else
                infoSensorAyun["humedad"] =  empObject.rows[i].humedad;         
            infoSensorAyun["pluviometro"] =  empObject.rows[i].pluviometro;    
            infoSensorAyun["presionatmosferica"] =  empObject.rows[i].presionatmosferica * 10;
            infoSensorAyun["presionatmosferica"] =  infoSensorAyun["presionatmosferica"].toFixed(1);               
         }
         if(empObject.rows[i].id_sensor === "THR")  // SENSOR AYUNTAMIENTO
        {            
            infoSensorMonte["id_data"] =  empObject.rows[i].id_data;        
            infoSensorMonte["fechahoraservidor"] =  empObject.rows[i].fechahoraservidor;
            infoSensorMonte["id_sensor"] =  empObject.rows[i].id_sensor;   
            if(empObject.rows[i].temperatura.indexOf(".")!==-1)
                infoSensorMonte["temperatura"] =  empObject.rows[i].temperatura.substring(0, empObject.rows[i].temperatura.indexOf(".")+2);       
            else
                 infoSensorMonte["temperatura"] =  empObject.rows[i].temperatura;               
            if(empObject.rows[i].humedad.indexOf(".")!==-1)
                infoSensorMonte["humedad"] =  empObject.rows[i].humedad.substring(0, empObject.rows[i].humedad.indexOf("."));           
            else
                infoSensorMonte["humedad"] =  empObject.rows[i].humedad;   
            infoSensorMonte["radiacion"] = "uv_1.png";                        
            if(empObject.rows[i].radiacion < 0) 
               infoSensorMonte["radiacion"] = "uv_0.png";    
           if((empObject.rows[i].radiacion >= 0) && (empObject.rows[i].radiacion > 400))
               infoSensorMonte["radiacion"] = "uv_1.png";    
           if((empObject.rows[i].radiacion >= 400) && (empObject.rows[i].radiacion > 800))
               infoSensorMonte["radiacion"] = "uv_2.png";    
           if((empObject.rows[i].radiacion >= 800) && (empObject.rows[i].radiacion > 1200))
               infoSensorMonte["radiacion"] = "uv_3.png";    
           if((empObject.rows[i].radiacion >= 1200) && (empObject.rows[i].radiacion > 1600)) {
               infoSensorMonte["radiacion"] = "uv_4.png";    
               icoAlert = "alertaRoja";
                if(icoAlert !== "alertaRoja")                
                 icoAlert = "alertaAmarilla";
               if(menAlert.indexOf("Sin riesgo")!==-1) 
                 menAlert = "<div style='color:red'><strong>  ¡Precaución, radiación solar muy alta! </strong></div>";
               else                   
                 menAlert += "<div style='color:red'><strong>  ¡Precaución, radiación solar muy alta! </strong></div>";               
           }           
           if((empObject.rows[i].radiacion > 1600))
           {
               infoSensorMonte["radiacion"] = "uv_5.png";   
              if(menAlert.indexOf("Sin riesgo")!==-1) 
                  menAlert = "<div style='color:red'><strong>  ¡Peligro, radiación solar extrema! </strong></div>";
               else                   
                  menAlert += "<div style='color:red'><strong>  ¡Peligro, radiación solar extrema! </strong></div>";
           }
            if(empObject.rows[i].fechahora.indexOf(".")!==-1)
                infoSensorMonte["fechahora"] =  empObject.rows[i].fechahora.substring(0, empObject.rows[i].fechahora.indexOf("."));           
            else
                infoSensorMonte["fechahora"] =  empObject.rows[i].fechahora;  
            var fechaEsp = infoSensorMonte["fechahora"].substring(0,infoSensorMonte["fechahora"].indexOf(" ")).split("-");
            infoSensorMonte["fecha"] =  fechaEsp[2] + "/" + fechaEsp[1] + "/" + fechaEsp[0];// infoSensor[i]["fechahora"].substring(0,infoSensor[i]["fechahora"].indexOf(" "));
            infoSensorMonte["fechahoraservidor"] =  empObject.rows[i].fechahora;        
            infoSensorMonte["hora"] = infoSensorMonte["fechahora"].substring(infoSensorMonte["fechahora"].indexOf(" ")+1,infoSensorMonte["fechahora"].length);  
             
         }         
     }
              
       callback();
    }  
  });  
}



function getId(callback) {    
    $.getJSON('http://150.241.239.65:8080/IESCities/api/entities/datasets/search?keywords=SensorsData&offset=0&limit=1000', function(data) {
       var empObject = eval('(' + JSON.stringify(data) + ')');
       numId = empObject[0].datasetId;                
       callback();
    }); 
}

function getIdCom(callback) {
    $.getJSON('http://150.241.239.65:8080/IESCities/api/entities/datasets/search?keywords=Comercial&offset=0&limit=1000', function(data) {
       var empObject = eval('(' + JSON.stringify(data) + ')');
       numIdCom = empObject[0].datasetId;     
       callback();
    }); 
}

function setLog(typeMens, message) {
  var d = new Date();
  var n = d.getTime();	
  n = n / 1000;  
  var urlAj = "http://150.241.239.65:8080/IESCities/api/log/app/event/" + n + "/Majadahonda Healthy City/" + numSessionLog + "/" + typeMens + "/" + message;
  $.ajax({        
 	url: urlAj,       
    type:'POST', 
    data: '', 
    dataType:'json', 
    error:function(jqXHR,text_status,strError){         
      console.log('No connection IESCities api log');

    },
    success:function(data){      

    }
  });  
}




function setEstadoTiempo(query) {     
  $.ajax({            
    url:'http://150.241.239.65:8080/IESCities/api/data/update/' + numIdCom + '/sql',           
    type:'POST', 
    data: query, 
    dataType:'json', 
    headers: {"Authorization": make_basic_auth('USER', 'CLAVE')},
    error:function(jqXHR,text_status,strError){ 
     alert("Error de conexión con servidor");
      navigator.notification.alert(
          'No se ha podido enviar sus datos, intenteló más tarde. Sentimos las molestias', // message
           null,            // callback to invoke with index of button pressed
          'Error de conexión con servidor',           // title
          'Cerrar' 
       );
    },
    success:function(data){
        setLog("AppProsume", "Data Input");
        alert('Su información ha sido añadida, gracias.');        
        navigator.notification.alert(
          'Su información ha sido añadida, gracias.', // message
           null,            // callback to invoke with index of button pressed
          'Infomación almacenada',           // title
          'Cerrar' 
       );
      	
      menu('mapa');
    }
  });  

}
      
function make_basic_auth(user, password){
    var tok = user + ':' + password;
    var hash = Base64.encode(tok);
    return "Basic " + hash;
}
      

function getInfoUser() {    
    
  var query = "Select ID, ID_USER, ID_EST_TIE, GRADOS, COORDENADAS, DATE, TIME from PREDICCION_USUARIO ORDER BY DATE DESC LIMIT 30";  
  $.ajax({  
        url:'http://150.241.239.65:8080/IESCities/api/data/query/' + numIdCom + '/sql',	
    type:'POST', 
    data: query, 
    contentType:'text/plain',
    error:function(jqXHR,text_status,strError){ 
       navigator.notification.alert(
          'No se puede obtener la información de otros usuarios, intenteló más tarde. Sentimos las molestias', // message
           null,            // callback to invoke with index of button pressed
          'Error de conexión el servidor',           // title
          'Cerrar' 
       );
    },
    success:function(data){
      var empObject = eval('(' + JSON.stringify(data) + ')');
      var empObjectLen = data.rows.length;         
      infUser = new Array(empObjectLen);
      for (var i=0; i<empObjectLen; i++) {
        infUser[i] = new Object();
        infUser[i]["id"] =  empObject.rows[i].ID;
        infUser[i]["id_user"] =  empObject.rows[i].ID_USER;
        infUser[i]["id_est_tie"] =  empObject.rows[i].ID_EST_TIE;          
        infUser[i]["grados"] =  empObject.rows[i].GRADOS;         
        infUser[i]["coordenadas"] =  empObject.rows[i].COORDENADAS;
        infUser[i]["date"] =  empObject.rows[i].DATE;
        infUser[i]["time"] = empObject.rows[i].TIME;
      }        
    }
  });  
}